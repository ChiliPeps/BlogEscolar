<!-- Jquery -->

{!! Html::script('extras/all.js') !!}

{!! Html::script('js/jquery-2.1.4.min.js') !!}
{!! Html::script('js/blog.js') !!}
{!! Html::script('js/datepicker/jquery-ui.js') !!}

{!! Html::script('https://code.jquery.com/jquery-1.12.4.js') !!}
<!-- {!! Html::script('https://code.jquery.com/ui/1.12.1/jquery-ui.js') !!} -->
{!! Html::script('js/datepicker/jquery-ui.js') !!}

<!-- Latest compiled and minified JavaScript -->
{!! Html::script('js/bootstrap.min.js') !!}
{!! Html::script('js/app.min.js') !!}
{!! Html::script('js/jquery.slimscroll.js') !!}

<!-- Sweet Alert -->
{!! Html::script('js/sweetalert.min.js') !!}
{!! Html::script('js/vue/vue.js') !!}
{!! Html::script('js/vue/vue.min.js') !!}
{!! Html::script('js/vue/vue-resource.min.js') !!}

<!-- Js del libro -->
{!! Html::script('lib/hash.js') !!}
{!! Html::script('lib/turn.js') !!}
{!! Html::script('lib/turn.min.js') !!}
{!! Html::script('lib/zoom.js') !!}

<!-- {!! Html::script('lib/zoom.min.js') !!} -->

{!! Html::script('lib/bookshelf.js') !!}
{!! Html::script('lib/scissor.js') !!}
{!! Html::script('lib/scissor.min.js') !!}