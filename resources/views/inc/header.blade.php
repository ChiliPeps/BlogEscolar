
<!-- Main Header -->
<header id="header" class="main-header">
    <!-- Logo -->
    <a id="logStJohns" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            <b>
                ST JOHN´S - La Paz
            </b>
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <b>
                ST JOHN´S - La Paz
            </b>
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a id="botonMenu" href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" data-placement="bottom" title="Ocultar o mostrar menú">
            <span class="sr-only">
                Toggle navigation
            </span>
        </a>
        @if(Auth::check())
            <div class="pull-right">
                <p>
                    <a href="logOut">
                        <span type="button" id="pictureOut" data-toggle="tooltip" data-placement="left" title="Cerrar sesión."> 
                            <i class="fa fa-fw fa-sign-out"></i>
                        </span>
                    </a>
                </p>
            </div>
        @endif
    </nav>
</header>