<!-- Left side column. contains the logo and sidebar -->
<aside id="menu" class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu nav nav-pills nav-stacked">
            <li class="header text-uppercase text-center"> 
                <h4> MENÚ </h4>
            </li>
            <li>
                <a href="{{ url('home') }}">
                    <i class="fa fa-fw fa-home"></i>Inicio
                </a>
            </li>
            <li>
                <a href="{{ url('about') }}">
                   <i class="fa fa-fw fa-plus"></i>Acerca de
               </a>
            </li>
            <li>
                <a href="{{ url('loginBlog') }}">
                    <i class="fa fa-fw fa-comments-o"></i>Blog
                </a>
            </li>
            <li>
                <a href="{{ url('news') }}">
                    <i class="fa fa-fw fa-newspaper-o"></i>Noticias
                </a>
            </li>
            <li>
                <a href="{{ url('contact') }}">
                    <i class="fa fa-fw fa-map-marker"></i>Contactanos
                </a>
            </li>
            <li>
                <a href="{{ url('stjohnsMundo') }}">
                    <i class="fa fa-fw fa-plane"></i>St John´s en el mundo
                </a>
            </li>
            <li>
                <a href="{{ url('reglamentoStjohns') }}">
                    <i class="glyphicon glyphicon-book"></i>Reglemanto St John´s
                </a>
            </li>
        </ul>
    </section>
</aside>