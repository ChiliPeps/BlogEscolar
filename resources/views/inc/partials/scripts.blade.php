
<script>
	//Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

	new Vue({

    	ready: function () { 
    		this.showEscolaridadesTareasBlog();    		
    		this.showCarpetasStJohnsMundoBlog();
    		this.getNoticias()
    	 },

		el: '#appStJohns',

		data: {
        	loading: false,
// St Johns en el Mundo
			arregloFotosKinder: {},
			panelKinder: false,
			panelPrimaria: false,
			panelSecundaria: false,
			panelPreparatoria: false,
			panelSliderFotos: false,
			panelActivoStMundo: 'Kinder',
			albumSeleccionado: '',
			nombreAlbum: 'nombre',
			imagenPrincipal: 'img/null.png',
			fotoAlbum:{
				contenido: 'img/null.png'
			},
// Blog 
			panelBlogOpciones: true,
			panelLoginInOpcionesBusqueda: false,
			opcion: '',
			texto: '',
			idEscolaridadGrupoGrado: '',
			nivelEducativo: '',
// Blog - Tareas
			arreglo: {},
			apuntador: 0,
			panelLoginInTareas: false,
			busquedaTareaFiltro: true,
			busquedaCalendario: false,
			panelKinderBlog: false,
			panelPrimariaBlog: false,
			panelSecundariaBlog: false,
			panelPreparatoriaBlog: false,
			busqueda: '',
			nivelEducativo: '',
			busquedaIdEscolaridad: '',
			busquedaTarea: '',
			parametrosBusqueda: '',
			busquedaTareaTipo: '',
			panelLoginInTareasOpciones: false,
			panelLoginInAvisosOpciones: false,
			escolaridadTexto: '',
			valorParametro: '',
			panelLoginInTareas: false,
			panelLoginInAvisos: false,
// Blog - Aviso
			busquedaFechaAvisos: false,
			panelLoginInAvisos: false,
			panelAvisosKinder: false,
			panelAvisosPrimaria: false,
			panelAvisosSecundaria: false,
			panelAvisosBachillerato: false,
			busquedaAviso: '',
			avisoRecetor: '',
			grupoGrado: '',
// noticias
			principalJson:null,
			noticiasJson:[],
			url_help: "{{ URL::to('/') }}/",
			noticias:[],
// Deportes	
			panelLoginInDeportes: false,
			panelFutbolBlogDeportes: false,
			panelGimnasioBlogDeportes: false,
			panelNatacionBlogDeportes: false,			
// Calendario
			panelLoginInCalendario: false,
			panelCalendariosKinder: false,
			panelCalendariosPrimaria: false,
			panelCalendariosSecundaria: false,
			panelCalendariosPreparatoria: false,

// Login
			mensaje: 'Bienvenido al Blog St John´s.',
			userBlog: {
				user: '',
				password: ''
			},
			mensajePanel: 'St John´s en el Mundo.'
		},
		methods: {
			mouseMove: function()
			{
				$(".carpetaFotos").mousemove(function (){
					$(this).find("a:eq(0)").css("display", "none");
					$(this).find("a:eq(1)").css("display", "block");
					$('#carpetaSeleccionada').text($(this).attr("id"));
				});
				this.albumSeleccionado = $('#carpetaSeleccionada').text();
			},
			mouseOut: function()
			{		
				$(".carpetaFotos").mouseout(function (){
					$(this).find("a:eq(1)").css("display", "none")
					$(this).find("a:eq(0)").css("display", "block");
				});
			},
//Get All Data
	        showEscolaridadesTareasBlog: function () {
	            //this.loading = true
	            var resource = this.$resource("{{route('blog')}}")
	            resource.get({}).then(function (response) {
	                //alert(JSON.stringify(response.data))
	                this.$set('originalJson', response.data)
	                this.escolaridadesJson = this.originalJson
	                this.loading = false
	            });
	        },
	        showCarpetasStJohnsMundoBlog: function () {
	            //this.loading = true
	            var resource = this.$resource("{{route('stjohnsMundo')}}")
	            resource.get({}).then(function (response) {
	                //alert(JSON.stringify(response.data))
	                this.$set('originalJson1', response.data)
	                this.carpetasStJohnsMundoJson = this.originalJson1
	                this.loading = false
	            });
	        },
// St Johns en el Mundo
			kinderIn: function() {
				this.panelSliderFotos = false;
				this.panelKinder = true;
				this.panelPrimaria = false;
				this.panelSecundaria = false;
				this.panelPreparatoria = false;
				this.mensajePanel = "St John´s en el Mundo | Kinder";
				this.panelActivoStMundo = "Kinder";
				// Colcocar en el slider las imágenes de la carpeta de Kinder
				this.fotosPrincipal(this.carpetasStJohnsMundoJson.resultadoCarpetasSociales, this.carpetasStJohnsMundoJson.resultadoFotosCarpetasKinder, "Kinder");
			},
			primariaIn: function() {
				this.panelSliderFotos = false;
				this.panelKinder = false;
				this.panelPrimaria = true;
				this.panelSecundaria = false;
				this.panelPreparatoria = false;
				this.mensajePanel = "St John´s en el Mundo | Primaria";
				this.panelActivoStMundo = "Primaria";
				// Colcocar en el slider las imágenes de la carpeta de Primaria
				this.fotosPrincipal(this.carpetasStJohnsMundoJson.resultadoCarpetasSociales, this.carpetasStJohnsMundoJson.resultadoFotosCarpetasPrimaria, "Primaria");
			},
			secundariaIn: function() {
				this.panelSliderFotos = false;
				this.panelKinder = false;
				this.panelPrimaria = false;
				this.panelSecundaria = true;
				this.panelPreparatoria = false;
				this.mensajePanel = "St John´s en el Mundo | Secundaría";
				this.panelActivoStMundo = "Secundaria";
				// Colcocar en el slider las imágenes de la carpeta de Secundaria
				this.fotosPrincipal(this.carpetasStJohnsMundoJson.resultadoCarpetasSociales, this.carpetasStJohnsMundoJson.resultadoFotosCarpetasSecundaria, "Secundaria");
			},
			preparatoriaIn: function() {
				this.panelSliderFotos = false;
				this.panelKinder = false;
				this.panelPrimaria = false;
				this.panelSecundaria = false;
				this.panelPreparatoria = true;
				this.mensajePanel = "St John´s en el Mundo | Preparatoria";
				this.panelActivoStMundo = "Preparatoria";
				// Colcocar en el slider las imágenes de la carpeta de Preparatoria
				this.fotosPrincipal(this.carpetasStJohnsMundoJson.resultadoCarpetasSociales, this.carpetasStJohnsMundoJson.resultadoFotosCarpetasBachillerato, "Bachillerato");
			},
			fotosPrincipal: function (arreglo1, arreglo2, escolaridad){
				var nombreCarpeta = ""; var c = 0;
				for (var i = 0; i < arreglo1.length; i++) {
			 		// Obtener el nombre de la carpeta.
			 		nombreCarpeta = arreglo1[i].nombre;
			 		nombreCarpeta = "#" + nombreCarpeta;
					if(arreglo1[i].numero > 0)
					{
						if(arreglo1[i].numero > 1)
						{
						 	if(arreglo1[i].receptor == escolaridad)
						 	{
						 		// Colocar las 2 primeras imágenes en el slider de la carpeta.
						 		$(nombreCarpeta).find("a:eq(0)").children('img').attr("src", arreglo2[c].contenido);
						 		$(nombreCarpeta).find("a:eq(1)").children('img').attr("src", arreglo2[c + 1].contenido);
						 		c = arreglo1[i].numero;
						 	}
						}
						else
						{
						 	if(arreglo1[i].receptor == escolaridad)
						 	{
						 		// Colocar la primera imagen en el slider de la carpeta.
						 		$(nombreCarpeta).find("a:eq(0)").children('img').attr("src", arreglo2[c].contenido);
						 		$(nombreCarpeta).find("a:eq(1)").children('img').attr("src", arreglo2[c].contenido);
						 		c = arreglo1[i].numero;
						 	}	
						}
					}
					else{ // Colocar una imagen con un Cero cuando no existan imágenes en las carpetas
					 	if(escolaridad == "Kinder")
					 	{
					 		this.fotosNull(nombreCarpeta, "img/blue.png");
					 	}
					 	else if(escolaridad == "Primaria")
					 	{
					 		this.fotosNull(nombreCarpeta, "img/green.png");
					 	}
					 	else if(escolaridad == "Secundaria")
					 	{
					 		this.fotosNull(nombreCarpeta, "img/orange.png");
					 	}
					 	else
					 	{
					 		this.fotosNull(nombreCarpeta, "img/red.png");
					 	}
					}
				};
			},
			fotosNull: function (div, imagen){
				// Colocar la imagen de Null cuando la carpeta no tenga fotos.
		 		$(div).find("a:eq(0)").children('img').attr("src", imagen);
		 		$(div).find("a:eq(1)").children('img').attr("src", imagen);
			},
			sliderFotos: function() {
				this.panelSliderFotos = true;
				if (this.panelActivoStMundo == "Kinder") {
					this.panelKinder = false;

				}else if (this.panelActivoStMundo == "Primaria") {
					this.panelPrimaria = false;

				} else if (this.panelActivoStMundo == "Secundaria") {
					this.panelSecundaria = false;
				}else{
					this.panelPreparatoria = false;
				}
				//debugger
				var posicionLista = 0; var divImagen = "";
				for (var i = 0; i < this.carpetasStJohnsMundoJson.resultadoFotosAll.length; i++) {
					if(this.albumSeleccionado == this.carpetasStJohnsMundoJson.resultadoFotosAll[i].nombre)
					{
						$('#slider').find("li:eq(" + posicionLista + ")").attr("data-slide-to", posicionLista);
						if(posicionLista == 0)
						{
							this.imagenPrincipal = this.carpetasStJohnsMundoJson.resultadoFotosAll[i].contenido; 
							$('#slider').find("li:eq(" + posicionLista + ")").attr("class", 'active');
							divImagen = "#img-" + this.carpetasStJohnsMundoJson.resultadoFotosAll[i].numeroId;
							$('.item:first').attr('class', 'item active');
						}
						posicionLista = posicionLista + 1;
					}
				};
			},
			sliderFotosBack: function() {
				this.panelSliderFotos = false;
				if (this.panelActivoStMundo == "Kinder") {
					this.panelKinder = true;

				}else if (this.panelActivoStMundo == "Primaria") {
					this.panelPrimaria = true;

				} else if (this.panelActivoStMundo == "Secundaria") {
					this.panelSecundaria = true;
				}else{
					this.panelPreparatoria = true;
				}
			},
// Blog Tareas - Avisos
			inBlog: function() 
			{
				//debugger
				this.idEscolaridadGrupoGrado = document.getElementById("gradoGrupoBlog").value;
				if(this.panelLoginInTareas == true)
				{ 
					this.tareas();
				}
				else
				{
					this.valorParametro = 0;
					document.getElementById("datepickerBlog").value = "";
					this.grupoGrado = $("#gradoGrupoBlog option:selected").text().trim();
				}
			},
			panelOpciones: function()
			{
				//debugger
				// Mostrar las opciones de búsqueda y ocultar los paneles de tarea, hasta que se elija un grupo y grado.
				document.getElementById("gradoGrupoBlog").value = 0;
				document.getElementById("escolaridadBlog").value = 0;
				this.idEscolaridadGrupoGrado = "";
				this.nivelEducativo = "";				

            	this.visibilidadPanelesTareas(false, false, false, false);
				this.mensaje = "Bienvenido al Blog St John´s.";
				this.panelBlogOpciones = true;
				// Ocultar los paneles
            	this.panelLoginInAvisos = false;
            	this.panelLoginInTareas = false;
            	this.panelLoginInDeportes = false;
            	this.panelLoginInCalendario = false;
            	this.panelLoginInOpcionesBusqueda = false;
				this.cambiarColorOpciones(" Elegir escolaridad... ");
				this.valorParametro = 0;

				this.mostarPanelCalendario(false, false, false, false);
				this.mostarPanelDeportes(false, false, false);
			},
			cambiarColorOpciones: function(escolaridadTexto)
			{
				switch(escolaridadTexto)
            	{
            		case 'Kinder': case ' Elegir escolaridad... ':
            		{
	            		document.getElementById("gradoGrupoBlog").className = "btn btn-primary";
	            		document.getElementById("opcionesBusqueda").className = "box box-primary box-solid collapsed-box";
            			break;
            		}
            		case 'Primaria':
            		{
	            		document.getElementById("gradoGrupoBlog").className = "btn btn-success";
	            		document.getElementById("opcionesBusqueda").className = "box box-success box-solid collapsed-box";	
            			break;
            		}
            		case 'Secundaria':
            		{
	            		document.getElementById("gradoGrupoBlog").className = "btn btn-warning";
	            		document.getElementById("opcionesBusqueda").className = "box box-warning box-solid collapsed-box";
            			break;
            		}
            		case 'Bachillerato':
            		{
	            		document.getElementById("gradoGrupoBlog").className = "btn btn-danger";
	            		document.getElementById("opcionesBusqueda").className = "box box-danger box-solid collapsed-box";
            			break;
            		}
            	}
			},
// Blog Tareas
			panelTareas: function()
			{
				this.busquedaFechaAvisos = false;
				this.panelLoginInDeportes = false;
				this.panelLoginInCalendario = false;
				this.nivelEducativo = $("#escolaridadBlog option:selected").text().trim();
				//alert(this.nivelEducativo);
				this.mensaje = "Bienvenido al Blog St John´s - Tareas";
            	this.visibilidadPanelesTareas(false, false, false, false);
            	this.panelLoginInTareas = true;
            	this.panelLoginInOpcionesBusqueda = true;
				this.panelBlogOpciones = false;
			},
			tareas: function ()
			{
				this.gradoGrupoOpcion = document.getElementById("gradoGrupoBlog").value;
		        this.escolaridadTexto = $("#escolaridadBlog option:selected").text().trim();
				document.getElementById("gradoGrupoHidden").value = this.gradoGrupoOpcion.trim();
				// Arreglo para colocar las tareas de una escolaridad especifica
				this.arreglo = {};
				// debugger
				for (var i = 0; i < this.escolaridadesJson.resultadoTarea.length; i++) 
				{
					if(this.gradoGrupoOpcion == this.escolaridadesJson.resultadoTarea[i].idEscolaridad)
					{
						this.arreglo[i] = this.escolaridadesJson.resultadoTarea[i];
					}
				};
				this.valorParametro = 0;
            	this.gradoGrupoTexto = $("#gradoGrupoBlog option:selected").text().trim();
            	// Ver el panel indicado de las tarea, de acuerdo a la escolaridad elejida.
            	//debugger
            	switch(this.escolaridadTexto)
            	{
            		case 'Kinder':
            		{
            			this.visibilidadPanelesTareas(true, false, false, false);
            			break;
            		}
            		case 'Primaria':
            		{
            			this.visibilidadPanelesTareas(false, true, false, false);
            			break;
            		}
            		case 'Secundaria':
            		{
            			this.visibilidadPanelesTareas(false, false, true, false);
            			break;
            		}
            		case 'Bachillerato':
            		{
            			this.visibilidadPanelesTareas(false, false, false, true);
            			break;
            		}
            	}
            	// Colocar el mensaje al usuario de que grado y grupo está búscando la tarea
            	if(this.gradoGrupoTexto != "Elegir grado y grupo...")
            	{
            		this.gradoGrupoTexto.toLowerCase();
					this.mensaje = "Tareas." + " | " + this.gradoGrupoTexto.charAt(0).toUpperCase() + this.gradoGrupoTexto.slice(1);
				}
			},
			visibilidadPanelesTareas: function (bandera1, bandera2, bandera3, bandera4)
			{
				this.panelKinderBlog = bandera1;
				this.panelPrimariaBlog = bandera2;
				this.panelSecundariaBlog = bandera3;
				this.panelPreparatoriaBlog = bandera4;
			},
			BusquedaTarea: function()
			{
				if(this.panelLoginInTareas == true)
				{ 
					this.tareas();
					//debugger
					// Mostrar el datePicker para buscar la tarea por fecha y ocultar el text que busca por los otro
					// parametros (Maestro y materia)
					document.getElementById("gradoGrupoHidden").value = this.gradoGrupoOpcion.trim();
	            	var opcionBusqueda = $("#parametrosBusquedaTarea option:selected").text().trim();
	            	if(opcionBusqueda == "Fecha de entrega" || opcionBusqueda == "Fecha de subida")
	            	{
	            		this.busquedaTareaFiltro = false;
						this.busquedaCalendario = true;
					}
					else
					{
	            		this.busquedaTareaFiltro = true;
						this.busquedaCalendario = false;
					}
	            	this.valorParametro = document.getElementById("parametrosBusquedaTarea").value.trim();            	
	            	// Limpiar los parametros de búsqueda.
					document.getElementById("textoBusqueda").value = "";
					document.getElementById("datepickerBlog").value = "";
					$("#textoBusqueda").focus();
					$("#textoBusqueda").blur();
				}
			},
			cambiarFecha: function(){
				alert(hola);
			},
// Blog Avisos
			panelAvisos: function()
			{
				//$("#gradoGrupoBlog value > 1").remove();
				this.panelLoginInDeportes = false;
				this.panelLoginInCalendario = false;
				this.busquedaFechaAvisos = true;
				this.panelLoginInTareas = false;
				this.nivelEducativo = $("#escolaridadBlog option:selected").text().trim();
				//alert(this.nivelEducativo);
				this.mensaje = "Bienvenido al Blog St John´s - Avisos";
            	this.panelLoginInOpcionesBusqueda = true;
            	this.panelLoginInAvisos = true;
				this.panelBlogOpciones = false;
			},
			cambiarColor: function()
			{
				//debugger;
				this.nivelEducativo = $("#escolaridadBlog option:selected").text().trim();
				//alert(this.nivelEducativo);
				this.grupoGrado = "";
				this.valorParametro = 0;
				$("#datepickerBlog").value = "";
				// Saber que escolridad se está búscando para colocar el color indicado
            	this.opcion = $("#escolaridadBlog option:selected").text().trim();
            	this.cambiarColorOpciones(this.opcion);
				this.visibilidadPanelesTareas(false, false, false, false);
				
				switch(this.opcion)
				{
					case "Kinder":
					{
						this.visibilidadPanelesAvisos(true, false, false, false);
						break;
					}
					case "Primaria":
					{
						this.visibilidadPanelesAvisos(false, true, false, false);
						break;
					}
					case "Secundaria":
					{
						this.visibilidadPanelesAvisos(false, false, true, false);
						break;
					}
					case "Bachillerato":
					{
						this.visibilidadPanelesAvisos(false, false, false, true);
						break;
					}
				}
			},
			visibilidadPanelesAvisos: function (bandera1, bandera2, bandera3, bandera4)
			{
				this.panelAvisosKinder = bandera1;
				this.panelAvisosPrimaria = bandera2;
				this.panelAvisosSecundaria = bandera3;
				this.panelAvisosBachillerato = bandera4;
			},
			busquedaFechaAviso: function ()
			{
				switch(this.escolaridadTexto)
            	{
            		case ' Elegir escolaridad... ':
            		{
            			document.getElementById("gradoGrupoBlog").className = "btn btn-primary";
            			document.getElementById("opcionesBusqueda").className = "box box-primary box-solid collapsed-box";
            			document.getElementById("parametrosBusquedaTarea").className = "btn btn-primary";
            			document.getElementById("gradoGrupoBlogAviso").className = "btn btn-primary";
            			document.getElementById("opcionesBusquedaAviso").className = "box box-primary box-solid collapsed-box";
            			document.getElementById("parametrosBusquedaTareaAviso").className = "btn btn-primary";
            		}
            		case 'Kinder':
            		{
            			if(tipo == "tarea")
            			{
	            			document.getElementById("gradoGrupoBlog").className = "btn btn-primary";
	            			document.getElementById("opcionesBusqueda").className = "box box-primary box-solid collapsed-box";
	            			document.getElementById("parametrosBusquedaTarea").className = "btn btn-primary";
	            		}
	            		else
	            		{
	            			document.getElementById("gradoGrupoBlogAviso").className = "btn btn-primary";
	            			document.getElementById("opcionesBusquedaAviso").className = "box box-primary box-solid collapsed-box";
	            			document.getElementById("parametrosBusquedaTareaAviso").className = "btn btn-primary";	
	            		}
            			break;
            		}
            		case 'Primaria':
            		{
            			if(tipo == "tarea")
            			{
	            			document.getElementById("gradoGrupoBlog").className = "btn btn-success";
	            			document.getElementById("opcionesBusqueda").className = "box box-success box-solid collapsed-box";
	            			document.getElementById("parametrosBusquedaTarea").className = "btn btn-success";
	            		}
	            		else
	            		{
	            			document.getElementById("gradoGrupoBlogAviso").className = "btn btn-success";
	            			document.getElementById("opcionesBusquedaAviso").className = "box box-success box-solid collapsed-box";
	            			document.getElementById("parametrosBusquedaTareaAviso").className = "btn btn-success";	
	            		}
            			break;
            		}
            		case 'Secundaria':
            		{
            			if(tipo == "tarea")
            			{
	            			document.getElementById("gradoGrupoBlog").className = "btn btn-warning";
	            			document.getElementById("opcionesBusqueda").className = "box box-warning box-solid collapsed-box";
	            			document.getElementById("parametrosBusquedaTarea").className = "btn btn-warning";
	            		}
	            		else
	            		{
	            			document.getElementById("gradoGrupoBlogAviso").className = "btn btn-warning";
	            			document.getElementById("opcionesBusquedaAviso").className = "box box-warning box-solid collapsed-box";
	            			document.getElementById("parametrosBusquedaTareaAviso").className = "btn btn-warning";	
	            		}
            			break;
            		}
            		case 'Bachillerato':
            		{
            			if(tipo == "tarea")
            			{
	            			document.getElementById("gradoGrupoBlog").className = "btn btn-danger";
	            			document.getElementById("opcionesBusqueda").className = "box box-danger box-solid collapsed-box";
	            			document.getElementById("parametrosBusquedaTarea").className = "btn btn-danger";
	            		}
	            		else
	            		{
	            			document.getElementById("gradoGrupoBlogAviso").className = "btn btn-danger";
	            			document.getElementById("opcionesBusquedaAviso").className = "box box-danger box-solid collapsed-box";
	            			document.getElementById("parametrosBusquedaTareaAviso").className = "btn btn-danger";	
	            		}
            			break;
            		}
            	}
				//debugger;
				this.arreglo = {};
				this.opcion = $("#escolaridadBlog option:selected").text().trim();
				var opcionGradoGrupo = $("#gradoGrupoBlog option:selected").text().trim();
				for (var i = 0; i < this.escolaridadesJson.resultadoAvisos.length; i++) 
				{
					if("Todos" == this.escolaridadesJson.resultadoAvisos[i].receptor || this.opcion == this.escolaridadesJson.resultadoAvisos[i].receptor || opcionGradoGrupo.trim() == this.escolaridadesJson.resultadoAvisos[i].receptor)
					{
						this.arreglo[i] = this.escolaridadesJson.resultadoAvisos[i];
					}
				};
				this.valorParametro = 1;
			},
// Blog Deportes
			panelDeportes: function ()
			{
				this.panelBlogOpciones = false;
				this.panelLoginInTareas = false;
				this.panelLoginInAvisos = false;
				this.panelLoginInCalendario = false;
				this.panelLoginInDeportes = true;
			},
			futbolDeportes: function ()
			{
				this.mensaje = "Deporte de fútbol | Bienvenido al Blog St John´s.";
				this.mostarPanelDeportes(true, false, false);
			},
			gimnasioDeportes: function ()
			{
				this.mensaje = "Gimnasio | Bienvenido al Blog St John´s.";
				this.mostarPanelDeportes(false, true, false);
			},
			natacionDeportes: function ()
			{
				this.mensaje = "Deporte de natación | Bienvenido al Blog St John´s.";
				this.mostarPanelDeportes(false, false, true);
			},
			mostarPanelDeportes: function (bandera1, bandera2, bandera3)
			{
				this.panelFutbolBlogDeportes = bandera1;
				this.panelGimnasioBlogDeportes = bandera2;
				this.panelNatacionBlogDeportes = bandera3;
			},
// Blog Calendario
			panelCalendario: function ()
			{
				this.mensaje = "Calendarios | Bienvenido al Blog St John´s."
				this.panelBlogOpciones = false;
				this.panelLoginInTareas = false;
				this.panelLoginInAvisos = false;
				this.panelLoginInDeportes = false;
				this.panelLoginInCalendario = true;
			},
			kinderCalendario: function()
			{
				this.mensaje = "Calendarios de kinder | Bienvenido al Blog St John´s."
				this.mostarPanelCalendario(true, false, false, false);
			},
			primariaCalendario: function()
			{
				this.mensaje = "Calendarios de primaria | Bienvenido al Blog St John´s."
				this.mostarPanelCalendario(false, true, false, false);
			},
			secundariaCalendario: function()
			{
				this.mensaje = "Calendarios de secundaria | Bienvenido al Blog St John´s."
				this.mostarPanelCalendario(false, false, true, false);
			},
			preparatoriaCalendario: function()
			{
				this.mensaje = "Calendarios de preparatoria | Bienvenido al Blog St John´s."
				this.mostarPanelCalendario(false, false, false, true);
			},
			mostarPanelCalendario: function(bandera1, bandera2, bandera3, bandera4)
			{
				this.panelCalendariosKinder = bandera1;
				this.panelCalendariosPrimaria = bandera2;
				this.panelCalendariosSecundaria = bandera3;
				this.panelCalendariosPreparatoria = bandera4;	
			},
// noticias
			getNoticias:function(){
					var resource = this.$resource("{{route('noticias.index')}}");
					resource.get({}).then(function (response) {
                	//alert(JSON.stringify(response.data))
                	this.$set('principalJson', response.data)
               		this.noticiasJson = this.principalJson
                	this.loading = false
                
           			});
			}
		}
	});
	// Filtrar la búsqueda por un id exacto
	Vue.filter('exactFilterBy', function(array, needle, inKeyword, key) {
			return array.filter(function(item) {
			return item[key] == needle;
		});
	});
</script>