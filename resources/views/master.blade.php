<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE-edge">
	<meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="google-site-verification" content="Sm6h-a4W7FCp1SimqfTc9a9ChrHzwF_wwJZix1cC1TY" />
	
	{!! Html::favicon('favicon.ico') !!}
	@include('inc/header_common')
	<title>  @yield('title') Blog escolar </title>
	<!-- Metas -->
	@yield('metas')
	<!-- Metas -->
</head>
	<body id="appStJohns" class="{{ cms_body_class() }}">
	    <div class="wrapper">
			@include('inc/header')
	        <!-- Left side column. contains the logo and sidebar -->
	        @include('inc/menu')
	        <!-- Content Wrapper. Contains page content -->
	        <div class="content-wrapper">
	            @yield('content')
	        </div><!-- /.content-wrapper -->
	    </div><!-- ./wrapper -->
		@include('inc/footer_common')
	    @include('inc/partials/scripts')
	</body>
</html>