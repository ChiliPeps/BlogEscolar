<meta name="author" content="{{ $noticia->autor }}">

{{-- "Open Graph" - Para Redes Sociales y mas --}}
<meta property="fb:app_id" content="667721110042956"/>
<meta property="og:locale" content="es_MX">
<meta property="og:type" content="article">
<meta property="og:title" content="{{ $noticia->titulo }} -">
<meta property="og:description" content="{{ $noticia->descripcion }}">
<meta property="og:url" content="{{ URL::to('/').'/'.$noticia->categoria.'/'.$noticia->id.'/'.$noticia->slugurl }}"/>
<meta property="og:site_name" content=""/>

{{-- Article Metas --}}
<meta property="article:publisher" content="https://www.facebook.com/"/>
{{-- @foreach($noticia->tags as $tag) --}}
{{-- <meta property="article:tag" content="{{$tag->tag->nombre}}"/> --}}
{{-- @endforeach --}}
<meta property="article:section" content="{{$noticia->categoria}}"/>
<meta property="article:published_time" content="{{$noticia->created_at}}"/>

{{-- Image --}}
<meta property="og:image" content="{{URL::to('/').'/'.$noticia->imagen}}"/>
<meta property="og:image:width" content="1000"/>
<meta property="og:image:height" content="670"/>

{{-- Twitter Metas --}}
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="{{$noticia->descripcion}}"/>
<meta name="twitter:title" content="{{$noticia->titulo}} - "/>
<meta name="twitter:site" content="@"/>
<meta name="twitter:image" content="{{URL::to('/').'/'.$noticia->imagen}}?w=240"/>
<meta name="twitter:creator" content="@"/>

