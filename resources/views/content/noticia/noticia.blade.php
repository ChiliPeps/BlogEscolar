<style>
	.center{
		margin-right: auto; max-width: 85%;
		margin-left: auto;  padding-top: 2%;
	}
	.titulo{
		text-align: center; font-size: 44px;
	}
	.parrafo img{
		width:100%;
	}
	.parrafo p{
		text-align: justify !important;
	}
	.right{float:right;}
	.white{
		color:white;
	}
	.fondowhite{
		background-color: white;
		text-align: center;
	}
	.comentarios{
		background-color:#9dc0ea;
		font-size: 22px;
		text-align: center;
	}
	.divition{
		border-right: 1px dotted #d2d1d1;
	}
</style>
<div class="container fondowhite">
	<div class="row">
		{{-- <div class="col-md-12"> --}}
			<div class="col-md-7">
				<h1 class="titulo">{{$noticia->titulo}}</h1>
			</div>
			<div class="col-md-5">
			</div>	
		{{-- </div> --}}
	</div>
	<div class="row">	
		{{-- <div class="col-md-12"> --}}
			<div class="col-md-7 divition">
				{!! Html::image($noticia->imagen, "map", ['class' => 'img-responsive center']) !!}
				<hr style="border-top: 1px dotted #d2d1d1;"></hr>
					<p>POR: <b> {{strtoupper($noticia->autor)}}</b>&nbsp;&nbsp; FECHA: <b>{{$noticia->fecha}}</b> <b class="right">{{$noticia->categoria}}</b></p>
				<hr style="border-bottom: 1px dotted #d2d1d1;"></hr>
				<div class="parrafo">{!!$noticia->contenido!!}</div>
			</div>
			<div class="col-md-5">

			</div>
		{{-- </div> --}}
	</div>
{{-- </div>



<div class="container"> --}}

	<div class="row">
		<br><br><br>
		<div class="col-md-7">
			<div style="display: flex;">
	{{-- Twitter --}}
			<div style="margin-right: 10px;">
				<a href="https://twitter.com/share" class="twitter-share-button" data-size="large" data-show-count="false">Tweet</a>
				<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
			</div>

	{{-- Facebook Like & Share --}}
			<div class="fb-share-button" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Compartir</a></div>

			&nbsp;&nbsp;&nbsp;
			{{-- WhatsApp button --}}
			<button type="button" class="btn btn-success" style="height:27px;"><a class="white" id="ga-whatsapp" href="whatsapp://send?text=Checa esta noticia: {{ URL::to('/').'/'.$noticia->categoria.'/'.$noticia->id.'/'.$noticia->slugurl }}" class="btn-whatsapp ga-whatsapp" data-action="share/whatsapp/share"><i class="ga-whatsapp fa fa-whatsapp"></i><span class="ga-whatsapp"> Enviar</span></a></button>
			</div>

			<br><br><br>
			<div class ="comentarios">Comentarios</div>
			<div id="fb-root"></div>
			<div class="fb-comments" data-href="{{ URL::to('/').'/'.$noticia->categoria.'/'.$noticia->id.'/'.$noticia->slugurl }}" data-width="100%" data-numposts="4"></div>
		</div>
	</div>
</div>

	

{{-- <div class="section-titulo bg-black"><p class="titulo-white">Comentarios</p></div> --}}
