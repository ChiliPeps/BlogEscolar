<section id="contacto">
	<div class="row">
		<div class="col-md-6" onload="carga_mapa()">
	        <h1>
	            Visitanos
	        </h1>
	        <hr style="border-color: rgba(29, 67, 121, 1);">
	        <div class="map-container">
	        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3641.249971510695!2d-110.34786728542983!3d24.127857984404663!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe755c2ef2e3888b5!2sColegio+Saint+Johns!5e0!3m2!1ses-419!2smx!4v1482525161222" width="90%" height="450" frameborder="0" style="border:0" allowfullscreen scrolling="no"></iframe>

				{{-- <iframe width="300" height="450" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed/v1/place?q=Status201%20Web%20Development&amp;key=AIzaSyCCGx7iveK21dme8OuLgX9Je7TUDDCw3_A"></iframe> --}}
			</div>
		</div>
		<div class="col-md-6">
	        <h1>
	            Contactanos
	        </h1>
	        <hr style="border-color: rgba(29, 67, 121, 1);">
	        <h5>
	        		<p>
			        	St John´s - Colegio La Paz
			        </p>
			        <p>
						<strong style="color: #FF7043;"> Teléfono: </strong> 123 - 456 - 7890
					</p>
					<p>
						<strong style="color: #FF7043;"> E-mail: </strong> info@mysite.com
					</p>
			</h5>
			<br>
			<form class="form-horizontal">
				<input type="text" class="form-control" id="nombrePersona" placeholder="Nombre">
				<input type="email" class="form-control" id="emailPersona" placeholder="ejemplo@ejemplo.com">
				<input type="phone" class="form-control" id="telefonoPersonas" placeholder="(612) 12 12345">
				<textarea class="form-control" rows="4" placeholder="Comentario"></textarea>
				<button type="submit" class="btn btn-primary"> Enviar </button>
			</form>
		</div>
	</div>
    @section('scripts')
		<script type="text/javascript"> 
			$('.map-container').click(function(){
				$(this).find('iframe').addClass('clicked')
			}).mouseleave(function(){
				$(this).find('iframe').removeClass('clicked')
			});
		</script>
    @stop
</section>
