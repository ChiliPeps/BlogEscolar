<style>
    .imgnoti {
        width:100%;
    }

    .decoration{
    color:black;
    text-decoration: none;
    }
    .not{
        border-bottom: 1px dotted #d2d1d1;
        margin-top: 30px;
    }
    .white
    {
        background-color: white;
    }
    .orange{
        color:#048daf;
    }
    .imgcentro img{
        padding-top:15px ;
        padding-bottom:30px;
    }
    .noticiasmas{
        /*width: 100%;*/
        width:245px;
        height: 190px;
        background-repeat: no-repeat;
        background-size: cover;
        position: relative;
    }
    .noticiasmas p
    {
        text-shadow: 1px 0 0 #000, 0 -1px 0 #000, 0 1px 0 #000, -1px 0 0 #000;
        color:white;
        font-size: 13px;
    }
    .mm{
        position: absolute;
        bottom: 0;
        right: 0;
    }
    .center{
            text-align: -webkit-center;
    }
</style>

{{-- christian  --}}
<section id="noticias ">
    {{-- <div class="container"> --}}
    <div class="row">
        <div id="noticiasLeft" class="col-md-9 white" >

                <div class="row not" v-for= "n in noticiasJson">
                
                    <div class="col-md-4 imgcentro">
                        <div>
                            {{-- {!! Html::image($noticia->imagen, "map", ['class' => 'img-responsive']) !!} --}}
                            <img :src="url_help + n.imagen" class="img-rounded imgnoti"/>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <a href="@{{url_help+n.categoria+'/'+n.id+'/'+n.slugurl}}">  <h3 class = "decoration"><b>@{{n.titulo}} </b></h3> </a> 
                                           
                        <p class="text-justify">
                            @{{n.descripcion}}
                            <p class="text-right">
                                <a href="@{{url_help+n.categoria+'/'+n.id+'/'+n.slugurl}}" class="btn btn-info" role="button"> 
                                    <strong>
                                        Ver Noticia
                                    </strong>
                                </a>
                            </p>
                        </p>
                        <h5 class="orange"> Por:  @{{n.autor}}   | @{{ n.categoria }}   | @{{ n.created_at.substr(0, 11) }}  </h5> 
                    </div>
                </div>

                <br><br>

        </div>
        {{-- <div class="container"> --}}
        <div id="noticiasRight" class="col-md-3 text-justify">
            <h4>
                <strong> 
                    Noticias más destacas en <br> St John´s 
                </strong>
            </h4>
            
            <br>
                 <div class="col-md-12 center" v-for= "n in noticiasJson">                     
                            <div class="noticiasmas" :style="{ 'background-image': 'url('+url_help+n.imagen+')' }">
                                <div class = "mm">
                                    <p><strong> @{{n.titulo}} </strong></p>
                                </div>                               
                            </div>       
                            <br>     
                    <p class="text-center">
                        <a href="@{{url_help+n.categoria+'/'+n.id+'/'+n.slugurl}}" class="btn btn-info" role="button"> 
                            <strong>
                                Leer Noticia
                            </strong>
                        </a>
                    </p>
                <hr style="border-color: #bbb7b7 ;">

                </div>
        </div>
{{-- </div> --}}
</section>




{{-- diseno del christian --}}
{{-- <section id="noticias">
    <div class="row">
        <div id="noticiasLeft" class="col-md-9" >
                <div class="row noticia" v-for= "n in noticiasJson">
                    <div class="col-md-6">
                        <div class="imgNoticias imgnoti"  >
                        <div class="imgNoticias">
                            <img :src="url_help + n.imagen" class="img-rounded img-responsive"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3> @{{n.titulo}} </h3>
                        <h5 class="text-right"> @{{ n.categoria }} | @{{ n.created_at }}  </h5>
                        <hr style="border-color: #5f9ecc ; border-width: 3px; box-shadow: 0px 0px 7px 0px #8c8c8c ;">
                        <p class="text-justify">
                            @{{n.descripcion}}
                            <p class="text-right">
                                <a href="@{{url_help+n.categoria+'/'+n.id+'/'+n.slugurl}}" class="btn btn-info" role="button"> 
                                    <strong>
                                        Leer más
                                    </strong>
                                </a>
                            </p>
                        </p>
                    </div>
                </div>
                <br><br>        
        </div>
        <div id="noticiasRight" class="col-md-3 text-justify">
            <h4>
                <strong> 
                    Noticias más destacas en <br> St John´s 
                </strong>
            </h4>
            <h5> <strong style="color: #FF7043 ;"> ¡ </strong> Para estar al tanto de la novedades en el colegio <strong style="color: #FF7043 ;"> ! </strong> </h5>
            <br>
         
                <strong style="color: #FF7043 ;">
           
                </strong>
                <p>
                   
                </p>
                <p class="text-right">
                    <a href="#" class="btn btn-info" role="button"> 
                        <strong>
                            Leer más
                        </strong>
                    </a>
                </p>
                <hr style="border-color: #bbb7b7 ;">
                <br><br>
        </div>
		<div id="noticiasRight" class="col-md-3 text-justify">
			<h4>
				<strong> 
					Noticias más destacas en <br> St John´s 
				</strong>
			</h4>
			<h5> <strong style="color: #FF7043;"> ¡ </strong> Para estar al tanto de la novedades en el colegio <strong style="color: #FF7043;"> ! </strong> </h5>
			<br>
			@foreach($noticiasTop as $noticiaTop)
				<div class="row">
					<div class="col-md-12">
						{!! Html::image($noticiaTop->imagen, "map", ['class' => 'img-responsive']) !!}
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12">
						<strong style="color: #FF7043;">
							{{ $noticiaTop->titulo }}
						</strong>
						<br>
						<p class="text-right">
							<strong>
								{{$noticiaTop->categoria }} | {{ $noticiaTop->fecha }}
							</strong>
						</p>
						<p class="text-right">
							<a href="#" class="btn btn-info" role="button"> 
								<strong>
									Leer más
								</strong>
							</a>
						</p>
					</div>
				</div>
				<hr style="border-color: #bbb7b7;">
			@endforeach
		</div>
    </div>
</section> --}}