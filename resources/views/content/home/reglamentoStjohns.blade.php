<section id="reglamentoStJohns">
	<div class="splash">
			<div class="center">
				<div class="bookshelf">
					<div class="shelf">
						<div class="row-1">
							<div class="loc">
								<div> <div class="sample thumb4" sample="magazine1"></div> </div>
								<div> <div class="sample thumb5" sample="magazine2"></div> </div>
								<div> <div class="sample thumb6" sample="magazine3"></div> </div>
							</div>
						</div>
						<div class="row-2">
							<div class="loc">
								<div> <div class="sample thumb7" sample="magazine4"></div> </div>
								<div> <div class="sample thumb8" sample="magazine5"></div> </div>
							</div>
						</div>			
					</div>
				</div>
				<!-- Samples-->
				<div class="samples">
					<div class="bar">
						<a class="icon quit"></a>
					</div>
					<div id="book-wrapper">
						<div id="book-zoom"></div>
					</div>
					<div id="slider-bar" class="turnjs-slider">
						<div id="slider"></div>
					</div>
				</div> <!-- End samples -->		
			</div>  <!-- end div center -->
		<div class="gradient"></div>
	</div> <!-- div splash -->
    @section('scripts')
    @stop
</section>