<section id="blog">
<!-- Panel de Opciones del Blog -->
 	<div v-if="panelBlogOpciones" id="opcionesBlog">
		<div class="row">
			<div class="col-sm-2 col-md-2"></div>
			<div class="col-sm-4 col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading" style="background-color: #a8def9">
						<h3>
							Tareas
						</h3>
					</div>
					<div class="panel-body text-center panelOpcionesFont">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-excel-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-pdf-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-word-o"></i>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-text"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-powerpoint-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-video-o"></i>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button @click="panelTareas" class="btn btn-block btn-primary">
							<strong> Ingresar </strong>
						</button>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-4">
				<div class="panel panel-success">
					<div class="panel-heading" style="background-color:#a7ff83">
						<h3>
							Avisos
						</h3>
					</div>
					<div class="panel-body text-center panelOpcionesFont">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-comments-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-smile-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button @click="panelAvisos" class="btn btn-block btn-primary">
							<strong> Ingresar </strong>
						</button>
					</div>
				</div>
			</div>
			<div class="col-sm-2 col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-sm-2 col-md-2"></div>
			<div class="col-sm-4 col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading" style="background-color:#a7ff83">
						<h3>
							Calendario
						</h3>
					</div>
					<div class="panel-body text-center panelOpcionesFont">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-calendar"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-calendar-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button @click="panelCalendario" class="btn btn-block btn-primary">
							<strong> Ingresar </strong>
						</button>
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-md-4">
				<div class="panel panel-success">
					<div class="panel-heading" style="background-color: #a8def9">
						<h3>
							Deportes
						</h3>
					</div>
					<div class="panel-body text-center panelOpcionesFont">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-futbol-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-life-bouy"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button @click="panelDeportes" class="btn btn-block btn-primary"></style>
							<strong> Ingresar </strong>
						</button>
					</div>
				</div>
			</div>
			<div class="col-sm-2 col-md-2"></div>
		</div>
	</div>
<!-- Panel de Tareas del Blog -->
	<div v-show="panelLoginInTareas">
<!-- Panel Tareas | Kinder -->
		<div v-if="panelKinderBlog">
			<div v-if="valorParametro == 0">
				<span v-for="tarea in escolaridadesJson.resultadoTarea | exactFilterBy idEscolaridadGrupoGrado in buscarTarea">
					<div class="box box-primary box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-md-3">
									<strong>
										<span>Profesor(a):</span> 
									</strong>
									<label> @{{{ tarea.profesor }}} </label>
									<br>
									<strong>
										<span>Tipo:</span> 
									</strong>
									<label> @{{{ tarea.tipo }}} </label>
									<br>
									<strong>
										<span>Fecha de entrega:</span> 
									</strong>
									<label> @{{{ tarea.fechaEntrega }}} </label>
								</div>
								<div class="col-md-9 text-left">
									<strong>
										<span>Materia:</span> 
									</strong>
									@{{{ tarea.materia }}}
									<br>
									<strong> 
										<span>Recomendaciones:</span>
									</strong>
									@{{{ tarea.recomendaciones }}}
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ tarea.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ tarea.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ tarea.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
			<div v-else>
				<span v-for="tarea in arreglo | filterBy busquedaTareaTipo in parametrosBusqueda">
					<div class="box box-primary box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-md-3">
									<strong>
										<span>Profesor(a):</span> 
									</strong>
									<label> @{{{ tarea.profesor }}} </label>
									<br>
									<strong>
										<span>Tipo:</span> 
									</strong>
									<label> @{{{ tarea.tipo }}} </label>
									<br>
									<strong>
										<span>Fecha de entrega:</span> 
									</strong>
									<label> @{{{ tarea.fechaEntrega }}} </label>
								</div>
								<div class="col-md-9 text-left">
									<strong>
										<span>Materia:</span> 
									</strong>
									@{{{ tarea.materia }}}
									<br>
									<strong> 
										<span>Recomendaciones:</span>
									</strong>
									@{{{ tarea.recomendaciones }}}
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ tarea.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ tarea.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ tarea.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
		</div>
<!-- Panel Tareas | Primaria -->
		<div v-if="panelPrimariaBlog">
			<div v-if="valorParametro == 0">
            	<span v-for="tarea in escolaridadesJson.resultadoTarea | exactFilterBy idEscolaridadGrupoGrado in buscarTarea">
				<div class="box box-success box-solid">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-md-3">
								<strong>
									<span>Profesor(a):</span> 
								</strong>
								<label> @{{{ tarea.profesor }}} </label>
								<br>
								<strong>
									<span>Tipo:</span> 
								</strong>
								<label> @{{{ tarea.tipo }}} </label>
								<br>
								<strong>
									<span>Fecha de entrega:</span> 
								</strong>
								<label> @{{{ tarea.fechaEntrega }}} </label>
							</div>
							<div class="col-md-9 text-left">
								<strong>
									<span>Materia:</span> 
								</strong>
								@{{{ tarea.materia }}}
								<br>
								<strong> 
									<span>Recomendaciones:</span>
								</strong>
								@{{{ tarea.recomendaciones }}}
							</div>
						</div>
					</div>
					<div class="box-body imagenContenidoBlog"> 
						<strong> @{{{ tarea.contenido }}} </strong>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-md-6">
								<strong>
									<span>Publicado por:</span> 
								</strong>
								@{{{ tarea.name }}}
							</div>
							<div class="col-md-6 text-right">
								<strong>
									<span>Fecha publicación:</span> 
								</strong>
								@{{{ tarea.fechaAlta }}}
							</div>
						</div>
					</div>
					</div>
				</span>
			</div>
			<div v-else>
	            <span v-for="tarea in arreglo | filterBy busquedaTareaTipo in parametrosBusqueda">
					<div class="box box-success box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-md-3">
									<strong>
										<span>Profesor(a):</span> 
									</strong>
									<label> @{{{ tarea.profesor }}} </label>
									<br>
									<strong>
										<span>Tipo:</span> 
									</strong>
									<label> @{{{ tarea.tipo }}} </label>
									<br>
									<strong>
										<span>Fecha de entrega:</span> 
									</strong>
									<label> @{{{ tarea.fechaEntrega }}} </label>
								</div>
								<div class="col-md-9 text-left">
									<strong>
										<span>Materia:</span> 
									</strong>
									@{{{ tarea.materia }}}
									<br>
									<strong> 
										<span>Recomendaciones:</span>
									</strong>
									@{{{ tarea.recomendaciones }}}
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ tarea.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ tarea.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ tarea.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
		</div>
<!-- Panel Tareas | Secundaria -->
		<div v-if="panelSecundariaBlog">
			<div v-if="valorParametro == 0">
	            <span v-for="tarea in escolaridadesJson.resultadoTarea | exactFilterBy idEscolaridadGrupoGrado in buscarTarea">
					<div class="box box-warning box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-md-3">
									<strong>
										<span>Profesor(a):</span> 
									</strong>
									<label> @{{{ tarea.profesor }}} </label>
									<br>
									<strong>
										<span>Tipo:</span> 
									</strong>
									<label> @{{{ tarea.tipo }}} </label>
									<br>
									<strong>
										<span>Fecha de entrega:</span> 
									</strong>
									<label> @{{{ tarea.fechaEntrega }}} </label>
								</div>
								<div class="col-md-9 text-left">
									<strong>
										<span>Materia:</span> 
									</strong>
									@{{{ tarea.materia }}}
									<br>
									<strong> 
										<span>Recomendaciones:</span>
									</strong>
									@{{{ tarea.recomendaciones }}}
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ tarea.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ tarea.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ tarea.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
			<div v-else>
	            <span v-for="tarea in arreglo | filterBy busquedaTareaTipo in parametrosBusqueda">
					<div class="box box-warning box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-md-3">
									<strong>
										<span>Profesor(a):</span> 
									</strong>
									<label> @{{{ tarea.profesor }}} </label>
									<br>
									<strong>
										<span>Tipo:</span> 
									</strong>
									<label> @{{{ tarea.tipo }}} </label>
									<br>
									<strong>
										<span>Fecha de entrega:</span> 
									</strong>
									<label> @{{{ tarea.fechaEntrega }}} </label>
								</div>
								<div class="col-md-9 text-left">
									<strong>
										<span>Materia:</span> 
									</strong>
									@{{{ tarea.materia }}}
									<br>
									<strong> 
										<span>Recomendaciones:</span>
									</strong>
									@{{{ tarea.recomendaciones }}}
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ tarea.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ tarea.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ tarea.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
		</div>
<!-- Panel Tareas | Preparatoria -->
		<div v-if="panelPreparatoriaBlog">
			<div v-if="valorParametro == 0">
	            <span v-for="tarea in escolaridadesJson.resultadoTarea | exactFilterBy idEscolaridadGrupoGrado in buscarTarea">
					<div class="box box-danger box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-md-3">
									<strong>
										<span>Profesor(a):</span> 
									</strong>
									<label> @{{{ tarea.profesor }}} </label>
									<br>
									<strong>
										<span>Tipo:</span> 
									</strong>
									<label> @{{{ tarea.tipo }}} </label>
									<br>
									<strong>
										<span>Fecha de entrega:</span> 
									</strong>
									<label> @{{{ tarea.fechaEntrega }}} </label>
								</div>
								<div class="col-md-9 text-left">
									<strong>
										<span>Materia:</span> 
									</strong>
									@{{{ tarea.materia }}}
									<br>
									<strong> 
										<span>Recomendaciones:</span>
									</strong>
									@{{{ tarea.recomendaciones }}}
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ tarea.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ tarea.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ tarea.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
			<div v-else>
	            <span v-for="tarea in arreglo | filterBy busquedaTareaTipo in parametrosBusqueda">
					<div class="box box-danger box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-md-3">
									<strong>
										<span>Profesor(a):</span> 
									</strong>
									<label> @{{{ tarea.profesor }}} </label>
									<br>
									<strong>
										<span>Tipo:</span> 
									</strong>
									<label> @{{{ tarea.tipo }}} </label>
									<br>
									<strong>
										<span>Fecha de entrega:</span> 
									</strong>
									<label> @{{{ tarea.fechaEntrega }}} </label>
								</div>
								<div class="col-md-9 text-left">
									<strong>
										<span>Materia:</span> 
									</strong>
									@{{{ tarea.materia }}}
									<br>
									<strong> 
										<span>Recomendaciones:</span>
									</strong>
									@{{{ tarea.recomendaciones }}}
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ tarea.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ tarea.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ tarea.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
		</div>
	</div>
<!-- Panel de Avisos del Blog -->
	<div v-show="panelLoginInAvisos">
		<div v-if="valorParametro == 0">
	        <span v-for="aviso in escolaridadesJson.resultadoAvisos | filterBy buscarAvisoReceptor in avisoReceptor">
				<div class="box box-default box-solid">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<strong>
									<span>Título:</span> 
								</strong>
								<label> @{{{ aviso.titulo }}} </label>
								<br>
								<strong>
									<span>Dirigido a:</span> 
								</strong>
								<label> @{{{ aviso.receptor }}} </label>
							</div>
						</div>
					</div>
					<div class="box-body imagenContenidoBlog"> 
						<strong> @{{{ aviso.contenido }}} </strong>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-md-6">
								<strong>
									<span>Publicado por:</span> 
								</strong>
								@{{{ aviso.name }}}
							</div>
							<div class="col-md-6 text-right">
								<strong>
									<span>Fecha publicación:</span> 
								</strong>
								@{{{ aviso.fechaAlta }}}
							</div>
						</div>
					</div>
				</div>
			</span>
<!-- Panel Avisos | Kinder -->
			<div v-if="panelAvisosKinder">
		        <span v-for="avisoEscolaridad in escolaridadesJson.resultadoAvisos | exactFilterBy nivelEducativo in avisoReceptor">
					<div class="box box-primary box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
		        <span v-for="avisoEscolaridad in escolaridadesJson.resultadoAvisos | exactFilterBy grupoGrado in avisoReceptor">
					<div class="box box-primary box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
<!-- Panel Avisos | Primaria -->
			<div v-if="panelAvisosPrimaria">
		        <span v-for="avisoEscolaridad in escolaridadesJson.resultadoAvisos | exactFilterBy nivelEducativo in avisoReceptor">
					<div class="box box-success box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
		        <span v-for="avisoEscolaridad in escolaridadesJson.resultadoAvisos | exactFilterBy grupoGrado in avisoReceptor">
					<div class="box box-success box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
<!-- Panel Avisos | Secundaria -->
			<div v-if="panelAvisosSecundaria">
		        <span v-for="avisoEscolaridad in escolaridadesJson.resultadoAvisos | exactFilterBy nivelEducativo in avisoReceptor">
					<div class="box box-warning box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
		        <span v-for="avisoEscolaridad in escolaridadesJson.resultadoAvisos | exactFilterBy grupoGrado in avisoReceptor">
					<div class="box box-warning box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
<!-- Panel Avisos | Preparatoria -->
			<div v-if="panelAvisosBachillerato">
		        <span v-for="avisoEscolaridad in escolaridadesJson.resultadoAvisos | exactFilterBy nivelEducativo in avisoReceptor">
					<div class="box box-danger box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
		        <span v-for="avisoEscolaridad in escolaridadesJson.resultadoAvisos | exactFilterBy grupoGrado in avisoReceptor">
					<div class="box box-danger box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
		</div>
		<div v-else>
	        {{-- <span v-for="aviso in arreglo | filterBy fechaAviso in avisoFecha">
				<div class="box box-default box-solid">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<strong>
									<span>Título:</span> 
								</strong>
								<label> @{{{ aviso.titulo }}} </label>
								<br>
								<strong>
									<span>Dirigido a:</span> 
								</strong>
								<label> @{{{ aviso.receptor }}} </label>
							</div>
						</div>
					</div>
					<div class="box-body"> 
						<strong> @{{{ aviso.contenido }}} </strong>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-md-6">
								<strong>
									<span>Publicado por:</span> 
								</strong>
								@{{{ aviso.name }}}
							</div>
							<div class="col-md-6 text-right">
								<strong>
									<span>Fecha publicación:</span> 
								</strong>
								@{{{ aviso.fechaAlta }}}
							</div>
						</div>
					</div>
				</div>
			</span> --}}
<!-- Panel Avisos | Kinder -->
			<div v-if="panelAvisosKinder">
		        <span v-for="avisoEscolaridad in arreglo | filterBy fechaAviso in avisoFecha">
					<div class="box box-primary box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
<!-- Panel Avisos | Primaria -->
			<div v-if="panelAvisosPrimaria">
		        <span v-for="avisoEscolaridad in arreglo | filterBy fechaAviso in avisoFecha">
					<div class="box box-success box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
<!-- Panel Avisos | Secundaria -->
			<div v-if="panelAvisosSecundaria">
		        <span v-for="avisoEscolaridad in arreglo | filterBy fechaAviso in avisoFecha">
					<div class="box box-warning box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
<!-- Panel Avisos | Preparatoria -->
			<div v-if="panelAvisosBachillerato">
		        <span v-for="avisoEscolaridad in arreglo | filterBy fechaAviso in avisoFecha">
					<div class="box box-danger box-solid">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-xs-12 col-md-12">
									<strong>
										<span>Título:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.titulo }}} </label>
									<br>
									<strong>
										<span>Dirigido a:</span> 
									</strong>
									<label> @{{{ avisoEscolaridad.receptor }}} </label>
								</div>
							</div>
						</div>
						<div class="box-body imagenContenidoBlog"> 
							<strong> @{{{ avisoEscolaridad.contenido }}} </strong>
						</div>
						<div class="box-footer">
							<div class="row">
								<div class="col-md-6">
									<strong>
										<span>Publicado por:</span> 
									</strong>
									@{{{ avisoEscolaridad.name }}}
								</div>
								<div class="col-md-6 text-right">
									<strong>
										<span>Fecha publicación:</span> 
									</strong>
									@{{{ avisoEscolaridad.fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</span>
			</div>
		</div>
		{{-- <pre> @{{ escolaridadesJson.resultadoAvisos | json }} </pre> --}}
	</div>
<!-- Panel de Deportes del Blog -->
	<div v-show="panelLoginInDeportes">
		<div class="row">
			<div class="col-md-12 text-right">
				<button class="btn btn-default">
					<strong @click="panelOpciones" style="cursor:pointer;"> 
						<i class="fa fa-arrow-circle-left"> </i> Regresar 
					</strong>
				</button>
				<button @click="futbolDeportes" type="button" class="btn btn-primary text-uppercase">
					<strong>Fútbol</strong>
				</button>
				<button @click="gimnasioDeportes" type="button" class="btn btn-success text-uppercase">
					<strong>Gimnasio</strong>
				</button>
				<button @click="natacionDeportes" type="button" class="btn btn-warning text-uppercase">
					<strong>Natación</strong>
				</button>
			</div>
		</div>
		<br>
<!-- Panel Deportes | Fútbol -->
		<div v-show="panelFutbolBlogDeportes">
			<span v-for="futbol in escolaridadesJson.deportesFutbol">
				<div class="box box-primary box-solid">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<strong>
									<span>Título:</span> 
								</strong>
								<label> @{{{ futbol.tipoDeporte }}} </label>
								<br>
							</div>
						</div>
					</div>
					<div class="box-body"> 
						<strong> @{{{ futbol.contenido }}} </strong>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-md-6">
								<strong>
									<span>Publicado por:</span> 
								</strong>
								@{{{ futbol.name }}}
							</div>
							<div class="col-md-6 text-right">
								<strong>
									<span>Fecha publicación:</span> 
								</strong>
								@{{{ futbol.fechaAlta }}}
							</div>
						</div>
					</div>
				</div>
			</span>
		</div>
<!-- Panel Deportes | Gimnasio -->
		<div v-show="panelGimnasioBlogDeportes">
			<span v-for="gimnasio in escolaridadesJson.deportesGimnasio">
				<div class="box box-success box-solid">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<strong>
									<span>Título:</span> 
								</strong>
								<label> @{{{ gimnasio.tipoDeporte }}} </label>
								<br>
							</div>
						</div>
					</div>
					<div class="box-body"> 
						<strong> @{{{ gimnasio.contenido }}} </strong>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-md-6">
								<strong>
									<span>Publicado por:</span> 
								</strong>
								@{{{ gimnasio.name }}}
							</div>
							<div class="col-md-6 text-right">
								<strong>
									<span>Fecha publicación:</span> 
								</strong>
								@{{{ gimnasio.fechaAlta }}}
							</div>
						</div>
					</div>
				</div>
			</span>
		</div>
<!-- Panel Deportes | Natación -->
		<div v-show="panelNatacionBlogDeportes">
			<span v-for="natacion in escolaridadesJson.deportesNatacion">
				<div class="box box-warning box-solid">
					<div class="box-header with-border">
						<div class="row">
							<div class="col-xs-12 col-md-12">
								<strong>
									<span>Título:</span> 
								</strong>
								<label> @{{{ natacion.tipoDeporte }}} </label>
								<br>
							</div>
						</div>
					</div>
					<div class="box-body"> 
						<strong> @{{{ natacion.contenido }}} </strong>
					</div>
					<div class="box-footer">
						<div class="row">
							<div class="col-md-6">
								<strong>
									<span>Publicado por:</span> 
								</strong>
								@{{{ natacion.name }}}
							</div>
							<div class="col-md-6 text-right">
								<strong>
									<span>Fecha publicación:</span> 
								</strong>
								@{{{ natacion.fechaAlta }}}
							</div>
						</div>
					</div>
				</div>
			</span>
		</div>
	</div>
<!-- Panel de Calendario del Blog -->
	<div v-show="panelLoginInCalendario">
		<div class="row">
			<div class="col-md-12 text-right">
				<button class="btn btn-default">
					<strong @click="panelOpciones" style="cursor:pointer;"> 
						<i class="fa fa-arrow-circle-left"> </i> Regresar 
					</strong>
				</button>
				<button @click="kinderCalendario" type="button" class="btn btn-primary text-uppercase">
					<strong>Kinder</strong>
				</button>
				<button @click="primariaCalendario" type="button" class="btn btn-success text-uppercase">
					<strong>Primaria</strong>
				</button>
				<button @click="secundariaCalendario" type="button" class="btn btn-warning text-uppercase">
					<strong>Secundaría</strong>
				</button>
				<button @click="preparatoriaCalendario" type="button" class="btn btn-danger text-uppercase">
					<strong>Preparatoria</strong>
				</button>
			</div>
		</div>
		<br>
<!-- Panel Calendario | Kinder -->
		<div v-show="panelCalendariosKinder" class="row">
			
			<div class="col-xs-10 col-md-10 imgCalendarioMensual img-responsive img-thumbnail" data-toggle="modal" data-target="#kinderCalendarioMensual">
				<h4>
					@{{{ escolaridadesJson.calendariosKinder[0].titulo }}}
				</h4>
				@{{{ escolaridadesJson.calendariosKinder[0].contenido }}}
			</div>
			<!-- Modal Mensual -->
			<div class="modal fade bs-example-modal-lg" id="kinderCalendarioMensual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"> 
								@{{{ escolaridadesJson.calendariosKinder[0].titulo }}} de @{{{ escolaridadesJson.calendariosKinder[0].receptor }}}. 
							</h4>
						</div>
						<div class="modal-body imgCalendario img-responsive">
							<div class="row">
								<div class="col-md-12">
									@{{{ escolaridadesJson.calendariosKinder[0].contenido }}}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-md-6">
								</div>
								<div class="col-md-6">
									Fecha de publicación: @{{{ escolaridadesJson.calendariosKinder[0].fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-2 col-md-2 imgCalendarioAnual img-responsive img-thumbnail" data-toggle="modal" data-target="#kinderCalendarioAnual">
				<h4>
					@{{{ escolaridadesJson.calendariosKinder[1].titulo }}}
				</h4>
				@{{{ escolaridadesJson.calendariosKinder[1].contenido }}}
			</div>
			<!-- Modal Anual -->
			<div class="modal fade bs-example-modal-lg" id="kinderCalendarioAnual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"> 
								@{{{ escolaridadesJson.calendariosKinder[1].titulo }}} de @{{{ escolaridadesJson.calendariosKinder[1].receptor }}}. 
							</h4>
						</div>
						<div class="modal-body imgCalendario img-responsive">
							<div class="row">
								<div class="col-md-12">
									@{{{ escolaridadesJson.calendariosKinder[1].contenido }}}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-md-6">
								</div>
								<div class="col-md-6">
									Fecha de publicación: @{{{ escolaridadesJson.calendariosKinder[1].fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- Panel Calendario | Primaria -->
		<div v-show="panelCalendariosPrimaria" class="row">			
			<div class="col-xs-10 col-md-10 imgCalendarioMensual img-responsive img-thumbnail" data-toggle="modal" data-target="#primariaCalendarioMensual">
				<h4>
					@{{{ escolaridadesJson.calendariosPrimaria[0].titulo }}}
				</h4>
				@{{{ escolaridadesJson.calendariosPrimaria[0].contenido }}}
			</div>
			<!-- Modal Mensual -->
			<div class="modal fade bs-example-modal-lg" id="primariaCalendarioMensual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"> 
								@{{{ escolaridadesJson.calendariosPrimaria[0].titulo }}} de @{{{ escolaridadesJson.calendariosPrimaria[0].receptor }}}. 
							</h4>
						</div>
						<div class="modal-body imgCalendario img-responsive">
							<div class="row">
								<div class="col-md-12">
									@{{{ escolaridadesJson.calendariosPrimaria[0].contenido }}}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-md-6">
								</div>
								<div class="col-md-6">
									Fecha de publicación: @{{{ escolaridadesJson.calendariosPrimaria[0].fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-2 col-md-2 imgCalendarioAnual img-responsive img-thumbnail" data-toggle="modal" data-target="#primariaCalendarioAnual">
				<h4>
					@{{{ escolaridadesJson.calendariosPrimaria[1].titulo }}}
				</h4>
				@{{{ escolaridadesJson.calendariosPrimaria[1].contenido }}}
			</div>
			<!-- Modal Anual -->
			<div class="modal fade bs-example-modal-lg" id="primariaCalendarioAnual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"> 
								@{{{ escolaridadesJson.calendariosPrimaria[1].titulo }}} de @{{{ escolaridadesJson.calendariosPrimaria[1].receptor }}}. 
							</h4>
						</div>
						<div class="modal-body imgCalendario img-responsive">
							<div class="row">
								<div class="col-md-12">
									@{{{ escolaridadesJson.calendariosPrimaria[1].contenido }}}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-md-6">
								</div>
								<div class="col-md-6">
									Fecha de publicación: @{{{ escolaridadesJson.calendariosPrimaria[1].fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- Panel Calendario | Secundaria -->
		<div v-show="panelCalendariosSecundaria" class="row">			
			<div class="col-xs-10 col-md-10 imgCalendarioMensual img-responsive img-thumbnail" data-toggle="modal" data-target="#secundariaCalendarioMensual">
				<h4>
					@{{{ escolaridadesJson.calendariosSecundaria[0].titulo }}}
				</h4>
				@{{{ escolaridadesJson.calendariosSecundaria[0].contenido }}}
			</div>
			<!-- Modal Mensual -->
			<div class="modal fade bs-example-modal-lg" id="secundariaCalendarioMensual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"> 
								@{{{ escolaridadesJson.calendariosSecundaria[0].titulo }}} de @{{{ escolaridadesJson.calendariosSecundaria[0].receptor }}}. 
							</h4>
						</div>
						<div class="modal-body imgCalendario img-responsive">
							<div class="row">
								<div class="col-md-12">
									@{{{ escolaridadesJson.calendariosSecundaria[0].contenido }}}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-md-6">
								</div>
								<div class="col-md-6">
									Fecha de publicación: @{{{ escolaridadesJson.calendariosSecundaria[0].fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-2 col-md-2 imgCalendarioAnual img-responsive img-thumbnail" data-toggle="modal" data-target="#secundariaCalendarioAnual">
				<h4>
					@{{{ escolaridadesJson.calendariosSecundaria[1].titulo }}}
				</h4>
				@{{{ escolaridadesJson.calendariosSecundaria[1].contenido }}}
			</div>
			<!-- Modal Anual -->
			<div class="modal fade bs-example-modal-lg" id="secundariaCalendarioAnual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"> 
								@{{{ escolaridadesJson.calendariosSecundaria[1].titulo }}} de @{{{ escolaridadesJson.calendariosSecundaria[1].receptor }}}. 
							</h4>
						</div>
						<div class="modal-body imgCalendario img-responsive">
							<div class="row">
								<div class="col-md-12">
									@{{{ escolaridadesJson.calendariosSecundaria[1].contenido }}}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-md-6">
								</div>
								<div class="col-md-6">
									Fecha de publicación: @{{{ escolaridadesJson.calendariosSecundaria[1].fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- Panel Calendario | Preparatoria -->
		<div v-show="panelCalendariosPreparatoria" class="row">			
			<div class="col-xs-10 col-md-10 imgCalendarioMensual img-responsive img-thumbnail" data-toggle="modal" data-target="#preparatoriaCalendarioMensual">
				<h4>
					@{{{ escolaridadesJson.calendariosPreparatoria[0].titulo }}}
				</h4>
				@{{{ escolaridadesJson.calendariosPreparatoria[0].contenido }}}
			</div>
			<!-- Modal Mensual -->
			<div class="modal fade bs-example-modal-lg" id="preparatoriaCalendarioMensual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"> 
								@{{{ escolaridadesJson.calendariosPreparatoria[0].titulo }}} de @{{{ escolaridadesJson.calendariosPreparatoria[0].receptor }}}. 
							</h4>
						</div>
						<div class="modal-body imgCalendario img-responsive">
							<div class="row">
								<div class="col-md-12">
									@{{{ escolaridadesJson.calendariosPreparatoria[0].contenido }}}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-md-6">
								</div>
								<div class="col-md-6">
									Fecha de publicación: @{{{ escolaridadesJson.calendariosPreparatoria[0].fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-2 col-md-2 imgCalendarioAnual img-responsive img-thumbnail" data-toggle="modal" data-target="#preparatoriaCalendarioAnual">
				<h4>
					@{{{ escolaridadesJson.calendariosPreparatoria[1].titulo }}}
				</h4>
				@{{{ escolaridadesJson.calendariosPreparatoria[1].contenido }}}
			</div>
			<!-- Modal Anual -->
			<div class="modal fade bs-example-modal-lg" id="preparatoriaCalendarioAnual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"> 
								@{{{ escolaridadesJson.calendariosPreparatoria[1].titulo }}} de @{{{ escolaridadesJson.calendariosPreparatoria[1].receptor }}}. 
							</h4>
						</div>
						<div class="modal-body imgCalendario img-responsive">
							<div class="row">
								<div class="col-md-12">
									@{{{ escolaridadesJson.calendariosPreparatoria[1].contenido }}}
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="row">
								<div class="col-md-6">
								</div>
								<div class="col-md-6">
									Fecha de publicación: @{{{ escolaridadesJson.calendariosPreparatoria[1].fechaAlta }}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- <pre> @{{ escolaridadesJson.calendariosKinder | json }} </pre> --}}
</section>