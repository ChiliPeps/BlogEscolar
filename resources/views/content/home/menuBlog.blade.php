<section id="menublog">
	{{-- v-if="panelBlogOpciones" --}}
	<div id="opcionesBlog">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading" style="background-color: #a8def9">
						<h3>
							Tareas
						</h3>
					</div>
					<div class="panel-body text-center panelOpcionesFont">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-excel-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-pdf-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-word-o"></i>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-text"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-powerpoint-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-file-video-o"></i>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<a href="{{ url('blog') }}">
							<button @click="panelTareas" class="btn btn-block btn-primary">
								<strong> Ingresar </strong>
							</button>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-success">
					<div class="panel-heading" style="background-color:#a7ff83">
						<h3>
							Avisos
						</h3>
					</div>
					<div class="panel-body text-center panelOpcionesFont">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-comments-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-smile-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
					</div>
					<div class="panel-footer">
                		<a href="{{ url('blog') }}">
							<button @click="panelAvisos" class="btn btn-block btn-primary"></style>
								<strong> Ingresar </strong>
							</button>
						</a>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="panel panel-info">
					<div class="panel-heading" style="background-color:#a7ff83">
						<h3>
							Calendario
						</h3>
					</div>
					<div class="panel-body text-center panelOpcionesFont">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-calendar"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-calendar-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button @click="paneCalendario" class="btn btn-block btn-primary">
							<strong> Ingresar </strong>
						</button>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="panel panel-success">
					<div class="panel-heading" style="background-color: #a8def9">
						<h3>
							Deportes
						</h3>
					</div>
					<div class="panel-body text-center panelOpcionesFont">
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-futbol-o"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
								<i class="fa fa-life-bouy"></i>
							</div>
							<div class="col-xs-4 col-sm-4 col-md-4">
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button @click="panelDeportes" class="btn btn-block btn-primary"></style>
							<strong> Ingresar </strong>
						</button>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</section>