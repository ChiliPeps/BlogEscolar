<section id="principal">
	<br>
	<div id="seccionNoticias">
		<div class="row">
			@foreach ($noticias as $noticia)
		 		<div class="col-md-4">
						<div class="thumbnail" style="border-color:#bdbaba;">
							{!! Html::image($noticia->imagen, "map", ['class' => 'img-responsive']) !!}
							{{-- <img src="img/imagen-1.jpg" > --}}
							<div class="caption">
								<h3>{{$noticia->titulo}}</h3>
								<p class="text-justify">
									{{$noticia->descripcion}}
									
								</p>
								<p class="text-right">
									<a href="{{$noticia->categoria}}/{{$noticia->id}}/{{$noticia->slugurl}}" class="btn btn-info">
										<strong> LEER NOTICIA </strong>
									</a>
								</p>
							</div>
						</div>
					</div> 
			@endforeach
		</div>
	</div>
	{{-- <div id="seccionNoticias">
		<div class="row">
			<div class="col-md-4">
				<div class="thumbnail" style="border-color:#bdbaba;">
					<img src="img/imagen-1.jpg" >
					<div class="caption">
						<h3>Thumbnail label</h3>
						<p class="text-justify">
							Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.
						</p>
						<p class="text-right">
							<a href="#" class="btn btn-info">
								<strong> LEER NOTICIA </strong>
							</a>
						</p>
			@foreach($noticiasTopInicio as $noticiaTopInicio)
				<div class="col-md-4">
					<div class="thumbnail" style="border-color:#bdbaba;">
						{!! Html::image($noticiaTopInicio->imagen, "map", ['class' => 'img-responsive']) !!}
						<div class="caption">
							<h3> {{ $noticiaTopInicio->titulo }} </h3>
							<p class="text-justify">
								{{ substr($noticiaTopInicio->descripcion, 0, 150) }}...
							</p>
							<p class="text-right">
								<a href="#" class="btn btn-info">
									<strong> LEER NOTICIA </strong>
								</a>
							</p>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div> --}}
</section>