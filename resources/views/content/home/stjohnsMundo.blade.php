<section id="stJohnsMundo">
	<div class="row">
		<div class="col-sm-5 col-md-5 text-left">
			{{-- class="text-capitalize" --}}
			<p v-text="mensajePanel"></p>
		</div>
		<div class="col-sm-7 col-md-7">
			<button @click="kinderIn" type="button" class="col-md-2 btn btn-primary text-uppercase">
				<strong>Kinder</strong>
			</button>
			<button @click="primariaIn" type="button" class="col-md-3 btn btn-success text-uppercase">
				<strong>Primaria</strong>
			</button>
			<button @click="secundariaIn" type="button" class="col-md-3 btn btn-warning text-uppercase">
				<strong>Secundaría</strong>
			</button>
			<button @click="preparatoriaIn" type="button" class="col-md-3 btn btn-danger text-uppercase">
				<strong>Preparatoria</strong>
			</button>
		</div>
		<p id="carpetaSeleccionada" hidden> </p>
	</div>
	<br>
	<div v-show="panelKinder" id="kinder" class="row">
		<div v-for="carpeta in carpetasStJohnsMundoJson.resultadoCarpetasSociales">
			<div v-if="carpeta.receptor == 'Kinder'"  class="col-md-2">
				<div class="box box-primary">
					<div class="box-header with-border">
						@{{{ carpeta.nombre }}}
					</div>
					<div @click="sliderFotos" id="@{{ carpeta.nombre }}" @mousemove="mouseMove" @mouseout="mouseOut" class="box-body carpetaFotos">
						<a href="#" class="thumbnail">
							<img src="">
						</a>
						<a href="#" class="thumbnail" style="display:none;">
							<img src="">
						</a>
					</div>
					<div class="box-footer">
						@{{{ carpeta.numero }}} Fotos
					</div>
				</div>
			</div>
		</div>
	</div>
	<div v-show="panelPrimaria" id="primaria" class="row">
		<span v-for="carpeta in carpetasStJohnsMundoJson.resultadoCarpetasSociales">
			<div v-if="carpeta.receptor == 'Primaria'" class="col-md-2">
				<div class="box box-success">
					<div class="box-header with-border">
						<span> @{{{ carpeta.nombre }}} </span>
					</div>
					<div id="@{{ carpeta.nombre }}" @click="sliderFotos" @mousemove="mouseMove" @mouseout="mouseOut" class="box-body carpetaFotos">
						<a href="#" class="thumbnail">
							<img src="">
						</a>
						<a href="#" class="thumbnail" style="display:none;">
							<img src="">
						</a>
					</div>
					<div class="box-footer">
						<span> @{{{ carpeta.numero }}} fotos </span>
					</div>
				</div>
			</div>
		</span>
	</div>
	<div v-show="panelSecundaria" id="secundaria" class="row">
		<span v-for="carpeta in carpetasStJohnsMundoJson.resultadoCarpetasSociales">
			<div v-if="carpeta.receptor == 'Secundaria'" class="col-md-2">
				<div class="box box-warning">
					<div class="box-header with-border">
						<span> @{{{ carpeta.nombre }}} </span>
					</div>
					<div id="@{{ carpeta.nombre }}" @click="sliderFotos" @mousemove="mouseMove" @mouseout="mouseOut" class="box-body carpetaFotos">
						<a href="#" class="thumbnail">
							<img src="">
						</a>
						<a href="#" class="thumbnail" style="display:none;">
							<img src="">
						</a>
					</div>
					<div class="box-footer">
						<span> @{{{ carpeta.numero }}} fotos </span>
					</div>
				</div>
			</div>
		</span>
	</div>
	<div v-show="panelPreparatoria" id="preparatoria" class="row">
		<span v-for="carpeta in carpetasStJohnsMundoJson.resultadoCarpetasSociales">
			<div v-if="carpeta.receptor == 'Bachillerato'" class="col-md-2">
				<div class="box box-danger">
					<div class="box-header with-border">
						<span> @{{{ carpeta.nombre }}} </span>
					</div>
					<div id="@{{ carpeta.nombre }}" @click="sliderFotos" @mousemove="mouseMove" @mouseout="mouseOut" class="box-body carpetaFotos">
						<a href="#" class="thumbnail">
							<img src="">
						</a>
						<a href="#" class="thumbnail" style="display:none;">
							<img src="">
						</a>
					</div>
					<div class="box-footer">
						<span> @{{{ carpeta.numero }}} Fotos </span>
					</div>
				</div>
			</div>
		</span>
	</div>
	<div id="sliderStJohnsMundo" v-show="panelSliderFotos">
		<div class="row">
			<div class="col-md-12">
				<h3>
					<i @click="sliderFotosBack" class="fa fa-fw fa-arrow-circle-left"></i>
					<strong> Regresar | Carpeta @{{ albumSeleccionado }}.</strong>
				</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2" v-for="fotoAlbum in carpetasStJohnsMundoJson.resultadoFotosAll | filterBy albumSeleccionado in nombreAlbum">
				<a class="thumbnail" data-toggle="modal" data-target="#myModal">
					{{-- <img src="img/null1.png" alt="error"> --}}
					<img v-bind:src="fotoAlbum.contenido" contenido="error">
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"> 
									<strong>
										Carpeta @{{ albumSeleccionado }}
									</strong>
								</h4>
							</div>
							<div class="modal-body">
								<div id="sliderFotos">
									<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
										<!-- Indicators -->
										<ol class="carousel-indicators">
											<li data-target="#carousel-example-generic" data-slide-to="0" v-for="fotoAlbum in carpetasStJohnsMundoJson.resultadoFotosAll | filterBy albumSeleccionado in nombreAlbum"></li>
										</ol>

										<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
											<div class="item">
												<img v-bind:src="imagenPrincipal" alt="error">
											</div>
											<div class="item" v-for="fotoAlbum in carpetasStJohnsMundoJson.resultadoFotosAll | filterBy albumSeleccionado in nombreAlbum" v-if="imagenPrincipal != fotoAlbum.contenido">
												<img v-bind:src="fotoAlbum.contenido" alt="error">
											</div>
										</div>
										<!-- Controls -->
										<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
											<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
											<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	{{-- <pre> @{{ carpetasStJohnsMundoJson.resultadoCarpetasSociales | json }} </pre> --}}
	{{-- <pre> @{{ carpetasStJohnsMundoJson.resultadoFotosCarpetasBachillerato | json }} </pre> --}}
	{{-- <pre> @{{ carpetasStJohnsMundoJson.resultadoFotosAll | json }} </pre> --}}
</section>