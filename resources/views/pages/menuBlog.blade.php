@extends('master')
@section('content')
    <section class="content">
		@if(Auth::check())
	        <h3 class="text-right">
        		<label v-text="mensaje">
        		</label>
	        </h3>
        @endif
		@include('content.home.menuBlog')
	</section>
@stop