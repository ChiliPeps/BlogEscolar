@extends('master')
@section('content')
    <section class="content-header text-center">
        <div id="headerBlog">
        	<div class="row">
        		@if(Auth::check())
		        	<div class="col-xs-12 col-md-12 text-right">
				        <h3>
			        		<label v-text="mensaje">
			        		</label>
				        </h3>
			        </div>
		        @endif
	        </div>
	    	<div v-show="panelLoginInOpcionesBusqueda">
		        <div class="row">
		        	<div class="col-md-12 text-right">
		        		<button class="btn btn-default">
							<strong @click="panelOpciones" style="cursor:pointer;"> 
								<i class="fa fa-arrow-circle-left"> </i> Regresar 
							</strong>
						</button>
						<div class="btn-group text-uppercase">
							<select @change="cambiarColor" class="btn btn-default" id="escolaridadBlog">
								<option selected value="0"> Elegir escolaridad... </option>
								{{-- 
								<option value="Kinder">Kinder</option>
								<option value="Primaria">Primaria</option>
								<option value="Secundaria">Secundaria</option>
								<option value="Preparatoria">Preparatoria</option> 
								--}}
								<option v-for="e in escolaridadesJson.escolaridades" v-bind:value="e.nivel_educativo">
							  		@{{ e.nivel_educativo }}
	                        	</option>
							</select>
							<select v-model="buscarGrupoGrado" hidden>
								<option value="nivel_educativo" selected>1</option>
							</select>
							{{-- Tareas --}}
							<input v-model="buscarTarea" type="text" value="idEscolaridad" hidden>
							{{-- Avisos --}}
							<input v-model="buscarAvisoReceptor" type="text" value="Todos" hidden>
							<input v-model="avisoReceptor" type="text" value="receptor" hidden>
							<input v-model="avisoFecha" type="text" value="fechaAlta" hidden>
						</div>
						<div class="btn-group text-uppercase">
							<select @change="inBlog" id="gradoGrupoBlog" class="btn btn-primary" style="margin-top: 2px;">
							  	<option value="0" selected> Elegir grado y grupo... </option>
							  	<option v-for="escolaridad in escolaridadesJson.resultadoEscolaridad | filterBy nivelEducativo in buscarGrupoGrado" v-bind:value="escolaridad.id">
							  		@{{{ escolaridad.nivel_educativo + " " + escolaridad.grado + "-" + escolaridad.grupo }}}
	                        	</option>
							</select>
						</div>
		        	</div>
		        </div>
		        <br>
		        <div class="row">
		        	<div class="col-md-7">
		        	</div>
					<div v-show="panelLoginInTareas" class="col-xs-12 col-md-5 pull-right">
						<div id="opcionesBusqueda" class="box box-primary box-solid collapsed-box">
							<div class="box-header with-border">
								<h3 class="box-title"> Opciones de búsqueda </h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
									</button>
								</div>
							<!-- /.box-tools -->
							</div>
							<!-- /.box-header -->
							<!-- style="display: none;" -->
							<div class="box-body text-right">
								<div class="col-xs-5 col-md-6">
									<select v-model="parametrosBusqueda" id="parametrosBusquedaTarea" @change="BusquedaTarea" class="btn btn-primary" style="padding-right:0px;">
										<option value="0" selected> Búsqueda por </option>
										<option value="profesor"> Profesor </option>
										<option value="materia"> Materia </option>
										<option value="fechaEntrega"> Fecha de entrega </option>
										<option value="fechaAlta"> Fecha de subida </option>
										<option value="codigo"> Código </option>
									</select>
								</div>
								<input v-model="idEscolaridadGrupoGrado" id="gradoGrupoHidden" type="text" value="" hidden>
								<div class="col-xs-7 col-md-6">
									<input v-show="busquedaTareaFiltro" id="textoBusqueda" type="text" v-model="busquedaTareaTipo" placeholder="Parametro de búsqueda" class="form-control">
									<div v-show="busquedaCalendario" class="input-group date">
										<div class="input-group-addon">
		                                	<i class="fa fa-calendar"></i>
		                                </div>
										<input type="text" id="datepickerBlog" class="form-control" v-model="busquedaTareaTipo" placeholder="Fecha de búsqueda" @change="cambiarFecha">
									</div>
								</div>
							</div>
						</div>					
					</div>
					<div v-show="busquedaFechaAvisos" class="col-xs-12 col-md-5 pull-right">
						<div id="opcionesBusqueda" class="box box-primary box-solid collapsed-box">
							<div class="box-header with-border">
								<h3 class="box-title"> Opciones de búsqueda </h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
									</button>
								</div>
							<!-- /.box-tools -->
							</div>
							<!-- /.box-header -->
							<!-- style="display: none;"  -->
							<div class="box-body text-right">
								<div class="input-group date">
									<div class="input-group-addon">
	                                	<i class="fa fa-calendar"></i>
	                                </div>
									<input @click="busquedaFechaAviso" v-model="fechaAviso" type="text" id="datepickerBlogAviso" class="form-control" placeholder="Fecha de búsqueda">
								</div>
							</div>
							<!-- /.box-body -->
						</div>					
					</div>
		        </div>
		    </div>
        </div>
	    {{-- <pre> @{{ escolaridadesJson.calendariosKinder | json }} </pre> --}}
    </section>
    <section class="content">
		@include('content.home.blog')
	</section>
@stop