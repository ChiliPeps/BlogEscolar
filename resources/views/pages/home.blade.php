@extends('master')
@section('content')
    <section class="content-header" id="sliderInicio">
	  	<div class="page-header">
			<img src="img/background.jpg">
			<div id="sloganStJohns">
				St John´s
				<br>
				"Ser mejor es ser St. John´s."
			</div>
		</div>
    </section>
    <section class="content">
		@include('content.home.principal')
	</section>
@stop