@extends('master')
@section('content')
    <section class="content-header">
        <h1>
            Reglamento St-John´s
        </h1>
    </section>
    <section class="content">
		@include('content.home.reglamentoStjohns')
	</section>
@stop