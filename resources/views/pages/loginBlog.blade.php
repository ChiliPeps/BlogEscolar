@extends('master')
@section('content')
	<section id="blog">
		<div id="login">
			<br><br><br>
			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-6">
					<div class="box box-info" style="border-left:black;">
						<div class="box-header with-border">
							<h3 class="box-title"> Iniciar sesión - Blog </h3>
						</div>
						<div class="form-horizontal">
							{{ Form::open(array('url' => '/loginBlog')) }}
								<div class="box-body">
									<div class="form-group">
										<label class="col-sm-2 control-label">Usuario</label>
										<div class="col-sm-10">
											<input type="text" class="form-control" name="usuario" id="inputUsuario" placeholder="Usuario" v-model="userBlog.user">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Password</label>
										<div class="col-sm-10">
											<input type="password" class="form-control" name="password" id="inputPassword3" placeholder="Password"  v-model="userBlog.password">
										</div>
									</div>
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
								</div>
								<!-- /.box-body -->
								<div class="box-footer">		        					
									@if(Session::has('mensaje_error'))
							            {{ Session::get('mensaje_error') }}
							        @endif
									<button type="submit" class="btn btn-info pull-right">
										<strong> Ingresar </strong>
									</button>
								</div>
								<!-- /.box-footer -->
							{{ Form::close() }}
						</div>
					</div>
				</div>
				<div class="col-md-3">
				</div>
			</div>
		</div>
	</section>
@stop
