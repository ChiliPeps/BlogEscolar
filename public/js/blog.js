$(document).ready(function ()
{
	//TOOLTIP
	$('#botonMenu').tooltip();
	$('.carpeta').tooltip();
	$('#pictureOut').tooltip();
	// if ("#sltTipoAviso") {};

	$("#sltTipoAviso").change(function ()
 	{
 		if($.trim($(this).val()) == "mensaje" )
 		{ 	
 			// $("#sltMes").css('display', "none");
 			$("#sltPara").removeAttr('disabled'); 
 			// $("#sltGrupoAviso").css('display', 'inline-block');
 			// $("#sltReceptorAviso option[value=masivo]").css('display', 'block');
 			// $("#sltReceptorAviso option[value=personal]").css('display', 'block');
  		}
  		else
 		{
 			$("#sltPara").attr('disabled', 'disabled'); 
 		}
 		
 	});

 	$("#sltPara").change(function ()
 	{

 		if($.trim($(this).val()) == "masivo")
	 	{ 
	 		document.getElementById("sltNivel").value = 0;
	 		$("#sltNivel").attr('disabled', 'disabled');
 			$("#sltReceptor").attr('disabled', 'disabled');

	 	}
  		else if($.trim($(this).val()) == "grupo")
 		{
 			$("#sltNivel").removeAttr('disabled');
 			$("#sltReceptor").attr('disabled', 'disabled');  
 			
 		}
 		else if($.trim($(this).val()) == "personal" )
 		{
 			$("#sltNivel").removeAttr('disabled'); 
 			$("#sltReceptor").removeAttr('disabled'); 
 		}
 		else{
 			$("#sltNivel").attr('disabled', 'disabled'); 
 			$("#sltReceptor").attr('disabled', 'disabled'); 
 		}
 		
 	});

 	$("#sltmes").change(function ()
 	{ 
 		if ($.trim($(this).val()) != null) 		  
 			{
 				document.getElementById("sltPara").value = "masivo"
 				$("#sltNivel").removeAttr('disabled'); 
 				$("#sltReceptor").removeAttr('disabled'); 
 			}
 	});

// St John´s en el Mundo.
	// $(".carpetaFotos").mousemove(function (){
	// 	$(this).find("a:eq(0)").css("display", "none");
	// 	$(this).find("a:eq(1)").css("display", "block");
	// });
	// $(".carpetaFotos").mouseout(function (){
	// 	$(this).find("a:eq(1)").css("display", "none")
	// 	$(this).find("a:eq(0)").css("display", "block");
	// });

//DATEPICKER

	$("#datepickerBlog, #datepickerBlogAviso").datepicker({
	 	language: "es-MX",
	 	autoclose: true,
	 	todayHighlight: true,
		toggleActive: true
	});
	$('#datepickerBlog').datepicker('option', {dateFormat: 'yy-mm-dd'});
	$('#datepickerBlogAviso').datepicker('option', {dateFormat: 'yy-mm-dd'});

	$(function() {
		$( "#datepicker").datepicker({
			language: "es-MX",
			autoclose: true,
			todayHighlight: true,
			toggleActive: true
		});
		$('#datepicker').datepicker('option', {dateFormat: 'yy/mm/dd'});
		//$('#datepicker').datepicker( 'option', $.datepicker.regional[ 'es' ] );
	});

	 // $(document).ready(function() {
  //       $('#gpschk').select2();
  //   });

	//Calendarios
	//$('.imgCalendario img').removeAttr("style");
});

