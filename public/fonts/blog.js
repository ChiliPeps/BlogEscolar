$(document).ready(function ()
{
	//TOOLTIP
	$('#botonMenu').tooltip();
	$('.carpeta').tooltip();

	$("#sltTipoAviso").change(function ()
 	{
 		if($.trim($(this).val()) == "mensaje")
 		{ 
 			// $("#sltMes").css('display', "none");
 			$("#sltReceptor").removeAttr('disabled'); 
 			$("#sltGrupoAviso").css('display', 'inline-block');
 			$("#sltReceptorAviso option[value=masivo]").css('display', 'block');
 			$("#sltReceptorAviso option[value=personal]").css('display', 'block');
  		}
  		else
 		{
 			$("#sltReceptor").attr('disabled', 'disabled'); 
 		}
 		
 	});

 	$("#sltReceptor").change(function ()
 	{
 		if($.trim($(this).val()) == "masivo")
	 	{ 
	 		$("#sltNivel").attr('disabled', 'disabled'); 
 			$("#sltGrupo").attr('disabled', 'disabled'); 
	 	}
  		else if($.trim($(this).val()) == "grupo")
 		{
 			$("#sltNivel").removeAttr('disabled');
 			$("#sltGrupo").attr('disabled', 'disabled');  
 			
 		}
 		else if($.trim($(this).val()) == "personal")
 		{
 			$("#sltNivel").removeAttr('disabled'); 
 			$("#sltGrupo").removeAttr('disabled'); 
 		}
 		else{
 			$("#sltNivel").attr('disabled', 'disabled'); 
 			$("#sltGrupo").attr('disabled', 'disabled'); 
 		}
 		
 	});

//DATEPICKER
 	// $(function() {
	 //    $( "#datepicker").datepicker({
	 //    	language: "es-MX",
  //   		autoclose: true,
		//     todayHighlight: true,
		//     toggleActive: true
	 //    });
	 //    $('#datepicker').datepicker('option', {dateFormat: 'yy/mm/dd'});
	 //    // $('#datepicker').datepicker( 'option', $.datepicker.regional[ 'es' ] );
	 //  });
});

