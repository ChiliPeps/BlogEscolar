<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('master', function () {
    return view('master');
});

Route::get('/', ['as' => 'inicio', 'uses' => 'BlogController@obtenerNoticiasInicio']);
// Route::get('/', function () {
//     return view('pages.home');
// });

Route::get('/home', ['as' => 'inicio', 'uses' => 'BlogController@obtenerNoticiasInicio']);
// Route::get('/home', function () {
//     return view('pages.home');
// });

Route::get('/about', function()
{
    return View::make('pages.about');
});

// Route::get('/blog', function()
// {
//     return View::make('pages.blog');
// });


// Nos mostrará el formulario de login.
Route::get('/loginBlog', 'BlogController@showLogin');

// Validamos los datos de inicio de sesión.
Route::post('/loginBlog', 'BlogController@postLogin');

// Nos indica que las rutas que están dentro de él sólo serán mostradas si antes el usuario se ha autenticado.
Route::group(array('before' => 'auth'), function()
{
    // Esta será nuestra ruta de bienvenida.
    Route::get('/blog', ['as' => 'blog', 'uses' => 'BlogController@escolaridadesTareasAvisos']);
    // Esta ruta nos servirá para cerrar sesión.
    Route::get('/logOut', 'BlogController@logOut');
});

Route::get('/news', ['as' => 'news', 'uses' => 'BlogController@obtenerNoticias']);
/*
Route::get('/news', function()
{
    return View::make('pages.news');
});
*/
Route::get('/contact', function()
{
    return View::make('pages.contact');
});

// Route::get('stjohnsMundo', function()
// {
//     return View::make('pages.stjohnsMundo');
// });
Route::get('/stjohnsMundo', ['as' => 'stjohnsMundo', 'uses' => 'BlogController@carpetasStjohsMundo']);


Route::get('/reglamentoStjohns', function()
{
    return View::make('pages.reglamentoStjohns');
});


// rutas noticia

Route::get('noticiasAll', ['as'=> 'noticias.index','uses' => 'NoticiasController@todasNoticias']);

Route::get('/{categoria}/{id}/{slugurl}',       ['as' => 'news',       'uses' => 'NoticiasController@getNoticia']);

Route::get('/',     ['as' => 'pages.home', 'uses' => 'NoticiasController@top3']);
Route::get('home',     ['as' => 'pages.home', 'uses' => 'NoticiasController@top3']);

