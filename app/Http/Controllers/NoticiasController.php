<?php

namespace App\Http\Controllers;

use Gmlo\CMS\Modules\Noticias\Noticia;
// use Gmlo\CMS\Modules\Categorias\Categoria;
use Gmlo\CMS\Modules\Tags\Tag_Noticia;
use App\Http\Requests;
use Request;
use Response;

class NoticiasController extends Controller
{
	public function todasNoticias()
	{
		$noticias = Noticia::orderBy('titulo','asc')->get();
		return $noticias;
	}
	// public function obtenerNoticias()
	// {
	// 	$noticias = Noticia::orderBy('titulo','asc')->get();
	// 	return view('pages.news', compact('noticias'));
	// }
	
	public function getNoticia($categria, $id, $slugurl)
	{
		//Take Route Name(Category Title)
		// $route_name = Request::route()->getName();

		//Check Category
		// $categoria = Categoria::where('title', $route_name)->select('id', 'title')->first();
		// if($categoria == null) { abort(404); }

		//Noticia - Tags - Categorias - Imagen
		// $noticia = Noticia::with(['categoria','tags.tag'])->where(['id' => $id, 'slugurl' => $slugurl ,'id_categoria' => $categoria->id])->first();
		// if($noticia == null) { abort(404); }
		$noticia = Noticia::where('id' , $id)->first();

		//Incrementar Hits
		Noticia::where('id', $noticia->id)->increment('hits');

		return view('pages.noticia', compact('noticia'));
	}
	

	public function getOtrasNoticias()
	{
		if(Request::ajax()) {
			$seccion = Request::get('seccion');
			$id_categoria = Categoria::where('title', $seccion)->first()->id;
			$noticias = Noticia::with('categoria')->where('id_categoria', '<>', $id_categoria)->orderBy('fecha', 'desc')->take(4)->get();
			return Response::json($noticias);
        }
	}

	public function getOtrasNoticias2()
	{
		if(Request::ajax()) {
			$noticias = Noticia::with('categoria')->orderBy('fecha', 'desc')->take(4)->get();
			return Response::json($noticias);
        }
	}

	public function getBusqueda()
	{
		//Busqueda
		$resultados = Noticia::where('titulo', 'LIKE', '%'.Request::get('search_field').'%')->paginate(8);
		return view('pages.busqueda', compact('resultados'));
	}

	public function getWeather()
	{
		if(Request::ajax()) {
			$client = new \GuzzleHttp\Client();
			$res = $client->request('GET', 'http://api.weatherunlocked.com/api/current/24.14222,-110.31083?app_id=ba78dabb&app_key=f393109fdafc058166486de045417e1a&lang=es', []);
			//echo $res->getStatusCode();// 200
			//echo $res->getHeaderLine('content-type');// 'application/json; charset=utf8'
			//echo $res->getBody();// {"type":"User"...'
			$result = json_decode($res->getBody());
			return Response::json($result);
		}
	}
	public function top3()
	{
		$noticias = Noticia::take(3)->get();
		return view('pages.home', compact('noticias'));
		
	}
}
