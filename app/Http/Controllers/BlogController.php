<?php
namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Request;
use Response;
use Redirect;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;
use DB;

use Gmlo\CMS\Controllers\EscolaridadesController as Escolaridades;
use Gmlo\CMS\Controllers\TareasController as Tareas;
use Gmlo\CMS\Controllers\AvisosController as Avisos;
use Gmlo\CMS\Controllers\CarpetasController as Carpetas;
use Gmlo\CMS\Controllers\SocialesController as Sociales;
use Gmlo\CMS\Controllers\NoticiasController as Noticias;
use Gmlo\CMS\Modules\Tareas\TareasRepo as TareasRepo;
use Gmlo\CMS\Modules\Carpetas\CarpetasRepo as CarpetasRepo;
use Gmlo\CMS\Modules\Categories\CategoriesRepo as CategoriesRepo;
use Gmlo\CMS\Modules\Noticias\Noticia as Noticia;
use Gmlo\CMS\Modules\Deportes\Deporte as Deporte;


class BlogController extends Controller
{
    /**
     * Muestra el formulario para login.
     */
    public function showLogin()
    {
        // Verificamos que el usuario no esté autenticado
        if (Auth::check())
        {
            // Si está autenticado lo mandamos a la raíz donde estara el mensaje de bienvenida.
            // return view('pages.menuBlog');
            // return view('pages.blog');
            return Redirect::to('/blog');
        }
        // Mostramos la vista login.blade.php (Recordemos que .blade.php se omite.)
        return view('pages.loginBlog');
    }

    /**
     * Valida los datos del usuario.
     */
    public function postLogin()
    {
        // Guardamos en un arreglo los datos del usuario.
        $email = Input::get('usuario');
        $password= Input::get('password');
        // Validamos los datos y además mandamos como un segundo parámetro la opción de recordar el usuario.
        if (Auth::attempt(['email' => $email, 'password' => $password]))
        {
            // De ser datos válidos nos mandara a la bienvenida
            // return view('pages.menuBlog');
            // return view('pages.blog');
            return Redirect::to('/blog');
        }
        // En caso de que la autenticación haya fallado manda un mensaje al formulario de login y también regresamos los valores enviados con withInput().
        return Redirect::to('/loginBlog')->with('mensaje_error', 'Tu contraseña o password son incorrectas.')->withInput();
    }


    /**
     * Muestra el formulario de login mostrando un mensaje de que cerro sesión.
     */
    public function logOut()
    {
        Auth::logout();
        return Redirect::to('/loginBlog')->with('mensaje_error', 'Tu sesión ha sido cerrada.');
    }

    public function carpetasStjohsMundo()
    {        
        if(Request::ajax())
        {
            $sociales = new Sociales();
            $resultadoCarpetasSociales = $sociales->obtenerCarpeta();
            $resultadoFotosCarpetasKinder = $sociales->obtenerFotosCarpetaKinder();
            $resultadoFotosCarpetasPrimaria = $sociales->obtenerFotosCarpetaPrimaria();
            $resultadoFotosCarpetasSecundaria = $sociales->obtenerFotosCarpetaSecundaria();
            $resultadoFotosCarpetasBachillerato = $sociales->obtenerFotosCarpetaBachillerato();
            $resultadoFotosAll = $sociales->obtenerFotosAll();
            return Response::json(compact('resultadoCarpetasSociales', 'resultadoFotosCarpetasKinder', 'resultadoFotosCarpetasPrimaria', 'resultadoFotosCarpetasSecundaria', 'resultadoFotosCarpetasBachillerato', 'resultadoFotosAll'));
        }
        return view('pages.stjohnsMundo');
    }

    public function escolaridadesTareasAvisos()
    {        
        if(Request::ajax())
        {
            // Escolaridades
            $escolaridad = new Escolaridades();
            $resultadoEscolaridad = $escolaridad->getAllData()->toArray();
            $escolaridades = $escolaridad->getEscolaridades();
            // Tareas
            $tareasRepo = new tareasRepo(); 
            $categoriesRepo = new CategoriesRepo();
            $tarea = new Tareas($tareasRepo, $categoriesRepo);
            $resultadoTarea = $tarea->alltareasBlog()->toArray();
            // Avisos
            $avisos = new Avisos();
            $resultadoAvisos = $avisos->avisosAllBlog();
            // Calendario
            $calendariosKinder = $avisos->calendariosKinderBlog();
            $calendariosPrimaria = $avisos->calendariosPrimariaBlog();
            $calendariosSecundaria = $avisos->calendariosSecundariaBlog();
            $calendariosPreparatoria = $avisos->calendariosPraparatoriaBlog();
            //$calendarios = DB::select('SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Kinder" AND nombreMes = "anual" and nombreMes = MONTHNAME("' + date("Y-m-d") + '");');
            // Deportes
            $deportesFutbol = DB::select('SELECT * FROM cms_deportes, cms_users WHERE cms_users.id = cms_deportes.idUser AND tipoDeporte = "Futbol"');
            $deportesGimnasio = DB::select('SELECT * FROM cms_deportes, cms_users WHERE cms_users.id = cms_deportes.idUser AND tipoDeporte = "Gimnasio"');
            $deportesNatacion = DB::select('SELECT * FROM cms_deportes, cms_users WHERE cms_users.id = cms_deportes.idUser AND tipoDeporte = "Natacion"');
            return Response::json(compact('escolaridades','resultadoEscolaridad', 'resultadoTarea', 'resultadoAvisos', 'calendariosKinder', 'calendariosPrimaria', 'calendariosSecundaria', 'calendariosPreparatoria', 'deportesFutbol', 'deportesGimnasio', 'deportesNatacion'));
        }
        return view('pages.blog');
    }

    public function obtenerNoticias()
    {
        $noticias = Noticia::All();
        $noticiasTop = DB::select('SELECT * FROM cms_noticias LIMIT 5');
        return view('pages.news', compact('noticias', 'noticiasTop'));
    }

    public function obtenerNoticiasInicio()
    {
        $noticias = Noticia::All();
        $noticiasTopInicio = DB::select('SELECT * FROM cms_noticias LIMIT 3');
        return view('pages.home', compact('noticias', 'noticiasTopInicio'));
    }

    public function obtenerCalendarios()
    {

    }
    // public function noticiasStjohsMundo()
    // {        
    //     if(Request::ajax())
    //     {
    //         // Escolaridades
    //         $noticias = new Noticias();
    //         $resultadoNoticias = $noticias->obtenerNoticas()->toArray();
    //         return Response::json(compact('resultadoNoticias'));
    //     }
    //     return view('pages.news');
    // }
}
