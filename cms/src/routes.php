<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Gmlo\CMS\Controllers', 'as' => 'CMS::', 'middleware' => ['web']], function ()
{
    Route::get('/',     ['as' => 'admin.home', 'uses' => 'AdminController@home']);
    Route::get('login', ['as' => 'admin.login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'admin.login', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'admin.logout', 'uses' => 'AuthController@getLogout']);

    // Password reset link request routes...
    Route::get('password/email', ['as' => 'admin.recover-password', 'uses' => 'PasswordController@getEmail']);
    Route::post('password/email', ['as' => 'admin.recover-password', 'uses' => 'PasswordController@postEmail']);

    // Password reset routes...
    Route::get('password/reset/{token}', ['as' => 'admin.reset-password', 'uses' => 'PasswordController@getReset']);
    Route::post('password/reset', ['as' => 'admin.reset-password', 'uses' => 'PasswordController@postReset']);

    Route::get('users/update-my-password', ['as' => 'admin.users.update-my-password', 'uses' => 'UserController@editMyPassword']);
    Route::put('users/update-my-password', ['as' => 'admin.users.update-my-password', 'uses' => 'UserController@updateMyPassword']);
    Route::resource('users', 'UserController');


    Route::get('users/{id}/update-password', ['as' => 'admin.users.update-password', 'uses' => 'UserController@editPassword']);
    Route::put('users/{id}/update-password', ['as' => 'admin.users.update-password', 'uses' => 'UserController@updatePassword']);

    Route::put('users/{id}/status-toggle', ['as' => 'admin.users.status-toggle', 'uses' => 'UserController@statusToggle']);


    // Categories
    Route::resource('categories', 'CategoriesController');

    // Articles
    Route::resource('articles', 'ArticlesController');
    Route::put('articles/toggle-status/{id}', ['as' => 'admin.articles.toggle-status', 'uses' => 'ArticlesController@toggleStatus']);

    //Tareas
    Route::get('tareas/gradogrupo',        ['as' => 'admin.tareas.gradogrupo', 'uses' => 'TareasController@regresaGyG']);
    Route::get('tareas/niveles',           ['as' => 'admin.tareas.niveles',    'uses' => 'TareasController@regresaNivel']);
    Route::delete('tareas/eliminar',       ['as' => 'admin.tareas.eliminar',   'uses' => 'TareasController@borrarTarea']);
    Route::get('tareas/todas',             ['as' => 'admin.tareas.todas',   'uses' => 'TareasController@showTareasAll']);
    Route::post('tareas/agregarTarea',     ['as' => 'admin.tareas.agregarTarea',   'uses' => 'TareasController@addTarea']);
    Route::post('tareas/actualizarTarea',  ['as' => 'admin.tareas.actualizarTarea',   'uses' => 'TareasController@actualizarTarea']);
    Route::get('tareas/recuperarTarea',    ['as' => 'admin.tareas.recuperarTarea',   'uses' => 'TareasController@recuperarTarea']);
    Route::get('tareas/borradas',          ['as' => 'admin.tareas.borradas',   'uses' => 'TareasController@borradas']);
    Route::get('tareas/ultimoid',          ['as' => 'admin.tareas.ultimoid', 'uses' => 'TareasController@LastID']);
    Route::get('tareas/graficas',          ['as' => 'admin.tareas.graficas', 'uses' => 'TareasController@grafica']);

    Route::resource('tareas', 'TareasController');
    // Route::put('tareas/toggle-status/{id}', ['as' => 'admin.tareas.toggle-status', 'uses' => 'TareasController@toggleStatus']);

    //Avisos
    Route::get('avisos/gradogrupo',        ['as' => 'admin.avisos.gradogrupo', 'uses'=> 'AvisosController@regresaGyG']);
    Route::get('avisos/niveles',           ['as' => 'admin.avisos.niveles',    'uses' => 'AvisosController@regresaNivel']);
    Route::delete('avisos/eliminar',       ['as' => 'admin.avisos.eliminar',   'uses' => 'AvisosController@Eliminar']);
    Route::get('avisos/todos',             ['as' => 'admin.avisos.todos', 'uses' => 'AvisosController@showAvisosAll']);
    Route::post('avisos/agregarAviso',     ['as' => 'admin.avisos.agregarAviso', 'uses' => 'AvisosController@addAviso']);
    Route::post('avisos/actualizarAviso',  ['as' => 'admin.avisos.actualizarAviso', 'uses' => 'AvisosController@updatedAviso']);
    Route::get('avisos/borrados',          ['as' => 'admin.avisos.borrados', 'uses' => 'AvisosController@showBorrados']);
    Route::get('avisos/recuperarAviso',    ['as' => 'admin.avisos.recuperarAviso', 'uses' => 'AvisosController@recuperar']);
    
    Route::resource('avisos', 'AvisosController');

    // Escolaridades
    Route::delete('escolaridades/borrar',   ['as' => 'admin.escolaridades.borrar',      'uses' => 'EscolaridadesController@delete']);
    Route::get('escolaridades',             ['as' => 'admin.escolaridades.index',      'uses' => 'EscolaridadesController@index']);
    Route::get('escolaridades/datos',       ['as' => 'admin.escolaridades.datos',   'uses' => 'EscolaridadesController@getData']);
    Route::get('escolaridades/valor',       ['as' => 'admin.escolaridades.valor',   'uses' => 'EscolaridadesController@obtenerval']);
    Route::post('escolaridades/actualizar', ['as' => 'admin.escolaridades.actualizar', 'uses' => 'EscolaridadesController@actualizar']);
    Route::post('escolaridades/save',       ['as' => 'admin.escolaridades.save', 'uses' => 'EscolaridadesController@guardar']);

    Route::resource('escolaridades','EscolaridadesController');

    //sociales
    // Route::resource('sociales/drop', ['as'=>'admin.sociales.drop', 'uses'=> 'SocialesController@dropmetodo']);
    Route::post('sociales/upload',   ['as' => 'admin.sociales.upload',  'uses' => 'SocialesController@upload']);
    Route::post('Sociales/guardar',   ['as' => 'admin.sociales.guardar',  'uses' => 'SocialesController@guardar']);
    Route::post('sociales/agregarCarpeta',   ['as' => 'admin.sociales.agregarCarpeta',  'uses' => 'CarpetasController@addCarpeta']);
    Route::delete('sociales/eliminarCarpeta',   ['as' => 'admin.sociales.eliminarCarpeta',  'uses' => 'CarpetasController@eliminarCarpeta']);
    Route::post('sociales/agregarFoto',   ['as' => 'admin.sociales.agregarFoto',  'uses' => 'SocialesController@addFoto']);
    // Route::get('sociales/todasCarpetas',   ['as' => 'admin.sociales.todasCarpetas',  'uses' => 'CarpetasController@todasCarpetas']);
    Route::get('sociales/todasCarpetas',   ['as' => 'admin.sociales.todasCarpetas',  'uses' => 'SocialesController@obtenerCarpeta']);
    Route::get('sociales/get_galeria',  ['as' => 'admin.sociales.galeria.get',      'uses' => 'SocialesController@getGaleria']);
    Route::post('sociales/set_galeria', ['as' => 'admin.sociales.galeria.set',      'uses' => 'SocialesController@setGaleria']);
    Route::resource('sociales', 'SocialesController');
    // Route::put('sociales/toggle-status/{id}', ['as' => 'admin.sociales.toggle-status', 'uses' => 'SocialController@toggleStatus']);

    //Deportes
    Route::delete('deportes/eliminar',       ['as' => 'admin.deportes.eliminar',   'uses' => 'DeportesController@borrarDeporte']);
    Route::get('deportes/todos',   ['as' => 'admin.deportes.todos',   'uses' => 'DeportesController@showDeportes']);
    Route::post('deportes/agregarADeportes',     ['as' => 'admin.deportes.agregarADeportes',   'uses' => 'DeportesController@addADeportes']);
    Route::post('deportes/actualizarDeportes',     ['as' => 'admin.deportes.actualizarDeportes',   'uses' => 'DeportesController@updateDeportes']);
    Route::resource('deportes', 'DeportesController');

    //Mantenimiento
    // @if (Auth::user()->type = "suadmin")
    //  {

         Route::resource('mantenimiento', 'MantenimientoController');
  //   }
  // @endif

    //Vaciado
    Route::get('vaciado/todos',   ['as' => 'admin.vaciado.datos',   'uses' => 'TareasController@vaciado']);    
    Route::delete('truncate/tareas',   ['as' => 'mantenimiento.tarea.truncate',   'uses' => 'TareasController@truncate']);    

    //Noticias
    Route::get('noticias',              ['as' => 'admin.noticias.index',            'uses' => 'NoticiasController@index']);
    Route::get('noticias/data',         ['as' => 'admin.noticias.data',             'uses' => 'NoticiasController@getData']);
    Route::get('noticias/categorias',   ['as' => 'admin.noticias.categorias',       'uses' => 'NoticiasController@getCategorias']);
    Route::post('noticias/create',      ['as' => 'admin.noticias.save',             'uses' => 'NoticiasController@saveNoticia']);
    Route::post('noticias/update',      ['as' => 'admin.noticias.update',           'uses' => 'NoticiasController@updateNoticia']);
    Route::delete('noticias/delete',    ['as' => 'admin.noticias.delete',           'uses' => 'NoticiasController@deleteNoticia']);
    Route::get('noticias/tags',         ['as' => 'admin.noticias.tags',             'uses' => 'NoticiasController@getTags']);
    Route::post('noticias/save_tag',    ['as' => 'admin.noticias.save_tag',         'uses' => 'NoticiasController@saveTag']);
    //Galeria
    Route::get('noticias/get_galeria',  ['as' => 'admin.noticias.galeria.get',      'uses' => 'NoticiasController@getGaleria']);
    Route::post('noticias/set_galeria', ['as' => 'admin.noticias.galeria.set',      'uses' => 'NoticiasController@setGaleria']);
    // Route::resource('noticias', 'NoticiasController');

    // Media Manager
    Route::get('media-manager', ['as' => 'admin.media-manager.index', 'uses' => 'MediaManagerController@index']);
    Route::post('media-manager/finder', ['as' => 'admin.media-manager.finder', 'uses' => 'MediaManagerController@finder']);
    Route::post('media-manager/upload', ['as' => 'admin.media-manager.upload', 'uses' => 'MediaManagerController@upload']);
    Route::get('media-manager/assets', ['as' => 'admin.media-manager.assets', 'uses' => 'MediaManagerController@getAssets']);
    Route::put('media-manager/update', ['as' => 'admin.media-manager.update', 'uses' => 'MediaManagerController@update']);
    Route::delete('media-manager/destroy', ['as' => 'admin.media-manager.destroy', 'uses' => 'MediaManagerController@destroy']);
});
