<?php

return [

    'app_name' => 'Blog escolar',

    'template_skin' => 'blue', // blue, black, purple, yellow, red, green

    'template_layout_options' => ['fixed'], // fixed, layout-boxed, layout-top-nav, sidebar-collapse, sidebar-mini

    // Users Module

    'user_types' => ['suadmin', 'admin', 'multinivel', 'kinder', 'primaria','secundaria','bachillerato'],

    // Media manager

    'default_path' => 'media-manager',

];