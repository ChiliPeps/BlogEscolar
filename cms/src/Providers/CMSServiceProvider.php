<?php

namespace Gmlo\CMS\Providers;


use App\Http\Kernel;
use Gmlo\CMS\MediaManager;
use Gmlo\CMS\Modules\Escolaridades\Escolaridad;
use Gmlo\CMS\Modules\Tareas\Tarea;
use Gmlo\CMS\Modules\Avisos\Aviso;
use Gmlo\CMS\Modules\Articles\Article;
use Gmlo\CMS\Modules\Categories\Category;
use Gmlo\CMS\Modules\Users\User;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Gmlo\CMS\CMS;
use Blade;
use Gmlo\CMS\Alert;
use Gmlo\CMS\FieldBuilder;

class CMSServiceProvider extends ServiceProvider
{

    protected $file_config;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        include __DIR__ . '/../helpers.php';

        $this->file_config = $file_config = __DIR__ . '/../config/cms.php';

        $this->mergeConfigFrom($this->file_config, 'cms');

        $this->publishFiles();

        // Translations
        $this->loadTranslationsFrom(__DIR__ . '/../lang', 'CMS');

        // Load our views
        $this->loadViewsFrom(__DIR__ . '/../views', 'CMS');
        $router->middleware('CMSAuthenticate', 'Gmlo\CMS\Middleware\CMSAuthenticate');

        $this->app['config']->set('auth.model', 'Gmlo\CMS\Modules\Users\User');

        $this->extendBlade();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__ . '/../routes.php';

        $this->app['cms'] = $this->app->share(function($app){
           return new CMS();
        });


        $this->app['alert'] = $this->app->share(function($app)
        {
            $alertBuilder = new Alert($app['view'], $app['session.store']);
            return $alertBuilder;
        });


        $this->app['field'] = $this->app->share(function($app)
        {
            $fieldBuilder = new FieldBuilder($app['form'], $app['view'], $app['session.store']);
            return $fieldBuilder;
        });

        $this->app['media_manager'] = $this->app->share(function($app)
        {
            return new MediaManager();
        });


        $this->app->singleton('command.cms.start', function ($app) {
            return $app['Gmlo\CMS\Commands\StartCommand'];
        });
        $this->commands('command.cms.start');


        $this->registerFakers();
    }

    protected function registerFakers()
    {

        // Users
        $factory = app('Illuminate\Database\Eloquent\Factory');
        $factory->defineAs(User::class, 'cms_site_demo', function ($faker) {
            return [
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => str_random(10),
                'type' => $faker->randomElement(['admin', 'editor']),
                'remember_token' => str_random(10),
            ];
        });


        // Categories
        $factory->defineAs( Category::class, 'cms_site_demo', function ($faker) {
            $title = $faker->sentence;
            return [
                'title'         => $title,
            ];
        });

        // Articles
        $factory->defineAs( Article::class, 'cms_site_demo', function ($faker) {
            $sumary = $faker->paragraph;
            $title = $faker->sentence;
            return [
                'title'         => $title,
                'slug_url'      => str_slug($title),
                'primary_img'   => null,
                'sumary'        => $sumary,
                'body'          => $faker->text,
                'title_seo'     => $title,
                'meta_keywords'     => implode(', ', $faker->words(10)),
                'meta_description'  => $sumary,
                'category_id'       => $faker->numberBetween(1, 5),
                'created_by'        => $faker->numberBetween(1, 5),
                'views'             => $faker->numberBetween(0, 200),
                'published_at'      => $faker->dateTimeThisYear()->format('Y-m-d H:i:s'),
            ];
        });

        $factory->defineAs( Escolaridad::class, 'cms_site_demo', function ($faker) {
            return [
                'grado'            => $faker->numberBetween($min = 1, $max = 3),
                'grupo'            => $faker->randomLetter,
                'nivel_educativo'  => $faker->randomElement($array = array ('Kinder','Primaria','Secundaria','Bachillerato')),
            
            ];
        });

        $factory->defineAs( Tarea::class, 'cms_site_demo', function ($faker) {

            return [
                'idUser'           => $faker->numberBetween($min = 1, $max = 5),
                'idEscolaridad'    => $faker->numberBetween($min = 1, $max = 10),
                'tipo'             => $faker->randomElement($array = array ('individual','equipo')),
                'fechaAlta'        => $faker->dateTimeBetween($startDate = '-12 days', $endDate = '+2 days'),
                'fechaEntrega'     => $faker->dateTimeBetween($startDate = '+2 days', $endDate = '+30 days'),               
                'materia'          => $faker->randomElement($array = array ('quimica','geografia','historia','matematicas','ingles')),
                'profesor'         => $faker->name,
                'contenido'        => $faker->text($maxNbChars = 1000),/*imageUrl($width = 640, $height = 480, 'business', true, 'Faker'),*/
                'recomendaciones'  => $faker->randomElement($array = array ('Ninguna','leer bien','comer frutas','none')),
                'created_at'       => $faker->dateTimeBetween($startDate = '+1 month', $endDate = '+11 month'),
            ];
        });

        $factory->defineAs( Aviso::class, 'cms_site_demo', function ($faker) {

            return [
                'idUser'           => $faker->numberBetween($min = 1, $max = 5),
                'tipo'             => $faker->randomElement($array = array ('mensaje','calendario')),   
                'contenido'        => $faker->text($maxNbChars = 1000),
                'created_at'       => $faker->dateTimeBetween($startDate = '+1 month', $endDate = '+11 month'),
                'receptor'         => $faker->randomElement($array = array ('todos','kinder','primaria','secundaria','bachillerato')),
                'titulo'           => $faker->text($maxNbChars = 15),
                'para'             => $faker->randomElement($array = array ('masivo','personal','grupo')),
            ];
        });
    }

    protected function publishFiles()
    {
        // Config
        $this->publishes([
            $this->file_config => base_path('config/cms.php'),
        ]);

        // Assets
        $this->publishes([
            __DIR__.'/../assets' => public_path('vendor/gmlo/cms'),
        ], 'public');

        $this->shareGlobalVariables();
    }

    protected function shareGlobalVariables()
    {
        //view()->share('cms_current_user', \Auth::user());
    }

    protected function extendBlade()
    {
        /*Blade::directive('linkSidebarMenu', function($route, $text, $icon) {
            return "<?php echo ''; ?>";
        });*/
    }
}
