<?php

namespace Gmlo\CMS\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//Modules
use Gmlo\CMS\Modules\Noticias\Noticia;
use Gmlo\CMS\Modules\Categorias\Categoria;
use Gmlo\CMS\Modules\Tags\Tag;
use Gmlo\CMS\Modules\Tags\Tag_Noticia;
use Gmlo\CMS\Modules\Investimges\Investimg;

//Requests
use Gmlo\CMS\Requests\CreateNoticia;
use Gmlo\CMS\Requests\UpdateNoticia;
use Gmlo\CMS\Requests\TagsRequest;

//Helpers
use Carbon\Carbon;
use Request;
use Response;
use DB;
use Image;
use File;
use Auth;

class NoticiasController extends Controller
{
    public function __construct()
    {
        $this->middleware('CMSAuthenticate');

        $tipos = [  'titulo'    => 'Titulo de Noticia',
            'categoria'      => 'Categoria'];

        view()->share('tipos', $tipos);
    }

    public function index()
    {
        return view('CMS::noticias.index');
    }

    public function getData()
    {
        if (Request::ajax())
        {
            return Response::json(Noticia::with('tags.tag')->orderBy('created_at', 'desc')->get());
        }
    }

    public function getCategorias()
    {
        if (Request::ajax())
        {
            return Response::json(Categoria::orderBy('title', 'asc')->get());
        }
    }

    public function getTags()
    {
        if (Request::ajax()) {
            return Response::json(Tag::select(['id', 'nombre'])->orderBy('nombre', 'asc')->get());
        }
    }

    public function saveTag(TagsRequest $request)
    {
        if (Request::ajax()) {
            return Response::json(Tag::create(['nombre' => Request::get('nombre')]));
        }
    }

    public function saveNoticia(CreateNoticia $request)
    {
        if (Request::ajax())
        {  
            //Save Record
            $noticia               = new Noticia();
            $noticia->titulo       = Request::get('titulo');
            $noticia->slugurl      = Request::get('slugurl');
            $noticia->fecha        = date("Y/m/d");
            // $noticia->fecha        = Request::get('fecha');
            $noticia->descripcion  = Request::get('descripcion');
            $noticia->contenido    = Request::get('contenido');
            $noticia->autor        = Auth::user()->name;
            // $noticia->autor        = Request::get('autor');
            // $noticia->prioridad    = Request::get('prioridad');
            $noticia->categoria    = Request::get('categoria');
            // $noticia->autor_foto   = Request::get('autor_foto');
            $noticia->hits         = 0;
            $noticia->imagen       = "Uploading";
            $noticia->thumb        = "Uploading";
            $noticia->save();

            //Save Images
            $imagen = $this->uploadImage(Request::file('imagen'), $noticia->id, 0);
            $thumb  = $this->uploadImage(Request::file('imagen'), $noticia->id, 1); //Thumb

            //Change values from record
            $noticia->imagen       = $imagen;
            $noticia->thumb        = $thumb;
            $noticia->save();

            //Save Tags
            $this->tagManager("save", Request::get('tags'), $noticia->id);
        }
    }


    public function updateNoticia(UpdateNoticia $request)
    {
        if (Request::ajax())
        {
            $noticia = Noticia::findOrFail(Request::get('id'));

            if(Request::file('imagen') != null) {
                //Delete Old Images
                $this->deleteImage($noticia->imagen);
                $this->deleteImage($noticia->thumb);

                //Save New Images
                $imagen = $this->uploadImage(Request::file('imagen'), $noticia->id, 0);
                $thumb  = $this->uploadImage(Request::file('imagen'), $noticia->id, 1); //Thumb

                //Save Records
                $noticia->imagen = $imagen;
                $noticia->thumb  = $thumb;
            }

            //Update Record
            $noticia->titulo       = Request::get('titulo');
            $noticia->slugurl      = Request::get('slugurl');
            // $noticia->fecha        = Request::get('fecha');
            $noticia->descripcion  = Request::get('descripcion');
            $noticia->contenido    = Request::get('contenido');
            // $noticia->autor        = Request::get('autor');
            // $noticia->id_categoria = Request::get('id_categoria');
            // $noticia->prioridad    = Request::get('prioridad');
            // $noticia->autor_foto   = Request::get('autor_foto');
            $noticia->save();

            //Update Tags
            $this->tagManager("update", Request::get('tags'), $noticia->id);
        }
    }

    public function deleteNoticia()
    {
        if (Request::ajax())
        {
            $noticia  = Noticia::find(Request::get('id'));
            //$this->deleteImage($noticia->imagen); No Estoy Seguro
            //$this->deleteImage($noticia->thumb); No Estoy Seguro
            $noticia->delete();
        }
    }

    protected function uploadImage($file, $id_noticia, $thumb)
    { 
        if($file != null) {
            if($file->isValid())
            {
                //Directories
                $directory_name = \Config::get('cms.default_path');
                $directory      = $directory_name.'/noticias/'.$id_noticia."/";

                // Check if the directory exists, if not then create it
                if(!file_exists($directory))  {
                    mkdir($directory, 0777, true);
                }

                //Read File Image
                $img = Image::make($file->getRealPath());

                //Make Thumb? 186x125
                if($thumb == 1) {

                    //File Name
                    $file_name = $id_noticia."_".time()."_thumb.".$file->getClientOriginalExtension();

                    //Real Path
                    $path = $directory.$file_name;

                    //Resize image
                    $img->resize(186, 125);

                } else { //Normal Size

                    //File Name
                    $file_name = $id_noticia."_".time().".".$file->getClientOriginalExtension();

                    //Real Path
                    $path = $directory.$file_name;
                }

                //Save Image
                $img->save($path);

                return $path;
            }
        }
    }

    protected function deleteImage($path)
    {
        //If file Exists, Delete it
        if (file_exists($path)) {
            unlink($path);
        }
    }

    protected function tagManager($action, $tags, $noticia_id)
    {
        //Action
        if($action == "update") {
            //Borrar Anteriores
            Tag_Noticia::where('id_noticia', $noticia_id)->delete();
        }

        //Agregar tags
        $query = array();
        for ($i = 0; $i < count($tags); $i++) {
            $q['id_tag']     = $tags[$i];
            $q['id_noticia'] = $noticia_id;
            $query[] = $q;
        }
        DB::table('cms_tag_noticia')->insert($query);
    }

    //Galeria
    public function getGaleria()
    {
        if (Request::ajax())
        {
            $galeria = Investimg::where('id_noticia', Request::get('id_noticia'))->get();
            return Response::json($galeria);
        }
        
    }

    public function setGaleria()
    {
        if (Request::ajax())
        {
            //If Null
            if(Request::file('imagenesMFU') == null){return;}

            $id_noticia = Request::get('id_noticia');
            $files      = Request::file('imagenesMFU');
            $paths      = null;
            $created    = Carbon::now();

            //Directories
            $directory_name = \Config::get('cms.default_path');
            $directory      = $directory_name.'/noticias/'.$id_noticia."/galeria/";

            // Check if the directory exists, if not then create it - OR - Clear old files
            if(!file_exists($directory))  { mkdir($directory, 0777, true); }
            else { File::cleanDirectory($directory); }

            //Clear Database Records
            Investimg::where('id_noticia', $id_noticia)->delete();

            for($i = 0; $i < count($files); $i++) {

                //Read File Image
                $img = Image::make($files[$i]->getRealPath());

                //File Name
                $file_name = $i."_".time().".".$files[$i]->getClientOriginalExtension();

                //Real Path
                $path = $directory.$file_name;
                
                //Save Image
                $img->save($path);

                //Make paths array
                $q['id_noticia'] = $id_noticia;
                $q['imagen']     = $path;
                $q['posicion']   = $i;
                $q['created_at'] = $created;
                $paths[] = $q;
            }
            
            //Save Record to DB
            DB::table('cms_investimges')->insert($paths);
        }
    }
}
