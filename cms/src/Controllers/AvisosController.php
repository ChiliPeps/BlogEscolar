<?php

namespace Gmlo\CMS\Controllers;
use Carbon\Carbon;
use Gmlo\CMS\Modules\Avisos\AvisosRepo;
use Gmlo\CMS\Modules\Categories\CategoriesRepo;
use Gmlo\CMS\Requests\CreateAviso;
use Gmlo\CMS\Requests\UpdateAviso;
use Gmlo\CMS\Modules\Avisos\Aviso;
use Request;
use Gmlo\CMS\Modules\Escolaridades\Escolaridad; // escolaridades
// use Illuminate\Http\Request;
use Response;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class AvisosController extends Controller
{
    private $avisosRepo;
    protected $categoriesRepo;

    public function __construct()
    {
        // $this->avisosRepo = $avisosRepo;
        // $this->categoriesRepo = $categoriesRepo;
        $this->middleware('CMSAuthenticate');
        // view()->share('categories', $this->categoriesRepo->lists('title', 'id'));
        // view()->share('nivel', $this->concatenaEscolaridad());
        view()->share('nivel', $this->RegresaEscolaridad());
        view()->share('kinder', $this->RegresaKinder());
        view()->share('primaria', $this->RegresaGradoGrupo());
        view()->share('secundaria', $this->RegresaSecundaria());
        view()->share('bachillerato', $this->RegresaBachillerato());
       
    }

    public function index()
    {
        // $avisos = $this->avisosRepo->orderBy('created_at', 'desc')->paginate();
        return view('CMS::avisos.index', compact('avisos'));
    }

    public function create()
    {
        return view('CMS::avisos.create');
    }


    public function store(CreateAviso $request)
    {
        $aviso = $this->avisosRepo->storeNew($request->all());

        \Alert::success('CMS::avisos.msg_aviso_created');
        return redirect()->route('CMS::admin.avisos.edit', $aviso->id);
    }

    public function edit($id)
    {
        $aviso = $this->avisosRepo->findOrFail($id);

        return view('CMS::avisos.edit', compact('aviso'));
    }


    public function update($id, UpdateAviso $request)
    {
        $aviso = $this->avisosRepo->findOrFail($id);
        $this->avisosRepo->update($aviso, $request->all());

        \Alert::success('CMS::avisos.msg_aviso_updated');
        return redirect()->route('CMS::admin.avisos.edit', $aviso->id);

    }

    public function destroy($id)
    {
        $aviso = $this->avisosRepo->findOrFail($id);
        $this->avisosRepo->delete($aviso);

        \Alert::success('CMS::avisos.msg_aviso_deleted');
        return redirect()->route('CMS::admin.avisos.index');
    }


    public function toggleStatus($id)
    {
        $aviso = $this->avisosRepo->findOrFail($id);
        $data = '';
        $message = null;
        if($aviso->published_at == null)
        {
            $data['published_at'] = Carbon::now();
            $message = 'CMS::avisos.msg_aviso_published';
        }
        else
        {
            $data['published_at'] = null;
            $message = 'CMS::avisos.msg_aviso_unpublished';
        }

        $this->avisosRepo->update($aviso, $data);
        \Alert::success($message);
        return redirect()->route('CMS::admin.avisos.edit', $aviso->id);
    }

    public function showAvisosAll()
    {
        $user = Auth::user()->type;

        if ( $user == 'suadmin' || $user == "admin") {
           
            //funcionan softdeletes
            $query = Aviso::orderBy('cms_avisos.created_at', 'desc')
            ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
            ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
            return $query;

        }
        else 
        {
            $id = Auth::user()->id;
            
             $query = Aviso::orderBy('cms_avisos.created_at', 'desc')
            ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
            ->where('cms_users.id', $id)
            ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
            return $query;
        }
       

        //no funcionan softdeletes
        // $query = DB::table('cms_avisos')->distinct()
        //  ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        //  ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
        //  return $query; 
    }

    public function avisosAll()
    {
        //funcionan softdeletes
        $query = Aviso::orderBy('cms_avisos.created_at', 'asc')
        ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
        return $query;
    }

    public function avisosAllBlog()
    {
        //funcionan softdeletes
        $mes = date("m");
        $ano = date("Y");
        if($mes == 1)
        {
            $mes = 12;
            $ano = $ano - 1;
        }
        else
        {
            $mes = $mes - 1;
        }
        $fecha = date("Y")."-".date("m")."-".date("d");
        $fecha2 = $ano."-".$mes."-"."21";
        $query = Aviso::orderBy('cms_avisos.created_at', 'desc')
        ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        ->where('cms_avisos.fechaAlta','BETWEEN', $fecha2,'AND',$fecha)
        ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
        return $query;
    }
    public function mesEspanol($nombreMes)
    {
        switch ($nombreMes) {
            case 'Jan':
                $mes = "enero";
                break;
            case 'Feb':
                $mes = "febrero";
                break;
            case 'Mar':
                $mes = "marzo";
                break;
            case 'Apr':
                $mes = "abril";
                break;
            case 'May':
                $mes = "mayo";
                break;
            case 'Jun':
                $mes = "junio";
                break;
            case 'Jul':
                $mes = "julio";
                break;
            case 'Aug':
                $mes = "agosto";
                break;
            case 'Oct':
                $mes = "septiembre";
                break;
            case 'Nov':
                $mes = "noviembre";
                break;
            case 'Dec':
                $mes = "diciembre";
                break;
        }
        return $mes;
    }

    public function calendariosKinderBlog()
    {
        // $mes = date("M");
        // $mes = $this->mesEspanol($mes);
        // $query = Aviso::orderBy('cms_avisos.created_at', 'desc')
        // ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        // ->where([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Kinder'],
        //             ['cms_avisos.nombreMes', 'anual']
        //         ])
        // ->orwhere([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Kinder'],
        //             ['cms_avisos.nombreMes', $mes]
        //         ])
        // ->get();

        // $queryA = Aviso::orderBy('cms_avisos.created_at', 'asc')
        // ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        // ->where([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Kinder'],
        //             ['cms_avisos.nombreMes', $mes]
        //         ])
        // ->distinct('cms_avisos.nombreMes');

        // $queryB = Aviso::orderBy('cms_avisos.created_at', 'asc')
        // ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        // ->where([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Kinder'],
        //             ['cms_avisos.nombreMes', 'anual']
        //         ])
        // ->union($queryA)
        // ->get();
        //(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
        // $query = DB::select('(SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Kinder" AND nombreMes = "'.$mes.'" ORDER BY id DESC LIMIT 1)
        //                     union
        //                     SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Kinder" AND nombreMes = "anual";');
        // return $query;
    }
    public function calendariosPrimariaBlog()
    {
        // $mes = date("M");
        // $mes = $this->mesEspanol($mes);
        // $query = Aviso::orderBy('cms_avisos.created_at', 'desc')
        // ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        // ->where([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Primaria'],
        //             ['cms_avisos.nombreMes', 'anual']
        //         ])
        // ->orwhere([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Primaria'],
        //             ['cms_avisos.nombreMes', $mes]
        //         ])
        // ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
        // $query = DB::select('SELECT * FROM cms_avisos WHERE tipo = "calendario" AND receptor = "Kinder" AND nombreMes = "anual" and nombreMes = MONTHNAME(' + date() + ');');

        // $query = DB::select('(SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Primaria" AND nombreMes = "'.$mes.'" ORDER BY id DESC LIMIT 1)
        //                     union
        //                     SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Primaria" AND nombreMes = "anual";');
        // return $query;
    }
    public function calendariosSecundariaBlog()
    {
        // $mes = date("M");
        // $mes = $this->mesEspanol($mes);
        // $query = Aviso::orderBy('cms_avisos.created_at', 'desc')
        // ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        // ->where([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Secundaria'],
        //             ['cms_avisos.nombreMes', 'anual']
        //         ])
        // ->orwhere([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Secundaria'],
        //             ['cms_avisos.nombreMes', $mes]
        //         ])
        // ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
        // $query = DB::select('SELECT * FROM cms_avisos WHERE tipo = "calendario" AND receptor = "Kinder" AND nombreMes = "anual" and nombreMes = MONTHNAME(' + date() + ');');
        
        // $query = DB::select('(SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Secundaria" AND nombreMes = "'.$mes.'" ORDER BY id DESC LIMIT 1)
        //                     union
        //                     SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Secundaria" AND nombreMes = "anual";');
        // return $query;
    }
    public function calendariosPraparatoriaBlog()
    {
        // $mes = date("M");
        // $mes = $this->mesEspanol($mes);
        // $query = Aviso::orderBy('cms_avisos.created_at', 'desc')
        // ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
        // ->where([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Bachillerato'],
        //             ['cms_avisos.nombreMes', 'anual']
        //         ])
        // ->orwhere([
        //             ['cms_avisos.tipo', 'calendario'],
        //             ['cms_avisos.receptor', 'Bachillerato'],
        //             ['cms_avisos.nombreMes', $mes]
        //         ])
        // ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
        // $query = DB::select('SELECT * FROM cms_avisos WHERE tipo = "calendario" AND receptor = "Kinder" AND nombreMes = "anual" and nombreMes = MONTHNAME(' + date() + ');');
        // $query = DB::select('(SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Bachillerato" AND nombreMes = "'.$mes.'" ORDER BY id DESC LIMIT 1)
        //                     union
        //                     SELECT * FROM cms_avisos WHERE tipo= "calendario" AND receptor = "Bachillerato" AND nombreMes = "anual";');
        // return $query;
    }

    public function regresaNivel()
    {   
        $query = Escolaridad::distinct()
        ->get(['nivel_educativo']);
        return $query;
    }

    public function regresaGyG()
    {
        //softdeletes
        $nivel_educativo= Request::get('x');
        $query = Escolaridad::orderBy('grado','asc')
        ->where('nivel_educativo', $nivel_educativo)
        ->get(['id','nivel_educativo','grado','grupo']);
        return $query;

        //no funciona softdeletes
        // $nivel_educativo= Request::get('x');
        // $query = DB::table('cms_escolaridad')
        // ->where('nivel_educativo', $nivel_educativo)
        // ->get(['id','nivel_educativo','grado','grupo']);
        // return $query;
    }

    public function addAviso(CreateAviso $request)
    {
        if (Request::ajax())
        {
            $aviso = new Aviso();
            $aviso->idUser          = Auth::user()->id;
            $aviso->contenido       = Request::get('contenido');
            $aviso->receptor        = Request::get('receptor');
            $aviso->tipo            = Request::get('tipo');
            $aviso->titulo          = Request::get('titulo');
            $aviso->fechaAlta       = date('Y/m/d H:i:s');
            $aviso->nombreMes       = Request::get('nombreMes');
            $aviso->para            = Request::get('para');
            $aviso->nivel           = Request::get('nivel');
            $aviso->save();

            return Response::Json($aviso);
        }
    }

    public function updatedAviso()
    {
         if (Request::ajax())
        {
            $aviso = Aviso::find(Request::input('id'));
            
            $aviso->receptor        = Request::get('receptor');
            $aviso->tipo            = Request::get('tipo');
            $aviso->titulo          = Request::get('titulo');

            $aviso->nombreMes       = Request::get('nombreMes');
            $aviso->contenido       = Request::get('contenido');
            $aviso->para            = Request::get('para');
            $aviso->nivel           = Request::get('nivel');
            $aviso->save();

            return Response::Json($aviso);
        }
    }

    public function Eliminar()
    {
        $this->registradelete();
        
        // registradelete();
        if (Request::ajax())
         {
            Aviso::destroy(Request::get('id'));
        }
    }

     public function RegresaEscolaridad()
    {
        $escolaridad =Escolaridad::select('nivel_educativo')->distinct()->get();
        $arreglo = array();
        // $arreglo = $escolaridad; 
        foreach ($escolaridad as $k)
        {
           $arreglo[] = $k->nivel_educativo;
        }
        // $arreglo = DB::table('cms_escolaridad')->distinct()->get('nivel_educativo');
        return $arreglo;      
    }

    public function RegresaGradoGrupo()
    {
        $gg = Escolaridad::select('id','nivel_educativo','grado','grupo')->Where('nivel_educativo','Primaria')->get();
        $arreglo = array();
        foreach ($gg as $key)
        {
            $arreglo[]= $key->nivel_educativo.' '.$key->grado. '-'. $key->grupo;
        }

        return $arreglo;
    }

     public function RegresaSecundaria()
    {
        $gg = Escolaridad::select('id','nivel_educativo','grado','grupo')->Where('nivel_educativo','Secundaria')->get();
        $arreglo = array();
        foreach ($gg as $key)
        {
            $arreglo[]= $key->nivel_educativo.' '.$key->grado. '-'. $key->grupo;
        }

        return $arreglo;
    }

    public function RegresaBachillerato()
    {
        $gg = Escolaridad::select('id','nivel_educativo','grado','grupo')->Where('nivel_educativo','Bachillerato')->get();
        $arreglo = array();
        foreach ($gg as $key)
        {
            $arreglo[]= $key->nivel_educativo.' '.$key->grado. '-'. $key->grupo;
        }

        return $arreglo;
    }

    public function RegresaKinder()
    {
        $gg = Escolaridad::select('id','nivel_educativo','grado','grupo')->Where('nivel_educativo','Kinder')->get();
        $arreglo = array();
        foreach ($gg as $key)
        {
            $arreglo[]= $key->nivel_educativo.' '.$key->grado. '-'. $key->grupo;
        }

        return $arreglo;
    }

     public function concatenaEscolaridad()
    {
        $escolaridades = Escolaridad::all();
        $arreglo = array();
        foreach ($escolaridades as $escolaridad)
        {
            $arreglo[$escolaridad->id] = $escolaridad->nivel_educativo .' '. $escolaridad->grado .' - ' .$escolaridad->grupo;
        }
        return $arreglo;
    }

    public function registradelete()
    {
        $current = Auth::user()->id;
        $nombre = Auth::user()->name;
        $now = date('Y/m/d H:i:s');
        $tabla = "aviso";
        $id = Request::get('id');
        $receptor = Request::get('contenido');

        DB::table('deletes')->insert(
            array('idUserElimino'=>$current,'nombreElimino'=>$nombre ,'fechaBorrado'=>$now, 'tabla'=>$tabla, 'idBorrado'=>$id)
            );
    }

    public function showBorrados()
    {
        $query = Aviso::onlyTrashed()
            ->join('cms_users','cms_avisos.idUser','=','cms_users.id')
            ->get(['contenido','fechaAlta','cms_avisos.id','cms_users.name','nombreMes','receptor','tipo','titulo','para','nivel']);
             return $query;
    }

    public function recuperar()
    {
         Aviso::withTrashed()->find(Request::get('id'))->restore();
    }
}
