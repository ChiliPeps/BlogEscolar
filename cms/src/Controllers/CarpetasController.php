<?php

namespace Gmlo\CMS\Controllers;
use Carbon\Carbon;
use Gmlo\CMS\Modules\Carpetas\CarpetasRepo;
use Gmlo\CMS\Modules\Categories\CategoriesRepo;
use Gmlo\CMS\Modules\Carpetas\Carpeta;
use Gmlo\CMS\Requests\CreateCarpeta;
use Gmlo\CMS\Requests\UpdateCarpeta;
use Gmlo\CMS\Modules\Escolaridades\Escolaridad; // escolaridades
// use Illuminate\Http\Request;
use Request;
use Response;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class CarpetasController extends Controller
{
    private $carpetasRepo;
    protected $categoriesRepo;

    public function __construct()//CarpetasRepo $carpetasRepo, CategoriesRepo $categoriesRepo)
    {
        // $this->carpetasRepo = $carpetasRepo;
        // $this->categoriesRepo = $categoriesRepo;
        $this->middleware('CMSAuthenticate');
        //view()->share('categories', $this->categoriesRepo->lists('title', 'id'));
    }

    public function index()
    {
        $carpetas = $this->carpetasRepo->orderBy('created_at', 'desc')->paginate();
        $todasCarpetas =$this->todasCarpetas();
        return view('CMS::carpetas.index', compact('carpetas','todascarpetas'));
    }

    public function create()
    {
        return view('CMS::carpetas.create');
    }


    public function store(CreateCarpeta $request)
    {
        $carpeta = $this->carpetasRepo->storeNew($request->all());

        \Alert::success('CMS::carpetas.msg_carpeta_created');
        return redirect()->route('CMS::admin.carpetas.edit', $carpeta->id);
    }

    public function edit($id)
    {
        $carpeta = $this->carpetasRepo->findOrFail($id);

        return view('CMS::carpetas.edit', compact('carpeta'));
    }


    public function update($id, UpdateCarpeta $request)
    {
        $carpeta = $this->carpetasRepo->findOrFail($id);
        $this->carpetasRepo->update($carpeta, $request->all());

        \Alert::success('CMS::carpetas.msg_carpeta_updated');
        return redirect()->route('CMS::admin.carpetas.edit', $carpeta->id);

    }

    public function destroy($id)
    {
        $carpeta = $this->carpetasRepo->findOrFail($id);
        $this->carpetasRepo->delete($carpeta);

        \Alert::success('CMS::carpetas.msg_carpeta_deleted');
        return redirect()->route('CMS::admin.carpetas.index');
    }

     public function eliminarCarpeta()
   {
         if(Request::ajax())
         {
            Carpeta::destroy(Request::get('id'));    
         }
   }

    public function addCarpeta()
    {
        if (Request::ajax())
        {
            $carpeta = new Carpeta();
            $carpeta->nombre    = Request::get('nombre');
            $carpeta->receptor  = Request::get('receptor');
            $carpeta->save();
            return Response::Json($carpeta);
            
        }
    }

    public function todasCarpetas()
    {
        if(Request::ajax())
        {
            return Response::json($this->todo());
        }           
    }
    public function todasCarpetasBlog()
    {
        $query = Carpeta::orderBy('cms_carpetas.created_at', 'desc')
        ->get(['id','nombre','receptor','created_at','updated_at']);
        return $query;
           
    }
    
    public function todo()
    {
        $carpeta = Carpeta::all();
        return $carpeta;
    }

}
