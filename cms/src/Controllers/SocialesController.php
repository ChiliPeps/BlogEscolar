<?php

namespace Gmlo\CMS\Controllers;
use Carbon\Carbon;
use Gmlo\CMS\Modules\Sociales\SocialesRepo;
use Gmlo\CMS\Modules\Categories\CategoriesRepo;
use Gmlo\CMS\Modules\Sociales\Social;
use Gmlo\CMS\Requests\CreateSocial;
use Gmlo\CMS\Requests\UpdateSocial;
use Gmlo\CMS\Modules\Escolaridades\Escolaridad; // escolaridades
use Gmlo\CMS\Modules\Carpetas\Carpeta;
// use Illuminate\Http\Request;
use Input; //prueba para dropzone
use Validator; // prueba para dropzone
use Request;
use Response;
use DB;
use Image;
use File;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class SocialesController extends Controller
{
    private $socialesRepo;
    protected $categoriesRepo;

    public function __construct()//SocialesRepo $socialesRepo, CategoriesRepo $categoriesRepo)
    {
        // $this->socialesRepo = $socialesRepo;
        // $this->categoriesRepo = $categoriesRepo;
        $this->middleware('CMSAuthenticate');
        // view()->share('categories', $this->categoriesRepo->lists('title', 'id'));
    }

    public function index()
    {
        //$sociales = $this->socialesRepo->orderBy('created_at', 'desc')->paginate();
        // $todasCarpetas =$this->todasCarpetas();
        return view('CMS::sociales.index', compact('sociales'));
    }

    public function create()
    {
        return view('CMS::sociales.create');
    }


    public function store(CreateSocial $request)
    {
        $social = $this->socialesRepo->storeNew($request->all());

        \Alert::success('CMS::sociales.msg_social_created');
        return redirect()->route('CMS::admin.sociales.edit', $social->id);
    }

    public function edit($id)
    {
        $social = $this->socialesRepo->findOrFail($id);

        return view('CMS::sociales.edit', compact('social'));
    }


    public function update($id, UpdateSocial $request)
    {
        $social = $this->socialesRepo->findOrFail($id);
        $this->socialesRepo->update($social, $request->all());

        \Alert::success('CMS::sociales.msg_social_updated');
        return redirect()->route('CMS::admin.sociales.edit', $social->id);

    }

    public function destroy($id)
    {
        $social = $this->socialesRepo->findOrFail($id);
        $this->socialesRepo->delete($social);

        \Alert::success('CMS::sociales.msg_social_deleted');
        return redirect()->route('CMS::admin.sociales.index');
    }


    public function toggleStatus($id)
    {
        $social = $this->socialesRepo->findOrFail($id);
        $data = '';
        $message = null;
        if($social->published_at == null)
        {
            $data['published_at'] = Carbon::now();
            $message = 'CMS::sociales.msg_social_published';
        }
        else
        {
            $data['published_at'] = null;
            $message = 'CMS::sociales.msg_social_unpublished';
        }

        $this->socialesRepo->update($social, $data);
        \Alert::success($message);
        return redirect()->route('CMS::admin.sociales.edit', $social->id);
    }

    public function addFoto(){

         if (Request::ajax())
        {
            $social = new Social();
            $social->contenido       = Request::get('contenido');
            $social->fechaAlta       = date("Y/m/d");
            $social->idCarpeta       = Request::get('carpeta');
            $social->idUser          = Auth::user()->id;
            $social->receptor        = Request::get('escolaridad');

            $social->save();
            return Response::Json($social);
            
        }
    }


    public function obtenerCarpeta()
    {
         // $carpetas = DB::table('cms_carpetas')->get();

       // $carpetas = DB::table('cms_carpetas')->distinct()
        $carpetas = DB::select('select DISTINCT cms_carpetas.id, nombre, (select COUNT(*) from cms_social where idCarpeta = cms_carpetas.id )as numero, cms_carpetas.created_at, cms_carpetas.receptor from cms_carpetas  LEFT JOIN  cms_social  ON cms_carpetas.id = cms_social.idCarpeta');
       // ->where('cms_social.idCarpeta','=','cms_carpetas.id')
       // ->get();
       // ->leftjoin('cms_social','cms_carpetas.id','=','cms_social.idCarpeta')
       // ->get(['cms_carpetas.id','cms_carpetas.nombre', DB::raw('count(cms_social.idCarpeta) WhereRaw cms_social.idCarpeta = cms_carpetas.id  as numero')]);

       return $carpetas;
    }

    public function obtenerFotosAll()
    {
        $fotos = DB::select("select distinct cms_social.id as numeroId, cms_carpetas.id, cms_carpetas.receptor, cms_social.contenido, cms_carpetas.nombre
                                from cms_carpetas, cms_social where cms_carpetas.id = cms_social.idCarpeta");
       return $fotos;
    }

    public function obtenerFotosCarpetaKinder()
    {
        $fotos = DB::select("select distinct cms_carpetas.id, cms_carpetas.receptor, cms_social.contenido, cms_carpetas.nombre
                                from cms_carpetas, cms_social where cms_carpetas.id = cms_social.idCarpeta and cms_carpetas.receptor = 'Kinder'");
       return $fotos;
    }
    public function obtenerFotosCarpetaPrimaria()
    {
        $fotos = DB::select("select distinct cms_carpetas.id, cms_carpetas.receptor, cms_social.contenido, cms_carpetas.nombre
                                from cms_carpetas, cms_social where cms_carpetas.id = cms_social.idCarpeta and cms_carpetas.receptor = 'Primaria'");
       return $fotos;
    }
    public function obtenerFotosCarpetaSecundaria()
    {
        $fotos = DB::select("select distinct cms_carpetas.id, cms_carpetas.receptor, cms_social.contenido, cms_carpetas.nombre
                                from cms_carpetas, cms_social where cms_carpetas.id = cms_social.idCarpeta and cms_carpetas.receptor = 'Secundaria'");
       return $fotos;
    }
    public function obtenerFotosCarpetaBachillerato()
    {
        $fotos = DB::select("select distinct cms_carpetas.id, cms_carpetas.receptor, cms_social.contenido, cms_carpetas.nombre
                                from cms_carpetas, cms_social where cms_carpetas.id = cms_social.idCarpeta and cms_carpetas.receptor = 'Bachillerato'");
       return $fotos;
    }

    public function eliminarCarpeta()
   {
         if(Request::ajax())
         {
            Carpeta::destroy(Request::get('id'));    
         }
   }

     //Galeria
    public function getGaleria()
    {   $id = Request::get('id');
        if (Request::ajax())
        {
            // $galeria = Investimg::where('id_noticia', Request::get('id_noticia'))->get();
            // return Response::json($galeria);
            $galeria = DB::table('cms_social')->where('idCarpeta',$id)->get();

            // $galeria = Social::where('idCarpeta', Request::get('carpeta.id'))->get();
            return Response::json($galeria);
        }
        
    }

    public function setGaleria()
    {
        if (Request::ajax())
        {
            //If Null
           if(Request::file('imagenesMFU') == null){
                $id = Request::get('idCarpeta');
                Social::where('idCarpeta', $id)->delete();
                return;
            }
            $id = Request::get('idCarpeta');
            // $id_noticia = Request::get('id_noticia');
            $files      = Request::file('imagenesMFU');
            $paths      = null;
            $created    = Carbon::now();

            //Directories
            $directory_name = \Config::get('cms.default_path');
            $directory      = $directory_name.'/Sociales/'.$id."/galeria/";

            // Check if the directory exists, if not then create it - OR - Clear old files
            if(!file_exists($directory))  { mkdir($directory, 0777, true); }
            else { File::cleanDirectory($directory); }

            //Clear Database Records
            Social::where('idCarpeta', $id)->delete();

            for($i = 0; $i < count($files); $i++) {

                //Read File Image
                $img = Image::make($files[$i]->getRealPath());

                //File Name
                $file_name = $i."_".time().".".$files[$i]->getClientOriginalExtension();

                //Real Path
                $path = $directory.$file_name;
                
                //Save Image
                $img->save($path);

                //Make paths array
                $q['idCarpeta'] = $id;
                // $q['imagen']     = $path;             
                $q['contenido'] = $path;             
                $q['idUser']   = Auth::user()->id; /*$i;*/
                $q['created_at'] = $created;
                $paths[] = $q;
            }
            
            //Save Record to DB
            DB::table('cms_social')->insert($paths);
        }
    }

}
