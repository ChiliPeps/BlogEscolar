<?php

namespace Gmlo\CMS\Controllers;

use Carbon\Carbon;
use Gmlo\CMS\Modules\Escolaridades\Escolaridad;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gmlo\CMS\Requests\CreateEscolaridad;

use Request;
use Response;
use Auth;
use DB;

class EscolaridadesController extends Controller
{
    public function __construct()
    {
        $this->middleware('CMSAuthenticate');
    }

    public function saludo()
    {
        $escolaridad = Escolaridad::findOrFail(Request::input(1));
        return $escolaridad;
    }
    
    public function index()
    {
        return view('CMS::escolaridades.index');
    }

    public function getData(){

        if(Request::ajax()){
            return Response::json($this->getAllData());
        }
    }

    public function getAllData()
    {
        return Escolaridad::orderBy('nivel_educativo', 'asc')->get();
    }

    public function getEscolaridades()
    {
        return Escolaridad::select('nivel_educativo')->distinct()->get();
    }

    public function actualizar(){

        if (Request::ajax())
        {
            $escolaridad = Escolaridad::findOrFail(Request::input('id'));
            $escolaridad->nivel_educativo = Request::input('nivelEducativo');
            $escolaridad->grado = Request::input('grado');
            $escolaridad->grupo = Request::input('grupo');
            $escolaridad->save();

            return Response::json($escolaridad);
        }
    }

    public  function guardar(CreateEscolaridad $request){
        
        //Save Record
        $escolaridad = new Escolaridad();

        $escolaridad->nivel_educativo = Request::get('nivel_educativo');
        $escolaridad->grado = Request::get('grado');
        $escolaridad->grupo = Request::get('grupo');
        $escolaridad->save();
    }

    public function delete()
    {
          if(Request::ajax())
         {
            Escolaridad::destroy(Request::get('id'));
            // Escolaridad::find(Request::get('id'))->delete;    
         }
    }

    public function obtenerval()
    {
        if (Request::ajax()) {

            $esc = DB::table('cms_tareas')
            ->join('cms_escolaridad', 'cms_tareas.idEscolaridad', '=' , 'cms_escolaridad.id')
            ->where('cms_tareas.id',Request::get('id'))
            ->get(['cms_escolaridad.id','cms_escolaridad.grado', 'cms_escolaridad.grupo', 'cms_escolaridad.nivel_educativo',DB::raw('CONCAT(cms_escolaridad.nivel_educativo,"-",cms_escolaridad.grado,"-",cms_escolaridad.grupo) as gpo')]);
            return  Response::Json($esc);

            // $escolaridad = Escolaridad::where('id',(Request::get('id')))->get();
            // return Response::Json($escolaridad);
        }

    }


    // public function metodo(){

    //     if(Request::ajax()){
    //         return Response::json($this->obtenerval());
    //     }
    // }

   
}
