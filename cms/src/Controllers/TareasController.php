<?php

namespace Gmlo\CMS\Controllers;
use Carbon\Carbon;
use Gmlo\CMS\Modules\Tareas\TareasRepo;
use Gmlo\CMS\Modules\Categories\CategoriesRepo;
use Gmlo\CMS\Modules\Tareas\Tarea;
use Gmlo\CMS\Modules\Noticias\Noticia;
use Gmlo\CMS\Modules\Deportes\Deporte;
use Gmlo\CMS\Modules\Avisos\Aviso;
use Gmlo\CMS\Requests\CreateTarea;
use Gmlo\CMS\Requests\UpdateTarea;
use Gmlo\CMS\Modules\Escolaridades\Escolaridad; // escolaridades
// use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Http\Request;
use Request;
use Response;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class TareasController extends Controller
{
    private $tareasRepo;
    protected $categoriesRepo;

    public function __construct(TareasRepo $tareasRepo, CategoriesRepo $categoriesRepo)
    {
        $this->tareasRepo = $tareasRepo;
        $this->categoriesRepo = $categoriesRepo;
        $this->middleware('CMSAuthenticate');
        view()->share('categories', $this->categoriesRepo->lists('title', 'id'));
    }

    public function index()
    {
        $tareas = $this->tareasRepo->orderBy('created_at', 'desc')->paginate();
        $todastareas = $this->alltareas();
        return view('CMS::tareas.index', compact('tareas','todastareas'));
    }

    public function create()
    {
        return view('CMS::tareas.create');
    }


    public function store(CreateTarea $request)
    {
        $tarea = $this->tareasRepo->storeNew($request->all());

        \Alert::success('CMS::tareas.msg_tarea_created');
        return redirect()->route('CMS::admin.tareas.edit', $tarea->id);
    }

    public function edit($id)
    {
        $tarea = $this->tareasRepo->findOrFail($id);

        return view('CMS::tareas.edit', compact('tarea'));
    }


    public function update($id, UpdateTarea $request)
    {
        $tarea = $this->tareasRepo->findOrFail($id);
        $this->tareasRepo->update($tarea, $request->all());

        \Alert::success('CMS::tareas.msg_tarea_updated');
        return redirect()->route('CMS::admin.tareas.edit', $tarea->id);

    }

    public function destroy($id)
    {
        $tarea = $this->tareasRepo->findOrFail($id);
        $this->tareasRepo->delete($tarea);

        \Alert::success('CMS::tareas.msg_tarea_deleted');
        return redirect()->route('CMS::admin.tareas.index');
    }



    public function showTareasAll()
    {
        if(Request::ajax())
        {
            return Response::json($this->alltareas());
        }
            
    }

    public function showTareasAllBlog()
    {
        if(Request::ajax())
        {
            return Response::json($this->alltareasBlog());
        }            
    }

    public function alltareas()
    {    
        if ( Auth::user()->type == "suadmin" || Auth::user()->type == "admin") {

            //funciona SofftDeletes
           
             $query = Tarea::orderBy('cms_tareas.created_at','desc')
             ->join('cms_users','cms_tareas.idUser','=','cms_users.id')
             ->join('cms_escolaridad','cms_tareas.idEscolaridad','=','cms_escolaridad.id')
             ->get(['cms_tareas.id','cms_tareas.contenido','cms_tareas.fechaAlta','cms_tareas.fechaEntrega','cms_escolaridad.nivel_educativo','cms_escolaridad.grado','cms_escolaridad.grupo','cms_users.name','cms_tareas.tipo','cms_tareas.materia','cms_tareas.profesor','cms_tareas.recomendaciones']);
             return $query;
            
        }

        else{
          $id=Auth::user()->id;
          
          $query = Tarea::orderBy('cms_tareas.created_at','desc')
         ->join('cms_users','cms_tareas.idUser','=','cms_users.id')
         ->join('cms_escolaridad','cms_tareas.idEscolaridad','=','cms_escolaridad.id')
         ->where('cms_tareas.idUser', $id)
         ->get(['cms_tareas.id','cms_tareas.contenido','cms_tareas.fechaAlta','cms_tareas.fechaEntrega','cms_escolaridad.nivel_educativo','cms_escolaridad.grado','cms_escolaridad.grupo','cms_users.name','cms_tareas.tipo','cms_tareas.materia','cms_tareas.profesor','cms_tareas.recomendaciones']);
         return $query;
        }
        
         //nofunciona sofdeltes
         // $query = DB::table('cms_tareas')->distinct()
         // ->join('cms_users','cms_tareas.idUser','=','cms_users.id')
         // ->join('cms_escolaridad','cms_tareas.idEscolaridad','=','cms_escolaridad.id')
         // ->get(['cms_tareas.id','cms_tareas.contenido','cms_tareas.fechaAlta','cms_tareas.fechaEntrega','cms_escolaridad.nivel_educativo','cms_escolaridad.grado','cms_escolaridad.grupo','cms_users.name','cms_tareas.tipo','cms_tareas.materia','cms_tareas.profesor','cms_tareas.recomendaciones']);
         // return $query;
    }
    
    public function alltareasBlog()
    {
        //funciona SofftDeletes
        $mes = date("m");
        $ano = date("Y");
        if($mes == 1)
        {
            $mes = 12;
            $ano = $ano - 1;
        }
        else
        {
            $mes = $mes - 1;
        }
        $fecha = date("Y")."-".date("m")."-".date("d");
        $fecha2 = $ano."-".$mes."-"."21";
        $query = Tarea::orderBy('cms_tareas.created_at','desc')
        ->join('cms_users','cms_tareas.idUser','=','cms_users.id')
        ->join('cms_escolaridad','cms_tareas.idEscolaridad','=','cms_escolaridad.id')
        ->whereBetween('cms_tareas.fechaAlta', [$fecha2, $fecha])
        ->orWhere('cms_tareas.fechaEntrega','LIKE', $fecha)
        ->get(['cms_tareas.id','cms_tareas.codigo','cms_tareas.idEscolaridad','cms_tareas.contenido','cms_tareas.fechaAlta','cms_tareas.fechaEntrega','cms_escolaridad.nivel_educativo','cms_escolaridad.grado','cms_escolaridad.grupo','cms_users.name','cms_tareas.tipo','cms_tareas.materia','cms_tareas.profesor','cms_tareas.recomendaciones']);
        return $query;
    }

    public function addTarea(CreateTarea $request)
    {
        if (Request::ajax())
        {
            $tarea = new Tarea();
            $tarea->idUser          = Auth::user()->id;
            $tarea->idEscolaridad   = Request::get('idEscolaridad');
            $tarea->tipo            = Request::get('tipo');
            $tarea->fechaAlta       = date("Y/m/d");
            $tarea->fechaEntrega    = Request::get('fechaEntrega');
            $tarea->materia         = Request::get('materia');
            $tarea->profesor        = Request::get('profesor');
            $tarea->contenido       = Request::get('contenido');
            $tarea->recomendaciones = Request::get('recomendaciones');
            $tarea->codigo          = Request::get('codigo');

            $tarea->save();
            return Response::Json($tarea);
            
        }
    }

    public function actualizarTarea()
    {
        if (Request::ajax())
        {
            $tarea = Tarea::find(Request::input('id'));
            
            $tarea->idEscolaridad   = Request::get('idEscolaridad');
            $tarea->tipo            = Request::get('tipo');
            $tarea->fechaEntrega    = Request::get('fechaEntrega');
            $tarea->materia         = Request::get('materia');
            $tarea->profesor        = Request::get('profesor');
            $tarea->contenido       = Request::get('contenido');
            $tarea->recomendaciones = Request::get('recomendaciones');

            $tarea->save();
            return Response::Json($tarea);
        }
    }

     public function borrarTarea()
    {
        $this->registradelete();
        
         if(Request::ajax())
         {
            Tarea::destroy(Request::get('id'));    
         }
        
    }
     public function recuperarTarea()
    {
        Tarea::withTrashed()->find(Request::get('id'))->restore();
    }

     public function regresaNivel()
    {   
        $query = DB::table('cms_escolaridad')->distinct()
        ->get(['nivel_educativo']);
        return $query;
    }

    public function regresaGyG()
    {
        $nivel_educativo= Request::get('x');

        $query = Escolaridad::orderBy('created_at', 'asc')
        ->where('nivel_educativo', $nivel_educativo)
        ->get(['id','nivel_educativo','grado','grupo']);
        return $query;

        // no funcionan los softdeletes
        // $nivel_educativo= Request::get('x');
        // $query = DB::table('cms_escolaridad')
        // ->where('nivel_educativo', $nivel_educativo)
        // ->get(['id','nivel_educativo','grado','grupo']);
        // return $query;
    }

    public function borradas()
    {
         $query = Tarea::onlyTrashed()
             ->join('cms_users','cms_tareas.idUser','=','cms_users.id')
             ->join('cms_escolaridad','cms_tareas.idEscolaridad','=','cms_escolaridad.id')
             ->get(['cms_tareas.idUser','cms_tareas.id','cms_tareas.contenido','cms_tareas.fechaAlta','cms_tareas.fechaEntrega','cms_escolaridad.nivel_educativo','cms_escolaridad.grado','cms_escolaridad.grupo','cms_users.name','cms_tareas.tipo','cms_tareas.materia','cms_tareas.profesor','cms_tareas.recomendaciones','cms_tareas.deleted_at']);
             return $query;

       // $query = Tarea::onlyTrashed()->get();
       // return $query;
    }

     public function registradelete()
    {
        $current = Auth::user()->id;
        $nombre = Auth::user()->name;
        $now = date('Y/m/d H:i:s');
        $tabla = "tareas";
        $id = Request::get('id');
        $receptor = Request::get('contenido');

        DB::table('deletes')->insert(
            array('idUserElimino'=>$current,'nombreElimino'=>$nombre ,'fechaBorrado'=>$now, 'tabla'=>$tabla, 'idBorrado'=>$id)
            );
    }

    public function vaciado()
    {
        // if(Request::ajax())
        //  {
            $query = DB::table('vaciado')
            ->get(['tablaVaciado','idUserUltimoVaciado','nombreUltimoVaciado','fechaVaciado','Cantidad']);
            // ->get(['idUserUltimoVaciado']);
            // ->get(['nombreUltimoVaciado']);
            // ->get(['Cantidad']);
            return $query;
        // }
    }

    public function truncate()
    {
        $tabla = Request::get('tabla'); 

        if ($tabla == "Tareas") 
        {
            Tarea::truncate();
            $cantidad = Request::get('cantidad');
            $cantidadact = $cantidad + 1;
            $current = Auth::user()->id;
            $nombre = Auth::user()->name;
            $now = date('Y/m/d H:i:s');
            DB::table('vaciado')->where('tablaVaciado','Tareas')
            ->update(array('idUserUltimoVaciado'=>$current, 'nombreUltimoVaciado'=>$nombre, 'Cantidad'=> $cantidadact, 'fechaVaciado' => $now));
            // ->update(['idUserUltimoVaciado'=>$current, 'nombreUltimoVaciado'=>$nombre, 'Cantidad'=> $cantidadact, 'fechaVaciado' => $now]);
        }
        elseif ( $tabla == "Noticias")
        {   DB::statement('SET FOREIGN_KEY_CHECKS=0;');
            Noticia::truncate();
            DB::table('cms_tag_noticia')->truncate();
            DB::table('cms_noticias')->truncate();
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');

            $cantidadact = (Request::get('cantidad') + 1);
            $current = Auth::user()->id;
            $nombre = Auth::user()->name;
            $now = date('Y/m/d H:i:s');
            DB::table('vaciado')->where('tablaVaciado','Noticias')
            ->update(array('idUserUltimoVaciado'=>$current, 'nombreUltimoVaciado'=>$nombre, 'Cantidad'=> $cantidadact, 'fechaVaciado' => $now));

        }

        elseif( $tabla == "Deportes")
        {
            Deporte::truncate();
            $cantidadact = (Request::get('cantidad') + 1);
            $current = Auth::user()->id;
            $nombre = Auth::user()->name;
            $now = date('Y/m/d H:i:s');
            DB::table('vaciado')->where('tablaVaciado','Deportes')
            ->update(array('idUserUltimoVaciado'=>$current, 'nombreUltimoVaciado'=>$nombre, 'Cantidad'=> $cantidadact, 'fechaVaciado' => $now));
        }
        else/*$tabla == "Avisos")*/
        {
            Aviso::truncate();

            $cantidadact = (Request::get('cantidad') + 1);
            $current = Auth::user()->id;
            $nombre = Auth::user()->name;
            $now = date('Y/m/d H:i:s');
            DB::table('vaciado')->where('tablaVaciado','Avisos')
            ->update(array('idUserUltimoVaciado'=>$current, 'nombreUltimoVaciado'=>$nombre, 'Cantidad'=> $cantidadact, 'fechaVaciado' => $now));
        }
     
    }

    public function LastID()
    {
          
          $lastid = DB::table('cms_tareas')->select('id')->orderBy('id', 'desc')->first();
          // if ($lastid == null) {
          //     // $lastid == 0;
          //     return Response::json(0);
          // }
          // else{
            return Response::json($lastid);
          // }
          
    }
    
    public function grafica()
    {
        $tareas = array();
        $avisos = array();
        for ($i=1; $i<=12 ; $i++) 
        { 
             $tareas[$i-1]  = Tarea::whereYear('created_at', '=', Request::get('year'))
              ->whereMonth('created_at', '=', $i)
              ->count();

              $avisos[$i-1]  = Aviso::whereYear('created_at', '=', Request::get('year'))
              ->whereMonth('created_at', '=', $i)
              ->count();
        }
        
        $chart['tareas'] = $tareas;
        $chart['avisos'] = $avisos;

        return Response::json($chart);
    }

}
