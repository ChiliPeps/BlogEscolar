<?php

namespace Gmlo\CMS\Controllers;
use Carbon\Carbon;
use Gmlo\CMS\Modules\Deportes\DeportesRepo;
use Gmlo\CMS\Modules\Categories\CategoriesRepo;
use Gmlo\CMS\Modules\Deportes\Deporte;
use Gmlo\CMS\Requests\CreateDeporte;
use Gmlo\CMS\Requests\UpdateDeporte;
// use Gmlo\CMS\Modules\Escolaridades\Escolaridad; // escolaridades
// use Illuminate\Http\Request;
use Request;
use Response;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class DeportesController extends Controller
{
    private $deportesRepo;
    protected $categoriesRepo;

    public function __construct(DeportesRepo $deportesRepo, CategoriesRepo $categoriesRepo)
    {
        $this->deportesRepo = $deportesRepo;
        $this->categoriesRepo = $categoriesRepo;
        $this->middleware('CMSAuthenticate');
        view()->share('categories', $this->categoriesRepo->lists('title', 'id'));
    }

    public function index()
    {
        $deportes = $this->deportesRepo->orderBy('created_at', 'desc')->paginate();
        $todosdeportes = $this->alldeportes();
        return view('CMS::deportes.index', compact('deportes','todosdeportes'));
    }

    public function create()
    {
        return view('CMS::deportes.create');
    }


    public function store(CreateDeporte $request)
    {
        $deporte = $this->deportesRepo->storeNew($request->all());

        \Alert::success('CMS::deportes.msg_deporte_created');
        return redirect()->route('CMS::admin.deportes.edit', $deporte->id);
    }

    public function edit($id)
    {
        $deporte = $this->deportesRepo->findOrFail($id);

        return view('CMS::deportes.edit', compact('deporte'));
    }


    public function update($id, UpdateDeporte $request)
    {
        $deporte = $this->deportesRepo->findOrFail($id);
        $this->deportesRepo->update($deporte, $request->all());

        \Alert::success('CMS::deportes.msg_deporte_updated');
        return redirect()->route('CMS::admin.deportes.edit', $deporte->id);

    }

    public function destroy($id)
    {
        $deporte = $this->deportesRepo->findOrFail($id);
        $this->deportesRepo->delete($deporte);

        \Alert::success('CMS::deportes.msg_deporte_deleted');
        return redirect()->route('CMS::admin.deportes.index');
    }

    public function addADeportes()
    {
        if (Request::ajax())
        {
            $deporte = new Deporte();
            $deporte->idUser          = Auth::user()->id;
            $deporte->fechaAlta       = date("Y/m/d");
            $deporte->tipoContenido   = Request::get('tipoContenido');
            $deporte->tipoDeporte     = Request::get('tipoDeporte');
            $deporte->contenido        = Request::get('contenido');
            
            $deporte->save();
            return Response::Json($deporte);
        }
    }

    public function updateDeportes()
    {
        if (Request::ajax())
        {
            $deporte = Deporte::find(Request::input('id'));
            $deporte->tipoContenido   = Request::get('tipoContenido');
            $deporte->tipoDeporte     = Request::get('tipoDeporte');
            $deporte->contenido        = Request::get('contenido');
            
            $deporte->save();
            return Response::Json($deporte);
        }
    }

     public function showDeportes()
    {
        if(Request::ajax())
        {
            return Response::json($this->alldeportes());
        }
            
    }
     public function alldeportes()
    {
         $query = DB::table('cms_deportes')->distinct()
         ->join('cms_users','cms_deportes.idUser','=','cms_users.id')
         ->get(['cms_deportes.id','cms_users.name','cms_deportes.contenido','cms_deportes.fechaAlta','cms_deportes.tipoContenido','cms_deportes.tipoDeporte']);
         return $query;
    }

    public function borrarDeporte()
    {
        if(Request::ajax())
         {
            Deporte::destroy(Request::get('id'));    
         }
    }

}
