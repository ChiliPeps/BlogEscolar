<?php

return [
    'user_types' => [
        'suadmin' => 'Super admin',
    ],
    'update_password'       => 'Actualizar Password',
    'block_user'            => 'Bloquear Usuario',
    'unblock_user'          => 'Desbloquear',
    'user_blocked'          => 'Bloqueado',
    'update_my_password'    => 'Actualizar password',
    'edit_user'             => 'Editar usuario',
    'users'                 => 'Usuarios',
    'create_new_user'       => 'Crear nuevo usuario',
    'msg_user_created'      => 'Usuario creado!',
    'msg_user_updated'      => 'Usuario actualizado!',
    'msg_you_cant_delete_yourself' => 'No puedes borrarte tu mismo!',
    'msg_password_updated'  => 'Password actualizado!',
    'msg_user_blocked'      => 'Usuario Bloqueado!',
    'msg_user_unblocked'    => 'Usuario Desbloqueado!',
    'msg_you_cant_block_yourself' => 'No puedes bloquearte tu mismo!',
    'msg_user_deleted' => 'Usuario borrado!',
];