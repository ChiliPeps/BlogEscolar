<?php

return [
    'avisos' => 'Avisos',
    'create_new_aviso' => 'Crear nueva aviso',
    'content'   => 'Content',
    'seo'       => 'SEO',
    'gallery'   => 'Gallery',
    'publish'   => 'Publish',
    'primary_image' => 'Primary image',
    'msg_aviso_created' => 'Aviso created',
    'edit_aviso' => 'Edit aviso',
    'msg_aviso_updated' => 'Aviso updated!',
    'unpublish' => 'Unpublish',
    'msg_aviso_published' => 'Aviso published!',
    'msg_aviso_unpublished' => 'Aviso unpublished!',
    'msg_aviso_deleted' => 'Aviso deleted!',
];