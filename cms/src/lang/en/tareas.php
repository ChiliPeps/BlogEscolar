<?php

return [
    'tareas' => 'Tareas',
    'create_new_tarea' => 'Crear nueva tarea',
    'content'   => 'Content',
    'seo'       => 'SEO',
    'gallery'   => 'Gallery',
    'publish'   => 'Publish',
    'primary_image' => 'Primary image',
    'msg_tarea_created' => 'Tarea created',
    'edit_tarea' => 'Edit tarea',
    'msg_tarea_updated' => 'Tarea updated!',
    'unpublish' => 'Unpublish',
    'msg_tarea_published' => 'Tarea published!',
    'msg_tarea_unpublished' => 'Tarea unpublished!',
    'msg_tarea_deleted' => 'Tarea deleted!',
];