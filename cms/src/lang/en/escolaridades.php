<?php

return [
    'escolaridades' => 'Escolaridades',
    'create_new_escolaridad' => 'Create new escolaridad',
    'content'   => 'Content',
    'seo'       => 'SEO',
    'gallery'   => 'Gallery',
    'publish'   => 'Publish',
    'primary_image' => 'Primary image',
    'msg_escolaridad_created' => 'Escolaridad created',
    'edit_escolaridad' => 'Edit escolaridad',
    'msg_escolaridad_updated' => 'Escolaridad updated!',
    'unpublish' => 'Unpublish',
    'msg_escolaridad_published' => 'Escolaridad published!',
    'msg_escolaridad_unpublished' => 'Escolaridad unpublished!',
    'msg_escolaridad_deleted' => 'Escolaridad deleted!',
];