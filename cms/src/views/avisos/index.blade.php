@extends('CMS::master')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-comments-o"></i> @lang('CMS::avisos.avisos')
        </h1>
    </section>
    <section class="content" id="app">
      {{--  <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"> </h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('CMS::admin.avisos.create') }}" class="btn bg-navy"><i class="fa fa-plus-circle"></i> @lang('CMS::core.create_new')</a>
                </div>                
                
            </div>  --}}
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Aviso</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
                 
                    <div class ="col-md-3">
                        <select class ="form-control" id="sltTipoAviso" name="tipo" v-model="aviso.tipo"  v-on:change = "calendario()" v-bind:style = "[errores.tipo ?  inputErrorStyle:'']">
                            <option value="0" disabled selected>Tipo de aviso</option>
                            <option value="mensaje"> Mensaje </option>
                            <option value ="calendario"> Calendario </option>
                            <option value = "circular"> Circular </option>
                        </select>
                    </div>
                    {{-- SELECT PARA MENSAJES --}}
                    <div class ="col-md-3"  v-show="mostrar">
                        <select v-model="aviso.para" class ="form-control" id="sltPara" disabled  v-on:change="metodo(), metodo2()">
                            <option value="0" disabled selected>Receptor de aviso</option>
                            <option value="masivo"> Todos </option>
                            <option value="grupo"> Grupo </option>
                            <option value ="personal"> Personal </option>
                        </select>
                    </div>

                    {{-- SELECT PARA CALENDARIOS --}}
                    <div class ="col-md-3"  v-show="calendarioF">
                        <select v-model="aviso.nombreMes" class ="form-control" id="sltmes">
                            <option value="" disabled selected>Seleccione un mes</option>
                            <option v-for="m in meses" v-bind:value="m.t" >
                                @{{m.t}}
                            </option>
                        </select>
                        <br>
                    </div> 

                    <div class ="col-md-3">

                        <select v-model="aviso.nivel" class ="form-control" id="sltNivel" name="sltNivel" disabled
                         v-on:change = "regresaGyG(), metodo2()">
                            <option value="0" disabled selected>Seleccione un nivel</option>
                             @if(Auth::user()->type == "suadmin" || Auth::user()->type == "multinivel" || Auth::user()->type == "admin")
                                <option v-for="nivel in nivelJson"> {{--  v-bind:value="nivel.value" --}}                               
                                @{{ nivel.nivel_educativo}}
                               @else
                               <option>
                               {{Auth::user()->type}}
                               @endif
                            </option>                         
                        </select>
                    </div>
                    
                    <div class ="col-md-3" v-show="mostrar">

                        <select class ="form-control" id="sltReceptor" disabled name="receptor" v-model="aviso.receptor">
                            <option value="0" disabled selected>Seleccione un grupo</option>
                            <option v-for="gg in ggJson"> @{{gg.nivel_educativo}} @{{gg.grado}}-@{{gg.grupo}}</option>
                        </select>
                    
                        <br>
                    </div>

                    <div class ="col-md-9" >
                        <input type="text" class="form-control" placeholder="Titulo de aviso" name='titulo' v-model="aviso.titulo" v-bind:style = "[errores.titulo ?  inputErrorStyle:'']">
                    </div>

                    <div class ="col-md-12" >
                        <br>
                        <textarea name="contenido" id="txtContenido" rows="15" cols="150" 
                        v-editor="aviso.contenido">
                               
                        </textarea>
                        <br>

                        <button v-show = "btnadd" type="button" class="btn btn-primary" @click="agregarAviso()" > Agregar </button> 
                        <button v-show = "btnupdate" type="button" class="btn btn-primary" @click="actualizarAviso()" > Actualizar </button> 
                        <button v-show = "btncancel" type="button" class="btn btn-danger" @click="cancelar()" > Cancelar </button> 

                    </div>
                 </div>  

            </div>
            <!-- /.box-body -->
         
            <div class="box box-primary">
                <div class="box-body table-responsive no-padding" style="display:block;">
                    <table class ="table table-bordered table-hover ">
                        <thead>
                            <tr class ="info">
                                <th>Id</th>
                                <th>Usuario</th>
                                <th>Tipo</th>

                                <th>Receptor</th>
                                <th>Titulo</th>
                                <th>Contenido</th>
                                <th>Publicacion</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for = "a in avisosJson">
                                <td>@{{a.id}}</td>
                                <td>@{{a.name}}</td>
                                <td>
                                    @{{a.tipo}} <div v-if="a.nombreMes != 0" style="display:inline;">/@{{a.nombreMes}} </div>
                                </td>         
                                <td>@{{a.receptor}}</td>
                                <td>@{{a.titulo}}</td>
                                <td> 
                                    <button @click="modalContenido(a.id)" type="button" class="btn btn-info text-center" > Ver </button> 
                                </td>           
                                <td>@{{a.fechaAlta}}</td>
                                <td>
                                    <button type="button" class="btn btn-danger" @click="eliminar(a.id)" >X</button> 
                                    
                                    <a href="#app"><button type="button" class="btn btn-warning" @click="editar(a.id)">  <i class="fa fa-repeat"></i> </button> </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    {{-- <pre> @{{ avisosJson | json}}</pre> --}}
                </div>
            </div>

            {{-- <div class="box-footer clearfix">
                {!! $avisos->render() !!}
            </div> --}}
            <modal :show.sync="showModal">

                <div slot = "head"> Contenido del aviso</div>
                <div slot="body">
                    @{{{ver.contenido}}}
                </div>

            </modal>

  tipo:    @{{aviso.tipo}} <br>  
  para:    @{{aviso.para}}<br>
  nivel :  @{{aviso.nivel}}<br>
  receptor: @{{aviso.receptor}}
    </section>

    @section('scripts')
        @include('CMS::avisos.partials.scripts')
    @stop
@endsection