@include('CMS::partials.vueModalTarea')
@include('CMS::partials.vuePagination')

<script>
       Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

     Vue.component('modal', {
        template: '#modal-template',
        props: {
            show: {
                type: Boolean,
                required: true,
                twoWay: true
            }
        }
    }),
     
     //nueva directiva editor (v-editor) para mandar ckeditor a aviso.contenido
    Vue.directive('editor', {
        twoWay: true,

    bind: function () {
        this.vm.$nextTick(this.setupEditor.bind(this));
    },

    setupEditor: function () {
       
        CKEDITOR.env.isCompatible = true;
        var self = this;
        var finder = CKEDITOR.replace(this.el.id);
        CKFinder.setupCKEditor(finder, '../ckfinder/');
        CKEDITOR.instances[this.el.id].on('change', function () {
        self.set(CKEDITOR.instances[self.el.id].getData());

        });
    },

    update: function (value) {
        // value = value || ' ';
        if (!CKEDITOR.instances[this.el.id]) {
            return this.vm.$nextTick(this.update.bind(this, value));
        }
        CKEDITOR.instances[this.el.id].setData('',value,value);
    },

    unbind: function () {
        CKEDITOR.instances[this.el.id].destroy();
    }
  
  }); // fin directiva editor

new Vue({

    ready: function () { this.showAvisosAll(), this.regresaNivel(), this.calendario() },


    el: '#app',

    data: {

         btnupdate:false,
         btncancel:false,
         btnadd:true,
         busqueda: '',
         tipo:'',
         showModal: false,
         mostrar:true,
         calendarioF:false,

        //Json Data
        loading: false,
        loadingModal: false,
        showError: false,
        originalJson: null, //Json Original
        avisosJson: null,     //Json que se muestra
        originalNivel:null,
        nivelJson:null,
        originalgg:null,
        ggJson:null,

        meses:[
            {t:'anual'},
           { t:'enero', value:'enero'}, {t:'febrero'}, {t:'marzo'}, {t:'abril'}, {t:'mayo'},
           { t:'junio'}, {t:'julio'}, {t:'agosto'}, {t:'septiembre'}, {t:'octubre'}, {t:'noviembre', }, {t:'diciembre'}
        ],
        //Json Pagination
        pag: {
            currentPage: 0,
            itemsPerPage: 15,
            resultCount: 0
        },

        xx:'',
        cal:'',
        aviso:{
            idaviso:'',
            idUser:'',
            contenido:'',
            fechaAlta:'',
            receptor:'',
            tipo:'',
            titulo:'',
            nombreMes:'',
            para:'',
            nivel:''
        },

        ver:{
            contenido:''
        },
         errores: {
                tipo: false,
                titulo: false,    
                contenido: false
            },

            bolsaErrores: null,

            inputErrorStyle: {
                outline: 'none',
                'border-color':       'red',
                '-webkit-box-shadow': '0px 0px 17px 0px rgba(255, 0, 0, 1)',
                '-moz-box-shadow':    '0px 0px 17px 0px rgba(255, 0, 0, 1)',
                'box-shadow':         '0px 0px 17px 0px rgba(255, 0, 0, 1)'
            }
      
    },

    computed: {
        //Json Pagination
        totalPages: function() {
            return Math.ceil(this.pag.resultCount / this.pag.itemsPerPage)
        }
    },

    methods: {
        //Mostrar todas los avisos/administrador
        showAvisosAll: function () {
            this.loading = true
            var resource = this.$resource("{{route('CMS::admin.avisos.todos')}}")
            resource.get({}).then(function (response) {
                //alert(JSON.stringify(response.data))
                this.$set('originalJson', response.data)
                this.avisosJson = this.originalJson
                this.loading = false
                
            });
         },
         agregarAviso: function(tipo,receptor,titulo,contenio,fechaAlta,nombreMes,para,nivel){

            var form = new FormData();
            // form.append('idUser');
            form.append('contenido', this.aviso.contenido);
            form.append('receptor', this.aviso.receptor);
            form.append('tipo', this.aviso.tipo);
            form.append('titulo', this.aviso.titulo);
            form.append('fechaAlta', this.aviso.fechaAlta);
            form.append('nombreMes', this.aviso.nombreMes);
            form.append('para', this.aviso.para);
            form.append('nivel', this.aviso.nivel);
           
            this.save(form, "{{route('CMS::admin.avisos.agregarAviso')}}")
            // document.getElementById("sltTipoAviso").value="0";
            // this.calendarioF=false
            // this.mostrar=true
         },

         actualizarAviso: function(tipo,receptor,titulo,contenio,fechaAlta,nombreMes,para,nivel){
             var form = new FormData();

            form.append('id', this.aviso.idaviso);
            form.append('contenido', this.aviso.contenido);
            form.append('receptor', this.aviso.receptor);
            form.append('tipo', this.aviso.tipo);
            form.append('titulo', this.aviso.titulo);
            form.append('fechaAlta', this.aviso.fechaAlta);
            form.append('nombreMes', this.aviso.nombreMes);
            form.append('para', this.aviso.para);
            form.append('nivel', this.aviso.nivel);

            this.save(form, "{{route('CMS::admin.avisos.actualizarAviso')}}")
       
            document.getElementById("sltTipoAviso").value="0";
            document.getElementById("sltReceptor").value="0";
            document.getElementById("sltNivel").value="0";
            document.getElementById("sltPara").value="0";
  
            this.btnadd = true
            this.btncancel = false
            this.btnupdate = false
            this.mostrar=true
            this.calendarioF=false
         },

         regresaNivel: function(){
            var resource = this.$resource("{{route('CMS::admin.avisos.niveles')}}")
            resource.get({}).then(function(response){
                this.$set('originalNivel', response.data)
                this.nivelJson=this.originalNivel
            });
         },

         regresaGyG: function() {
        
            var x = document.getElementById("sltNivel").value;
            var resource = this.$resource("{{route('CMS::admin.avisos.gradogrupo')}}")
            resource.get({x}).then(function(response){
            this.$set('originalgg', response.data)
            this.ggJson=this.originalgg 
            });
         },

         eliminar:function(id){
            var Confirmbox= confirm("Deseas eliminar el aviso con id:" + id + "?")
            if(Confirmbox) var resource = this.$resource("{{route('CMS::admin.avisos.eliminar')}}")
            resource.delete({id:id}).then(function (response) { 
            this.notificationFx('se ha borrado el aviso ', "success", "fa fa-check-circle-o fa-3x")
            }, function (response) { 
                this.notificationFx("Se ha producido un error.", "error", "fa fa-exclamation-triangle fa-3x")
            })
        },

        editar:function(idRecord){

            var Confirmbox= confirm("Deseas editar el aviso con id: " + idRecord + "?")
            if(Confirmbox)
            var id = this.findIndexByKeyValue(this.avisosJson,'id',idRecord)

            this.$data.aviso.idaviso = idRecord
            this.$data.aviso.tipo = this.avisosJson[id]['tipo']
            this.$data.aviso.titulo = this.avisosJson[id]['titulo']
            this.$data.aviso.contenido = this.avisosJson[id]['contenido']
            $("#sltPara").removeAttr('disabled');
            this.$data.aviso.para = this.avisosJson[id]['para']

                if (this.avisosJson[id]['tipo'] == 'calendario')
                {   
                    this.mostrar=false
                    this.calendarioF=true;
                    this.$data.aviso.nombreMes=this.avisosJson[id]['nombreMes']
                    // $("#sltNivel").removeAttr('disabled');
                    // $("#sltPara").val('grupo');
                    $("#sltNivel").removeAttr('disabled'); 
                   
                    // document.getElementById("#sltNivel").disabled=false;
                    this.$data.aviso.nivel = this.avisosJson[id]['nivel']
                    this.$data.aviso.receptor =  this.$data.aviso.nivel 
                    



                }
                if (this.avisosJson[id]['para'] == 'grupo') 

                {
                    document.getElementById("sltNivel").value = 0;
                    $("#sltNivel").removeAttr('disabled');
                    $("#sltPara").val('grupo');
                    document.getElementById("sltNivel").value = this.avisosJson[id]['nivel'];
                    $("#sltReceptor").attr('disabled', 'disabled'); 
                    this.$data.aviso.nivel = this.avisosJson[id]['nivel']
                    this.$data.aviso.receptor = document.getElementById("sltNivel").value; 
                }

                else if(this.avisosJson[id]['para'] == 'personal'){
              
                    document.getElementById("sltReceptor").value = 0;

                    $("#sltPara").val('personal');
                    $("#sltNivel").removeAttr('disabled');
                    $("#sltReceptor").removeAttr('disabled');
                    this.$data.aviso.nivel = this.avisosJson[id]['nivel']
                    this.$data.aviso.receptor=this.avisosJson[id]['receptor']

                    var x = this.avisosJson[id]['nivel'];
                    var resource = this.$resource("{{route('CMS::admin.avisos.gradogrupo')}}")
                    resource.get({x}).then(function(response){
                    this.$set('originalgg', response.data)
                    this.ggJson=this.originalgg 
                    });
                    this.$data.aviso.receptor == this.avisosJson[id]['receptor']
                    // alert(this.avisosJson[id]['receptor']);

                    }

                else if (this.avisosJson[id]['para'] == 'masivo')
                    {
                        document.getElementById("sltNivel").value = 0;
                        document.getElementById("sltReceptor").value = 0;
                        $("#sltNivel").attr('disabled', 'disabled');
                        $("#sltReceptor").attr('disabled', 'disabled');
                        $("#sltNivel").attr('disabled', 'disabled');
                        $("#sltReceptor").attr('disabled', 'disabled'); 
                        this.$data.aviso.receptor = 'Todos'
                    }

            this.btnadd = false
            this.btnupdate = true
            this.btncancel = true
        },

        metodo:function(){
            this.xx = document.getElementById("sltPara").value;

            
            if (this.xx == "masivo") 
                { debugger;
                    this.aviso.receptor = "Todos";
                    this.aviso.nivel = 0;
                } 
            
        },
        metodo2:function(){

            this.cal = document.getElementById("sltmes").value;
            this.xx = document.getElementById("sltPara").value;
            var x = document.getElementById("sltNivel").value;
            if (this.xx == "grupo" || this.cal != null && this.cal != 0) 
                {
                    
                    this.aviso.receptor=x;
                }            
        },

        limpiaForm: function() {
            // e.preventDefault()
            this.cleanInputErrors()
            $("#sltPara").attr('disabled', 'disabled'); 
            $("#sltNivel").attr('disabled', 'disabled');
            $("#sltReceptor").attr('disabled', 'disabled'); 

            this.$data.aviso.titulo = ""
            this.$data.aviso.nombreMes = ""
            this.$data.aviso.contenido = ""

            this.aviso.para = 0
            this.aviso.nivel = 0
            this.aviso.receptor = 0

        },

        cancelar: function(){
            this.limpiaForm()
            this.btncancel= false
            this.btnupdate=false
            this.btnadd=true
            this.mostrar=true
            this.calendarioF=false
            document.getElementById("sltTipoAviso").value="0";
            // this.aviso.tipo="Tipo de aviso"
        },

        calendario: function()
        {
            // debugger;
            if (this.aviso.tipo == "calendario") 
                {
                   this.mostrar=false
                   this.calendarioF=true
                }
            else
            {
                this.mostrar=true
                this.calendarioF=false
            }  ;


        },

          //Vue Http Resources
            save: function (data, route, msgSuccess) {
                var resource = this.$resource(route)
                resource.save(data).then(function (response) {
                    this.success()
                    this.limpiaForm()
                    this.notificationFx("El aviso se agrego correctamente", "success", "fa fa-check-circle-o fa-3x")
                    document.getElementById("sltTipoAviso").value="0";
                    this.calendarioF=false
                    this.mostrar=true

                }, function (response) {
                    // this.fail()
                    this.cleanInputErrors()
                    this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
                    this.inputErrors(response.data)
                })
            },

            notificationFx: function (message, tipo, icon) {
                var notification = new NotificationFx({
                    message : '<span><i class="'+icon+'"></i></span><p>'+message+'</p>',
                    layout : 'bar',
                    effect : 'slidetop',
                    type : tipo // notice, warning, error or success
                })
                notification.show()
                this.showAvisosAll()
            },

             modalContenido: function(idRecord){
            var id = this.findIndexByKeyValue(this.originalJson,'id', idRecord);
            this.showModal = true;
            this.ver.contenido=this.originalJson[id]['contenido'];
             },

            findIndexByKeyValue: function (arraytosearch, key, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i][key] == valuetosearch) {
                        return i;
                    }
                }
                return null;
        },

        inputErrors: function (campos) {
                this.bolsaErrores = campos
                for (error in this.errores) {
                    if(campos.hasOwnProperty(error)) {
                        this.errores[error] = true
                    }
                }
            },

        cleanInputErrors: function () {
                this.bolsaErrores = null
                for (error in this.errores) {
                    this.errores[error] = false
                }
            },
            //Vue Http Response Handler
            success: function (response) {
                this.loadingModal = false
                this.showError = false
                this.showModal = false
                this.showModalMessage = false
            },
            fail: function (response) {
                this.loadingModal = false
                this.showError = true
            },

        },
              
    filters: {

        //Json Pagination
        paginate: function(list) {
            this.pag.resultCount = list.length
            if (this.pag.currentPage > this.totalPages) {
                this.pag.currentPage = this.totalPages - 1
            }
            var index = this.pag.currentPage * this.pag.itemsPerPage
            return list.slice(index, index + this.pag.itemsPerPage)
   
        }
    }     
});

</script>