<!-- Vue Components & Mixins -->
{{-- @include('CMS::partials.vueModal') --}}
@include('CMS::partials.vueModalTarea')
@include('CMS::partials.vuePagination')

<script>
    //Laravel's Token
     Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

     Vue.component('modal', {
        template: '#modal-template',
        props: {
            show: {
                type: Boolean,
                required: true,
                twoWay: true
            }
        }
    }),

     //nueva directiva editor (v-editor) para mandar ckeditor a add.contenido
    Vue.directive('editor', {
        twoWay: true,

    bind: function () {
        this.vm.$nextTick(this.setupEditor.bind(this));
    },

    setupEditor: function () {

        var self = this;
        CKEDITOR.env.isCompatible = true;
        var finder = CKEDITOR.replace(this.el.id);
        CKFinder.setupCKEditor(finder, '../ckfinder/');
        CKEDITOR.instances[this.el.id].on('change', function () {
        self.set(CKEDITOR.instances[self.el.id].getData());

        });

    },

    update: function (value) {
        // value = value || ' ';
        if (!CKEDITOR.instances[this.el.id]) {
            return this.vm.$nextTick(this.update.bind(this, value));
        }
        CKEDITOR.instances[this.el.id].setData('',value,value);
    },

    unbind: function () {
        CKEDITOR.instances[this.el.id].destroy();
    }
  
  }); // fin directiva editor

new Vue({

    ready: function () { this.showDeportesAll() },


    el: '#app',

    data: {
         btnadd:true,
         btnupdate:false,
         btncancel:false,
         busqueda: '',
         tipo:'',
         showModal: false,

        //Json Data
        originalJson: null, //Json Original
        deportesJson: null,     //Json que se muestra

        //Error/Success/Loading Elements
        loading: false,
        loadingModal: false,
        showError: false,
        
        

        //Json Pagination
        pag: {
            currentPage: 0,
            itemsPerPage: 15,
            resultCount: 0
        },

        add: {
            idDeporte:'',
            tipoDeporte:'',
            tipoContenido:'',
            contenido:''

        },

        ver:{
            contenido:''
        }

      
    },

    computed: {
        //Json Pagination
        totalPages: function() {
            return Math.ceil(this.pag.resultCount / this.pag.itemsPerPage)
        }
    },

    methods: {
        //Mostrar todas las tareas/administrador
        showDeportesAll: function () {
            this.loading = true
            var resource = this.$resource("{{route('CMS::admin.deportes.todos')}}")
            resource.get({}).then(function (response) {
                //alert(JSON.stringify(response.data))
                this.$set('originalJson', response.data)
                this.deportesJson = this.originalJson
                this.loading = false
                
            });
         },


         agregarADeportes: function(tipoContenido,tipoDeporte,contenido) {

            var form = new FormData();
           
            form.append('tipoContenido', this.add.tipoContenido);
            form.append('tipoDeporte', this.add.tipoDeporte);
            form.append('contenido', this.add.contenido);
            
            this.save(form,"{{route('CMS::admin.deportes.agregarADeportes')}}")
        },

        actualizarDeportes: function(tipoContenido,tipoDeporte,contenido){
             var form = new FormData();
           
            form.append('id', this.add.idDeporte);
            form.append('tipoContenido', this.add.tipoContenido);
            form.append('tipoDeporte', this.add.tipoDeporte);
            form.append('contenido', this.add.contenido);
            
            this.save(form,"{{route('CMS::admin.deportes.actualizarDeportes')}}")
        },

        eliminar:function(id){
            var Confirmbox= confirm("Deseas eliminar la publicacion con id:" + id + "?")
            if(Confirmbox) var resource = this.$resource("{{route('CMS::admin.deportes.eliminar')}}")
            resource.delete({id:id}).then(function (response) { 
            this.notificationFx('se ha borrado la publicacion ', "success", "fa fa-check-circle-o fa-3x")
            }, function (response) { 
                this.notificationFx("Se ha producido un error.", "error", "fa fa-exclamation-triangle fa-3x")
            })
        },

        editar:function (idRecord){

             var Confirmbox= confirm("Deseas editar el aviso con id: " + idRecord + "?")
             if (Confirmbox)
             var id = this.findIndexByKeyValue(this.originalJson,'id',idRecord)
             this.$data.add.idDeporte = idRecord
             this.$data.add.tipoDeporte = this.deportesJson[id]['tipoDeporte']
             this.$data.add.tipoContenido = this.deportesJson[id]['tipoContenido']
             this.$data.add.contenido = this.deportesJson[id]['contenido']

             this.btnupdate = true
             this.btncancel = true
             this.btnadd = false
        },

        cancelar: function (){
            this.$data.add.tipoDeporte = "Seleccione un deporte"
            this.$data.add.tipoContenido = "Tipo Publicacion"
            this.$data.add.contenido= ""
            this.btnupdate = false
            this.btncancel = false
            this.btnadd = true
        },

         notificationFx: function (message, tipo, icon) {
            var notification = new NotificationFx({
                message : '<span><i class="'+icon+'"></i></span><p>'+message+'</p>',
                layout : 'bar',
                effect : 'slidetop',
                type : tipo // notice, warning, error or success
            })
            notification.show()
            this.showDeportesAll()
            
        },

        limpiaForm: function() {
            // e.preventDefault()
            this.$data.add.tipoDeporte = "Seleccione un deporte"
            this.$data.add.tipoContenido= "Tipo Publicacion"
            this.$data.add.contenido = ""
            this.btnupdate= false
            this.btncancel=false
            this.btnadd=false
           
        },

        //Vue Http Resources
            save: function (data, route, msgSuccess) {
            var resource = this.$resource(route)
            resource.save(data).then(function (response) {
                this.success()
                this.limpiaForm()
                this.notificationFx('La tarea se agregó correctamente', "success", "fa fa-check-circle-o fa-3x")
            }, function (response) {
                this.errors()
                this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
            })
        },

        delete: function (data, route, msgSuccess) {
            var resource = this.$resource(route)
            resource.delete(data).then(function (response) {
                this.success()
                this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
            }, function (response) {
                this.errors()
                this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
            })
        },
        modalContenido: function(idRecord){

            var id = this.findIndexByKeyValue(this.originalJson,'id', idRecord);
            this.showModal = true;
            this.ver.contenido = this.originalJson[id]['contenido']
        },

        findIndexByKeyValue: function (arraytosearch, key, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i][key] == valuetosearch) {
                        return i;
                    }
                }
                return null;
        },

        

           success: function (response) {
                this.loadingModal = false
                this.showError = false
                this.showModal = false
                this.showModalMessage = false
                this.showDeportesAll()
            },

    },

   
      
              

    filters: {

        //Json Pagination
        paginate: function(list) {
            this.pag.resultCount = list.length
            if (this.pag.currentPage > this.totalPages) {
                this.pag.currentPage = this.totalPages - 1
            }
            var index = this.pag.currentPage * this.pag.itemsPerPage
            return list.slice(index, index + this.pag.itemsPerPage)
   
        }
    }     
});



</script>