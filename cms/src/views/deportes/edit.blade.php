@extends('CMS::master')

@section('content')
<section class="content-header">
  <h1>
    <i class="fa fa-file"></i> @lang('CMS::tareas.edit_tarea')
  </h1>
</section>

<section class="content">
    {!! Form::model($tarea, ['route' => ['CMS::admin.tareas.update', $tarea->id], 'method' => 'PUT']) !!}
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">
                <a href="{{ route('CMS::admin.tareas.index') }}" class="btn btn-default"><i class="fa fa-chevron-circle-left"></i> @lang('CMS::core.back')</a>
                <button type="submit" class="btn bg-navy"><i class="fa fa-floppy-o"></i> @lang('CMS::core.save')</button>
            </div>
        </div>
        <div class="box-body"></div>
    </div>

    @include('CMS::tareas.partials.inputs')

    {!! Form::close()  !!}
</section>

{!! Field::renderDeleteButtonDialogs() !!}

{!! Form::open(['route' => ['CMS::admin.tareas.toggle-status', $tarea->id], 'id' => 'frmToggleStatus', 'method' => 'PUT']) !!}
{!! Form::close() !!}
@stop

@section('scripts')
    @include('CMS::tareas.partials.scripts')
@stop