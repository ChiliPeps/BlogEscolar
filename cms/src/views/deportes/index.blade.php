@extends('CMS::master')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-file-word-o"></i> @lang('CMS::deportes.deportes')
        </h1>
    </section>
    <section class="content" id="app">
        
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Deportes de St.Johns</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
                
                <div class = "col-md-12">
                
                        <div class ="col-md-4">
                        <select v-model="add.tipoDeporte" class ="form-control">
                            <option disabled selected> Seleccione un deporte </option>
                            <option> Futbol </option>
                            <option> Natacion </option>
                            <option> Gimnasio </option>
                            
                        </select>
                    </div>

                    <div class ="col-md-4">
                        <select v-model="add.tipoContenido" class ="form-control">
                            <option  disabled selected> Tipo Publicacion </option>
                            <option> Aviso </option>
                            <option> Horario </option>
                            <option> Calendario </option>
                        </select>
                    </div>

                    <div id="el">
                        <div  class="col-md-12">
                        <br>
                             <textarea name="txtTarea" id="txtContenido" rows="10" cols="150" v-editor="add.contenido">
                                      
                            </textarea>
                        <br>
                        </div> 
                    </div>

                    <div class = "col-md-12">
                        <button v-show="btnadd" @click="agregarADeportes()" type="button" class="btn btn-info text-center" >Agregar </button>
                        <button v-show="btnupdate" @click="actualizarDeportes()" type="button" class="btn btn-info text-center" >Actualizar </button>
                        <button v-show="btncancel" @click="cancelar()" type="button" class="btn btn-danger text-center" >Cancelar </button>
                    </div>

                </div>

            </div>
            <!-- /.box-body -->
          </div>

          <div class="box box-primary">

            <div class="box-body table-responsive no-padding" style="display:block;">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="info">
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Tipo Deporte</th>
                                    <th>Tipo Contenido</th>
                                    <th>Contenido</th> 
                                    <th>FechaAlta</th>                                
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="d in deportesJson">

                                    <td>@{{d.id}}</td>
                                    <td>@{{d.name}}</td>
                                    <td>@{{d.tipoDeporte}}</td>
                                    <td>@{{d.tipoContenido}}</td>                           
                                
                                    <td> <button @click="modalContenido(d.id)" type="button" class="btn btn-info text-center" > Ver </button> </td>
                                    </td>

                                   <td>@{{d.fechaAlta}}</td>
                         
                                    <td>
                                        <button type="button" class="btn btn-danger" @click="eliminar(d.id)" >X</button> 
                                    
                                       <a href="#app"> <button type="button" class="btn btn-warning" @click="editar(d.id)" >  <i class="fa fa-repeat"></i> </button> </a>
                                    </td>
                                </tr>

                                    </td>
                                                                
                                </tr>

                            </tbody>
                         </table> 

            </div>

          </div>
    

         <modal :show.sync="showModal">

                <div slot = "head"> Contenido</div>
                <div slot="body">
                    @{{{ver.contenido}}}
                </div>

            </modal>

    </section>

    @section('scripts')
        @include('CMS::deportes.partials.scripts')
    @stop
@endsection