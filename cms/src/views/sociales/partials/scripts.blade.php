<!-- Vue Components & Mixins -->
{{-- @include('CMS::partials.vueModal') --}}
@include('CMS::partials.vueModalTarea')
@include('CMS::partials.vuePagination')

<script>
    //Laravel's Token
     Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

     Vue.component('modal', {
        template: '#modal-template',
        props: {
            show: {
                type: Boolean,
                required: true,
                twoWay: true
            }
        }
    }),

new Vue({

    ready: function () { this.regresaCarpetas() },


    el: '#app',

    data: {

         busqueda: '',
         tipo:'',
         showModal: false,
         panel:true,
         panelMFU: false,
         carpetaMFU: { id:'', nombre: '' },

         galeriaJson:[],

         public_url: "{{ URL::to('/') }}/",
         //Image Render Data Array
        renderImages: [],
        imageData: [],
        fileData: [],

        //Json Data
        originalJson: null, //Json Original
        carpetasJson: null,     //Json que se muestra

        //Error/Success/Loading Elements
        loading: false,
        loadingModal: false,
        showError: false,

        //Json Pagination
        pag: {
            currentPage: 0,
            itemsPerPage: 15,
            resultCount: 0
        },

        social:{
                carpeta: '',
                escolaridad: '',
                contenido:''
        },
        add: {
            nombre: '',
            receptor:''
        },

        ver:{
            nombre:''
        }

      
    },

    computed: {
        //Json Pagination
        totalPages: function() {
            return Math.ceil(this.pag.resultCount / this.pag.itemsPerPage)
        }
    },

    methods: {
        //Mostrar todas las tareas/administrador
        metodoagregarcarpeta: function (nombre, receptor) {
            
            var form = new FormData();
            form.append('nombre', this.add.nombre);
            form.append('receptor', this.add.receptor);
            this.save(form,"{{route('CMS::admin.sociales.agregarCarpeta')}}")
            this.limpiarForm()
         },

         regresaCarpetas: function(){
            this.loading = true
            var resource = this.$resource("{{route('CMS::admin.sociales.todasCarpetas')}}")
            resource.get({}).then(function (response) {
                //alert(JSON.stringify(response.data))
                this.$set('originalJson', response.data)
                this.carpetasJson = this.originalJson
                this.loading = false
                
            });
         },

         eliminarCarpeta: function(id, nombre){
            var Confirmbox = confirm("Deseas eliminar la carpeta " + nombre + "?")
            if(Confirmbox) var resource = this.$resource("{{route('CMS::admin.sociales.eliminarCarpeta')}}")
            resource.delete({id:id}).then(function (response) { 
            this.notificationFx('se ha borrado la Carpeta ', "success", "fa fa-check-circle-o fa-3x")
            this.regresaCarpetas()
            }, function (response) { 
                this.notificationFx("Se ha producido un error.", "error", "fa fa-exclamation-triangle fa-3x")
            })
            
         },

         agregarFoto: function(carpeta,escolaridad,contenido){
            var form = new FormData();
            form.append('carpeta',this.social.carpeta);
            form.append('escolaridad',this.social.escolaridad);
            form.append('contenido',this.social.contenido);
            this.save(form,"{{route('CMS::admin.sociales.agregarFoto')}}")

         },

        // eliminar:function(id){
        //     var Confirmbox= confirm("Deseas eliminar el eleento con id:" + id + "?")
        //     if(Confirmbox) var resource = this.$resource("{{route('CMS::admin.tareas.eliminar')}}")
        //     resource.delete({id:id}).then(function (response) { 
        //     this.notificationFx('se ha borrado la tarea ', "success", "fa fa-check-circle-o fa-3x")
        //     }, function (response) { 
        //         this.notificationFx("Se ha producido un error.", "error", "fa fa-exclamation-triangle fa-3x")
        //     })
        // },

         notificationFx: function (message, tipo, icon) {
            var notification = new NotificationFx({
                message : '<span><i class="'+icon+'"></i></span><p>'+message+'</p>',
                layout : 'bar',
                effect : 'slidetop',
                type : tipo // notice, warning, error or success
            })
            notification.show()
            // this.showTareasAll()
        },

        //Vue Http Resources
            save: function (data, route, msgSuccess) {
            var resource = this.$resource(route)
            resource.save(data).then(function (response) {
                this.success()
                this.notificationFx('La informacion se agrego correctamente', "success", "fa fa-check-circle-o fa-3x")
            }, function (response) {
                this.errors()
                this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
            })
        },

        delete: function (data, route, msgSuccess) {
            var resource = this.$resource(route)
            resource.delete(data).then(function (response) {
                this.success()
                this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
            }, function (response) {
                this.errors()
                this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
            })
        },
        modalContenido: function(idRecord){
            var id = this.findIndexByKeyValue(this.originalJson,'id', idRecord);
            this.showModal = true;
            this.ver.contenido=this.originalJson[id]['contenido'];
        },
        fotos: function(){
            this.$resource("{{route('CMS::admin.sociales.guardar')}}")
        },

         openMFU: function(carpeta) {
            // this.panelIndex = false
            this.panelMFU = true
            this.panel=false
            this.carpetaMFU.id     = carpeta.id
            this.carpetaMFU.nombre = carpeta.nombre
            // this.loadingMFU = true

            var resource = this.$resource("{{route('CMS::admin.sociales.galeria.get')}}");
            resource.get({id: carpeta.id}).then(function (response) {
                this.$set('galeriaJson', response.data);

                //Callbacks
                for (var i = 0; i < this.galeriaJson.length; i++) {
                    //Fetch & Blob
                    // this.ajaxFetch(this.public_url+this.galeriaJson[i].imagen, i)
                    this.ajaxFetch(this.public_url+this.galeriaJson[i].contenido, i)
                }

                // this.loadingMFU = false
            });
        },
         closeMFU: function() {
            this.galeriaJson = []
            this.renderImages = []
            this.imageData = []
            this.fileData = []
            this.socialesMFU = { id:'', nombre: '' }
            this.panelMFU = false
            this.panel = true
            this.regresaCarpetas()
            // this.panelIndex = true
        },
         updateMFU: function() {
            // this.loadingUploadMFU = true
            var form = new FormData();
            if(this.fileData.length > 0) {
                for (var i = 0; i < this.fileData.length; i++) {
                    form.append('imagenesMFU[]', this.fileData[i], this.imageData[i].name);
                }
            }
            form.append('idCarpeta', this.carpetaMFU.id);
            var resource = this.$resource("{{route('CMS::admin.sociales.galeria.set')}}")
            resource.save(form).then(function (response) {
                this.notificationFx("Galería Actualizada!", "success", "fa fa-check-circle-o fa-3x")
                this.loadingUploadMFU = false
            }, function (response) {
                this.notificationFx("Se ha producido un error.", "error", "fa fa-exclamation-triangle fa-3x")
                this.loadingUploadMFU = false
            });
        },
                //Image Load & Render
        onFileChangeMFU: function(e) {                 
            var files = e.target.files || e.dataTransfer.files

            if (!files.length)
                return;

            if(!this.isValidImageMFU(files)) {
                this.removeImageMFU()
                return;
            }

            this.createImageMFU(files)
        },
        createImageMFU: function(files) {

            for (var i = 0; i < files.length && i < 8; i++) {
                var image = new Image()
                var reader = new FileReader()
                reader.onload = (e) => { 
                    this.renderImages.push(e.target.result); 
                }
                reader.readAsDataURL(files[i])

                //File Normal Data
                this.imageData.push({ name: files[i].name, size: files[i].size+' Bytes' })

                //File Important Data
                this.fileData.push(files[i])
            }
        },
        isValidImageMFU: function (files) {
            var _validFileExtensions = ["jpg", "jpeg", "png"];
            for (var i = 0; i < files.length; i++) {

                //Size
                if (files[i].size > 2097152) { return false } //2 MB

                //Type
                type = files[i].name.split('.')[files[i].name.split('.').length - 1].toLowerCase()
                if (_validFileExtensions.indexOf(type) == -1) { return false }

            } return true
        },
        removeImageMFU: function (index) {
            this.renderImages.splice(index, 1)
            this.imageData.splice(index, 1)
            this.fileData.splice(index, 1)
        },

        ajaxFetch: function(url, order) {
            fetch(url).then((res) => { //Check for Safari fetch()
                return res.blob();
            }).then((blob) => {

                //Add blob
                this.fileData.push(blob);

                 //File name
                var f = getFilename(url);
                var name = f.filename+'.'+f.ext;

                //File Normal Data
                this.imageData.push({ name: name, size: 'En el servidor' })

                readBlobAsDataUrl(blob, (dataUrl) => {
                    //Add Rendered Image
                    this.renderImages.push(dataUrl)
                    
                })
            })
        },

        findIndexByKeyValue: function (arraytosearch, key, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i][key] == valuetosearch) {
                        return i;
                    }
                }
                return null;
        },

        limpiarForm: function(){
             this.$data.add.nombre = ""
        },

           success: function (response) {
                this.loadingModal = false
                this.showError = false
                this.showModal = false
                this.showModalMessage = false
                this.regresaCarpetas()
            },

    },



    filters: {

        //Json Pagination
        paginate: function(list) {
            this.pag.resultCount = list.length
            if (this.pag.currentPage > this.totalPages) {
                this.pag.currentPage = this.totalPages - 1
            }
            var index = this.pag.currentPage * this.pag.itemsPerPage
            return list.slice(index, index + this.pag.itemsPerPage)
   
        }
    }     
});

var readBlobAsDataUrl = function(blob, cb) {
    var a = new FileReader();
    a.onload = function(e) {
        cb(e.target.result);
    };
    a.readAsDataURL(blob);
}

function getFilename(url) {
  // returns an object with {filename, ext} from url (from: http://coursesweb.net/ )

  // get the part after last /, then replace any query and hash part
  url = url.split('/').pop().replace(/\#(.*?)$/, '').replace(/\?(.*?)$/, '');
  url = url.split('.');  // separates filename and extension
  return {filename: (url[0] || ''), ext: (url[1] || '')}
}

</script>