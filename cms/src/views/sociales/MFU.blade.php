<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Imágenes para <b>Sociales </b></h3>
        <div class="box-tools pull-right">
            <button @click="updateMFU" class="btn bg-green"><i class="fa fa-upload"></i>  Actualizar Galería <i v-show="loadingUploadMFU" class="fa fa-spinner fa-spin"></i> </button>
            <button @click="closeMFU" class="btn bg-navy"><i class="fa fa-close"></i> Cerrar</button>
        </div>
    </div>

    {{-- Loading MFU --}}
    <div class="text-center"><i v-show="loadingMFU" class="fa fa-spinner fa-spin fa-5x"></i></div>

    <div class="box-body">
        <div class="row">
            <div class="col-md-12">

                <h3>(Máximo 8 Imágenes)</h3>

                {{-- File Input --}}
                <div class="text-center" v-if="renderImages.length <= 8">
                    <div class="fileUpload btn btn-primary btn-lg">
                        <span>Subir imagenes</span>
                        <input name="fileMFU" type="file" class="upload" multiple @change="onFileChangeMFU">
                    </div>
                </div>


                {{-- Image List - style => flexislots --}}
                <ul v-show="renderImages" class="mailbox-attachments clearfix flexislots">

                    <li v-for="r in renderImages" track-by="$index">
                        <span class="mailbox-attachment-icon" style="padding: 0;"><img :src="r" style="max-width: 100%; height: auto;"></span>

                        <div class="mailbox-attachment-info">
                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i> @{{imageData[$index].name}}</a>
                            <span class="mailbox-attachment-size">
                                @{{imageData[$index].size}}
                                <button class="btn btn-default btn-xs pull-right" @click="removeImageMFU($index)"><i class="fa fa-trash-o"></i></button>
                            </span>
                        </div>
                    </li>

                </ul>

            </div>
        </div>
    </div>


</div> 