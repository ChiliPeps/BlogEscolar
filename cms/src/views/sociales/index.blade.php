@extends('CMS::master')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-file-word-o"></i> @lang('CMS::sociales.sociales')
        </h1>
    </section>
    <section class="content" id="app">
        
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Carpeta</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
                <div class= "col-md-12">
                    <div class="col-md-4">
                        <input v-model="add.nombre" type="text" class="form-control"  placeholder="Nombre de carpeta"></input>
                    </div>
                    
                       <div class="col-md-4">
                         <select class ="form-control" v-model="add.receptor">
                            <option  disabled selected> Selecciona la escolaridad</option>
                            <option> Kinder </option>
                            <option> Primaria </option>
                            <option> Secundaria </option>
                            <option> Bachillerato </option>
                         </select>
                        </div>
                    <div class="col-md-2">
                        <button type="button"  @click="metodoagregarcarpeta" class="btn btn-info text-center">Crear carpeta</button>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>

        <div class="box box-primary" v-show="panel">
            <div class="box-header with-border">
                <h3 class="box-title">Social</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            <!-- /.box-tools -->                
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                    <!-- /.box-body -->
                <div class="box-body table-responsive no-padding" style="display:block;">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="info">

                                    <th> Nombre </th>
                                    <th> Fotos </th>
                                    <th> Dirigido a </th>
                                    <th> Fecha </th>
                                    <th></th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="c in carpetasJson">

                                    <td> @{{c.nombre}} </td>
                                    <td> @{{c.numero}} </td>
                                    <td> @{{c.receptor}} </td>
                                    <td> @{{c.created_at}} </td>
                                    <td>
                                        <button @click="openMFU(c)" type="button" class="btn btn-info text-center">Manejar Carpeta</button>
                                        <button @click="eliminarCarpeta(c.id,c.nombre)" type="button" class="btn btn-danger text-center"> Eliminar </button>
                                    </td>
                                        
                                    </tr>
                              
                            </tbody>
                        </table>
                    </div>

            </div>
        </div>
        
             <!-- Panel MultiFileUpload -->
        <div v-show="panelMFU">
            @include('CMS::sociales.MFU')
        </div>
        
        </div>

    </section> 

    @section('scripts')
        @include('CMS::sociales.partials.scripts')
   
    @stop

@endsection