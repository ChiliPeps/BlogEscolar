<!-- Vue Components & Mixins -->
@include('CMS::partials.vueModalTarea')
@include('CMS::partials.vuePagination')

<script>
    //Laravel's Token
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

    Vue.component('modal', {
        template: '#modal-template',
        props: {
            show: {
                type: Boolean,
                required: true,
                twoWay: true
            }
        }
    })

    new Vue({

        ready: function () { this.getAllData(), this.tareasBorradas(), this.avisosBorrados() },

        el: '#app',

        //Mixins
        mixins: [pagination],

        data: {

            //Json Data
            originalJsonM: null, //Json Original
            showJsonM: [],     //Json que se muestra
            originalBorradas:null,
            showBorradas:[],
            originalAvisos:null,
            showAvisos:[],
            ver:{
                contenido:'',
                contenidoAviso:''
                },

            //Error/Success/Loading Elements
            loading: false,
            loadingModal: false,
            showError: false,
            // actionOn: false, //??

            //Modal Vars
            showModal: false,

            busqueda: '', //Textbox busqueda
            tipo: '',     //Dropdown tipo
        
        },

        methods: {

            //Get All Data
            getAllData: function () {
                this.loading = true
                var resource = this.$resource("{{route('CMS::admin.vaciado.datos')}}")
                resource.get({}).then(function (response) {
                    //alert(JSON.stringify(response.data))
                    this.$set('originalJsonM', response.data)
                    this.showJsonM = this.originalJsonM
                    this.loading = false
                });
            },

            truncate: function (tabla, cantidad){

                var ConfirmBox = confirm("Deseas Borrar la tabla ? , esto no se puede deshacer" )
                if(ConfirmBox) var resource = this.$resource("{{route('CMS::mantenimiento.tarea.truncate')}}")
                resource.delete({tabla:tabla, cantidad:cantidad}).then(function (response){
                this.notificationFx('Se ha vaciado la tabla',"success","fa fa-exclamation-triangle fa-3x")
                })
       
            },   

            tareasBorradas: function()
            {
                this.loading = true
                var resource = this.$resource("{{route('CMS::admin.tareas.borradas')}}")
                resource.get({}).then(function (response) {
                    //alert(JSON.stringify(response.data))
                    this.$set('originalBorradas', response.data)
                    this.showBorradas = this.originalBorradas
                    this.loading = false
                });
            },

            avisosBorrados: function()
            {
                this.loading = true
                var resource = this.$resource("{{route('CMS::admin.avisos.borrados')}}")
                resource.get({}).then(function (response) {
                    //alert(JSON.stringify(response.data))
                    this.$set('originalAvisos', response.data)
                    this.showAvisos = this.originalAvisos
                    this.loading = false
                });
            },

            modalContenido: function(idRecord){
            var id = this.findIndexByKeyValue(this.originalBorradas,'id', idRecord);
            this.showModal = true;
            this.ver.contenido = this.originalBorradas[id]['contenido'];
             },

            modalAvisos: function(idRecord){
            var id = this.findIndexByKeyValue(this.originalAvisos,'id', idRecord);
            this.showModal = true;
            this.ver.contenido = this.originalAvisos[id]['contenido'];
             },

             findIndexByKeyValue: function (arraytosearch, key, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i][key] == valuetosearch) {
                        return i;
                    }
                }
                return null;
            },

            recuperar: function(id)
            {
                var resource = this.$resource("{{route('CMS::admin.tareas.recuperarTarea')}}")
                resource.get({id:id}).then(function (response){
                this.notificationFx('La tarea se recupero',"success","fa fa-exclamation-triangle fa-3x")
                })


            },

             recuperarAviso: function(id)
            {
                var resource = this.$resource("{{route('CMS::admin.avisos.recuperarAviso')}}")
                resource.get({id:id}).then(function (response){
                this.notificationFx('El aviso se recupero',"success","fa fa-exclamation-triangle fa-3x")
                })


            },

            notificationFx: function (message, tipo, icon) {
                var notification = new NotificationFx({
                    message : '<span><i class="'+icon+'"></i></span><p>'+message+'</p>',
                    layout : 'bar',
                    effect : 'slidetop',
                    type : tipo // notice, warning, error or success
                })
                notification.show()
                this.getAllData()
                this.tareasBorradas()
                this.avisosBorrados()
            },     

            //Ajax Functions
   

            //Vue Http Resources
            save: function (data, route, msgSuccess) {
                var resource = this.$resource(route)
                resource.save(data).then(function (response) {
                    this.success()
                    this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
                this.getAllData();

                }, function (response) {
                    this.fail()
                    this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
                })
            },
            //Vue Http Response Handler
            success: function (response) {

                this.loadingModal = false
                this.showError = false
                this.showModal = false
                this.showModalMessage = false
                this.getAllData();
                
            },
            fail: function (response) {
                this.loadingModal = false
                this.showError = true
            },
        },

        // filters: {
        //     onlyNumberDisplay: {
        //         read: function(val) {

        //             return isNaN(val) ? '' : val
        //         },
        //         write: function(val, oldVal) {
        //             var number = +val.replace(/[^\d]/g, '')
        //             console.log(number);
        //             return (number == 0) ? '' : number
        //         }
        //     },
        //     onlyTextDisplay: {
        //         read: function(val) {
        //             return val
        //         },
        //         write: function(val, oldVal) {
        //             var text = '';
        //             text = text + val.replace(/[\d]/g, '')
        //             console.log(text)
        //             return text
        //         }
        //     }
        // }

    });

</script>