@extends('CMS::master')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-file-word-o"></i> @lang('Administrar BLOG')
        </h1>
        <br>
    </section>

{{--    <ul class="nav nav-tabs">
	  <li role="presentation" class="active"><a href="#">Home</a></li>
	  <li role="presentation"><a href="#">Profile</a></li>
	  <li role="presentation"><a href="#">Messages</a></li>
	</ul> --}}
<section id="app" class="content" >
	<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab_1" aria-expanded="true">@lang('Vaciar tablas ')</a></li>
                <li><a data-toggle="tab" href="#tab_2" aria-expanded="false"> Tareas Eliminadas </a></li>
                <li><a data-toggle="tab" href="#tab_3" aria-expanded="false"> Avisos Eliminados </a></li>
            </ul>
            <div class="tab-content">
                <div id="tab_1" class="tab-pane active">
                    <div class="box box-primary">
                        <div class="box-body table-responsive no-padding" style="display:block;">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr class="info">
                                        <th class="text-center"> Tabla </th>
                                        <th class="text-center">Id/Nombre(Quien borro)</th>
                                        {{-- <th> Cantidad </th> --}}
    									<th> Ultimo Vaciado</th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="v in originalJsonM">
                                        <td class="text-center">@{{v.tablaVaciado}} </td>
                                        <td class="text-center">@{{v.idUserUltimoVaciado}} / @{{v.nombreUltimoVaciado}} </td>  
                                        {{-- <td>@{{v.Cantidad}} </td>--}}
                                        <td>@{{v.fechaVaciado}}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger" @click="truncate(v.tablaVaciado, v.Cantidad)">Vaciar Contenido</button> 
                                        </td>

                                    </tr>       
                                </tbody>
                             </table> <br>

                        </div>       
                    </div>
                  
                </div>

                <div id="tab_2" class="tab-pane">

                    <div class="row">
                        <div class="col-md-3">
                           <input type="text" v-model="busqueda">
                        </div>
                        <div class="col-md-3">
                            <select name="" id="" v-model="tipo">
                                <option selected value="id">id</option>
                                <option value="materia">materia</option>
                                <option value="profesor">profesor</option>
                               
                            </select>
                        </div>
                    </div>

              		<div class="box-body table-responsive no-padding" style="display:block;">
                        <br>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="info">
                                    <th> Usuario</th>
                                    <th> Id Tarea </th>
                                    <th> Tipo </th>
                                    <th> Publicacion</th> 
                                    <th> Entrega </th>
                                    <th> Materia </th>
                                    <th>Profesor</th>
                                    <th>Escolaridad</th>
                                    <th>Recmendaciones</th>
                                    <th>Fecha de borrado</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="t in showBorradas | filterBy busqueda in tipo"> <!--  -->
                                    <td>@{{t.name}}</td>
                                    <td class="text-center">@{{t.id}}</td>
                                    <td>@{{t.tipo}}</td>
                                    <td>@{{t.fechaAlta}}</td>
                                    <td>@{{t.fechaEntrega}}</td>
                                    <td>@{{t.materia}}</td>
                                    <td>@{{t.profesor}}</td>
                                    <td>@{{t.nivel_educativo}}
                                        @{{t.grado}}
                                        @{{t.grupo}}
                                    </td>
                                    <td>@{{t.recomendaciones}}</td>
                                    <td>@{{t.deleted_at}}</td>
                                    <td> 
                                        <button @click="modalContenido(t.id)" type="button" class="btn btn-info text-center" > Ver </button> 
                                        <button @click="recuperar(t.id)" type="button" class="btn btn-success text-center" > Recuperar </button>
                                    </td>

                                </tr>
                            </tbody>
                         </table> 

                    </div>
                </div>

                 <modal :show.sync="showModal">
        
                    <div slot = "head"> Contenido de la tarea</div>
                        <div slot="body">
                            @{{{ver.contenido}}}
                        </div>
        
                </modal>

                <div id="tab_3" class="tab-pane">
					<div class="box-body table-responsive no-padding" style="display:block;">
                        <br>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="info">
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Tipo</th>
                                    <th>Receptor</th>
                                    <th>Titulo</th>
                                    <th>Publicacion</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="a in showAvisos "> <!--  -->
                                    <td>@{{a.id}}</td>
                                    <td>@{{a.name}}</td>
                                    <td>@{{a.tipo}}</td>
                                    <td>@{{a.receptor}}</td>
                                    <td>@{{a.titulo}}</td>          
                                    <td>@{{a.fechaAlta}}</td>
                                    <td>
                                        <button @click="modalAvisos(a.id)" type="button" class="btn btn-info text-center" > Ver </button> 

                                        <button @click="recuperarAviso(a.id)" type="button" class="btn btn-success text-center" > Recuperar </button> 
                                                                               
                                    </td>

                                </tr>
                            </tbody>
                         </table> 

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    @section('scripts')
        @include('CMS::mantenimiento.partials.scripts')
    @stop
@endsection