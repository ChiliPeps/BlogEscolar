@extends('CMS::master')

@section('content')
    <section class="content-header">
        <h1>
            Dashboard
        </h1>
    </section>

<?php
use Carbon\Carbon;
$years = array_combine(range(Carbon::now()->year, 2016), range(Carbon::now()->year, 2016));
?>

<section class="content">
        @if (Auth::user()->name == "Padres") 
           <h1>Porfavor cierra sesion e inicia como administraor</h1>
        @else

            {!! Alert::render() !!}
            @lang('CMS::core.welcome') {{ Auth::user()->name }}      
        
        <br><br>
        <div class="col-md-12" id="app">Tareas mes
                <div class="nav-tabs-custom">

                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab_1" aria-expanded="true"> Totales </a></li>
                        <li><a data-toggle="tab" href="#tab_2" aria-expanded="false"> Por Escolaridad </a></li>
                        <li><a data-toggle="tab" href="#tab_3" aria-expanded="false"> Por Maestro </a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab_1" class="tab-pane active">
                            <div class="box box-primary">
                                <div class="chart">
                                    <div class="col-md-6">
                                {!! Field::select('year', $years, \Carbon\Carbon::now()->year, ['id' => 'tipo', 'label' => 'Año', 'v-model' => 'year', 'v-on:change' => 'getGrafica']) !!}
                                    </div>

                                    {{-- <canvas id="barChart" style="height: 180px; width: 703px;" height="180" width="703"></canvas> --}}
                                     <canvas id="myChart" width="300" height="80"></canvas>
                                </div>   
                            </div>
                          
                        </div>

                        <div id="tab_2" class="tab-pane">
            
                        </div>



                        <div id="tab_3" class="tab-pane">
              
                        </div>
                    </div>
                </div>
        </div> 

    </section>
     @endif

@endsection


@section('scripts')
    @include('CMS::partials.homescripts')
@stop