<script>
var select2 = {
  directives: {
       select2:  {
           twoWay: true,
           bind: function () {
             var self = this
             $(this.el)
               .select2({
                 //data: optionsData,
                 language: "es"
               })
               .on('change', function () {
                 self.set(this.value)
               })
           },
           update: function (value) {
             $(this.el).val(value).trigger('change')
           },
           unbind: function () {
             $(this.el).off().select2('destroy')
           }
       }
   }
}
</script>
