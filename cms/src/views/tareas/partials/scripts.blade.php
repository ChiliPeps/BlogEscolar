<!-- Vue Components & Mixins -->
{{-- @include('CMS::partials.vueModal') --}}
@include('CMS::partials.vueModalTarea')
@include('CMS::partials.vuePagination')


<script>
    //Laravel's Token
     Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

     Vue.component('modal', {
        template: '#modal-template',
        props: {
            show: {
                type: Boolean,
                required: true,
                twoWay: true
            }
        }
    }),


     //nueva directiva editor (v-editor) para mandar ckeditor a add.contenido
  //   Vue.directive('editor', {
  //       twoWay: true,

  //   bind: function () {
  //       this.vm.$nextTick(this.setupEditor.bind(this));
  //   },

  //   setupEditor: function () {
  //       CKEDITOR.env.isCompatible = true;
  //       var self = this;
  //       var finder = CKEDITOR.replace(this.el.id);
  //       CKFinder.setupCKEditor(finder, '../ckfinder/');
  //       CKEDITOR.instances[this.el.id].on('change', function () {
  //       self.set(CKEDITOR.instances[self.el.id].getData());

  //       });
  //   },

  //   update: function (value) {
  //       value = value || ' ';
  //       if (!CKEDITOR.instances[this.el.id]) {
  //           return this.vm.$nextTick(this.update.bind(this, value));
  //       }
  //       CKEDITOR.instances[this.el.id].setData(value);
  //   },

  //   unbind: function () {
  //       CKEDITOR.instances[this.el.id].destroy();
  //   }
  
  // }); // fin directiva editor

new Vue({

    ready: function () { this.showTareasAll(),this.regresaNivel(), this.regresaultimoid() },


    el: '#app',

         directives: {
            datepicker:  {
                bind: function () {
                    $(this.el).datepicker({
                        todayBtn: true,
                        todayHighlight: true,
                        autoclose: true,
                        format: 'yy/mm/dd',
                        language: 'es'
                    });
                },
                update: function (value) {
                    $(this.el).datepicker('update', value);
                }
            },
        editor:{
           
            twoWay: true,

            bind: function () {
               Vue.nextTick(this.setupEditor.bind(this));
            },

            setupEditor: function () {
                CKEDITOR.env.isCompatible = true;
                var self = this;
                var finder = CKEDITOR.replace(this.el.id);
                CKFinder.setupCKEditor(finder, '../ckfinder/');
                CKEDITOR.instances[this.el.id].on('change', function () {
                self.set(CKEDITOR.instances[self.el.id].getData());

                });
            },

            update: function (value) {
                // value = value || ' ';
                if (!CKEDITOR.instances[this.el.id]) {
                    return this.vm.$nextTick(this.update.bind(this, value));
                }
                CKEDITOR.instances[this.el.id].setData('',value,value);
            },

            unbind: function () {
                CKEDITOR.instances[this.el.id].destroy();
            }
        },

            select:  {
               twoWay: true,
               bind: function () {
                 var self = this
                 $(this.el)
                   .select2({
                     // data: optionsData,
                     language: "es"
                   })
                   .on('change', function () {
                     // self.set(this.value)
                     self.set($(self.el).val())
                   })
               },
               update: function (value) {
                 $(this.el).val(value).trigger('change')
               },
               unbind: function () {
                 $(this.el).off().select2('destroy')
               }
           }

     },
     
     // Mixins
    mixins: [pagination],

    data: {

         btnadd: true,
         btnupdate: false,
         btncancel:false,
         busqueda: '',
         tipo:'',
         showModal: false,
         uno:false,
         dos:true,

        //Json Data
        originalJson: null, //Json Original
        tareasJson: [],     //Json que se muestra

        lastid:'',
        ultimoid:'',

        //Error/Success/Loading Elements
        loading: false,
        loadingModal: false,
        showError: false,
        originalNivel:null,
        nivelJson:null,
        originalgg:null,
        ggJson:null,
        originalVal:null,
        valorJson:null,

        //Json Pagination
        // pag: {
        //     currentPage: 0,
        //     itemsPerPage: 15,
        //     resultCount: 0
        // },

        add: {
            idtarea:'',
            tipo:'',
            fechaAlta:'',
            fechaEntrega:'',
            materia:'',
            profesor:'',
            idEscolaridad:'',
            idmultipleEscolaridad:[],
            contenido:'',
            recomendaciones:'',
            nivel:'',
            codigo:''
        },

        ver:{
            contenido:''
        },

         errores: {
                tipo: false,          contenido: false,         
                profesor: false,      fechaEntrega: false,    
                materia: false     
            },

            bolsaErrores: null,

            inputErrorStyle: {
                outline: 'none',
                'border-color':       'red',
                '-webkit-box-shadow': '0px 0px 17px 0px rgba(255, 0, 0, 1)',
                '-moz-box-shadow':    '0px 0px 17px 0px rgba(255, 0, 0, 1)',
                'box-shadow':         '0px 0px 17px 0px rgba(255, 0, 0, 1)'
            }
      
    },

    // computed: {
    //     //Json Pagination
    //     totalPages: function() {
    //         return Math.ceil(this.pag.resultCount / this.pag.itemsPerPage)
    //     }
    // },

    methods: {
        //Mostrar todas las tareas/administrador
        showTareasAll: function () {
            this.loading = true
            var resource = this.$resource("{{route('CMS::admin.tareas.todas')}}")
            resource.get({}).then(function (response) {
                //alert(JSON.stringify(response.data))
                this.$set('originalJson', response.data)
                this.tareasJson = this.originalJson
                this.loading = false
                
            });
         },

         regresaNivel: function(){
            var resource = this.$resource("{{route('CMS::admin.tareas.niveles')}}")
            resource.get({}).then(function(response){
                this.$set('originalNivel', response.data)
                this.nivelJson=this.originalNivel
            });
         },

         regresaGyG: function() {
            // alert("alerta");
            var x = document.getElementById("tareaNivel").value;
            var resource = this.$resource("{{route('CMS::admin.tareas.gradogrupo')}}")
            resource.get({x}).then(function(response){
            this.$set('originalgg', response.data)
            this.ggJson=this.originalgg 
            });
         },

         regresaultimoid:function(){
            var resource = this.$resource("{{route('CMS::admin.tareas.ultimoid')}}")
                resource.get({}).then(function(response) {
                //alert(JSON.stringify(response.data))
                if (response.data.id == null) {
                    this.ultimoid=0;
                }
                else{

                    this.$set('lastid', response.data)
                    this.ultimoid = this.lastid.id
                }
               
                
            });

                
         },

         agregarTarea: function(idEscolaridad,tipo,fechaEntrega,materia,profesor,contenido, recomendaciones,codigo) {        
            if (this.add.idmultipleEscolaridad == null)
                {
                     this.notificationFx("Selecciona un receptor", "error", "fa fa-close fa-3x")
                };

            if (this.ultimoid==0) 
                {
                     this.add.codigo = this.add.nivel.substring(0, 3)+ this.add.materia.substring(0,3)+1;
                }
            else{
                var suma = parseInt(this.ultimoid) + 1;
                this.add.codigo = this.add.nivel.substring(0, 3) + this.add.materia.substring(0,3) + suma;
                };

            for (var i = 0 ; i < this.add.idmultipleEscolaridad.length; i++) {

                var form = new FormData();

                form.append('idEscolaridad', this.add.idmultipleEscolaridad[i]);
                form.append('tipo', this.add.tipo);
                form.append('fechaEntrega', this.add.fechaEntrega);
                form.append('materia', this.add.materia);
                form.append('profesor', this.add.profesor);
                form.append('contenido', this.add.contenido);
                form.append('recomendaciones', this.add.recomendaciones);
                form.append('codigo', this.add.codigo);                  
                this.save(form,"{{route('CMS::admin.tareas.agregarTarea')}}", "Tarea agregada correctamente.")
            };
            
            // var form = new FormData();
            // form.append('idEscolaridad', this.add.idmultipleEscolaridad);
            // form.append('tipo', this.add.tipo);
            // form.append('fechaEntrega', this.add.fechaEntrega);
            // form.append('materia', this.add.materia);
            // form.append('profesor', this.add.profesor);
            // form.append('contenido', this.add.contenido);
            // form.append('recomendaciones', this.add.recomendaciones);
            
            // this.save(form,"{{route('CMS::admin.tareas.agregarTarea')}}")
            
            // alert(this.add.codigo);
            // swal({ title: "Tu codigo de tarea es:",   text: this.add.codigo,   type: "warning",   showCancelButton: false, confirmButtonColor: "#00c0ef", confirmButtonText: "Aceptar", closeOnConfirm: true })

        },

        eliminar:function(id){
            var Confirmbox= confirm("Deseas eliminar la tarea con id:" + id + "?")
            if(Confirmbox) /*var resource = this.$resource("{{route('CMS::admin.tareas.eliminar')}}")*/
            this.delete({id:id}, "{{route('CMS::admin.tareas.eliminar')}}", "La tarea se ha borrado")
            
        },

         notificationFx: function (message, tipo, icon) {
            var notification = new NotificationFx({
                message : '<span><i class="'+icon+'"></i></span><p>'+message+'</p>',
                layout : 'bar',
                effect : 'slidetop',
                type : tipo // notice, warning, error or success
            })
            notification.show()
        },

        limpiaForm: function() {
            this.$data.add.tipo = ""
            this.$data.add.fechaEntrega = ""
            this.$data.add.materia = ""
            this.$data.add.profesor = ""
            this.$data.add.contenido = ""
            this.$data.add.recomendaciones = ""
            this.$data.add.idmultipleEscolaridad = ""
            this.$data.add.idEscolaridad = ""
            this.$data.add.nivel=""
            this.originalgg=[]
            $("#gpschk").empty()
             this.bolsaErrores = null
                for (error in this.errores) {
                    this.errores[error] = false
                }
            // this.add.idmultipleEscolaridad = []
           
        },
        actualizarTarea:function(idEscolaridad,tipo,fechaEntrega,materia,profesor,contenido, recomendaciones){
            var form = new FormData();
            form.append('id',this.add.idtarea);
            form.append('idEscolaridad', this.add.idEscolaridad);
            form.append('tipo', this.add.tipo);
            form.append('fechaEntrega', this.add.fechaEntrega);
            form.append('materia', this.add.materia);
            form.append('profesor', this.add.profesor);
            form.append('contenido', this.add.contenido);
            form.append('recomendaciones', this.add.recomendaciones);
            
            this.update(form,"{{route('CMS::admin.tareas.actualizarTarea')}}","Tarea Actualizada correctamente")
            this.btnupdate = false
            this.btncancel = false
            this.btnadd    = true
            this.uno       = false
            this.dos       = true
            // $("#gg").removeAttr('disabled');

        },

        Editar: function (idRecord){
            var Confirmbox= confirm("Deseas editar la tarea con id: " + idRecord + "?")
            if(Confirmbox)
            var id = this.findIndexByKeyValue(this.originalJson,'id',idRecord)

            var resource = this.$resource("{{route('CMS::admin.escolaridades.valor')}}")
            resource.get({id:idRecord}).then(function (response) {
            this.$set('originalVal', response.data)
            this.valorJson = this.originalVal

            this.$data.add.idtarea = idRecord
            this.$data.add.tipo  = this.originalJson[id]['tipo']
            this.$data.add.fechaEntrega = this.originalJson[id]['fechaEntrega']
            this.$data.add.materia = this.originalJson[id]['materia']
            this.$data.add.profesor = this.originalJson[id]['profesor']
            this.$data.add.contenido = this.originalJson[id]['contenido']
            this.$data.add.recomendaciones = this.originalJson[id]['recomendaciones']
            this.$data.add.nivel = this.valorJson[0]['nivel_educativo']

            var x = this.valorJson[0]['nivel_educativo'];
            var resource = this.$resource("{{route('CMS::admin.tareas.gradogrupo')}}")
            resource.get({x}).then(function(response){
            this.$set('originalgg', response.data)
            this.ggJson=this.originalgg })
            // debugger;
            this.$data.add.idEscolaridad = this.valorJson[0]['id']

            });  
            
            this.btnupdate = true
            this.btncancel = true
            this.btnadd    = false
            this.dos = false
            this.uno = true
            // $("#gg").attr('disabled', 'disabled');
            
        },
        cancelar: function(){
            this.limpiaForm();
            this.btnupdate = false
            this.btncancel = false
            this.btnadd    = true
            this.dos = true
            this.uno = false
        
        },

        //Vue Http Resources
            save: function (data, route, msgSuccess) {
            var resource = this.$resource(route)
            resource.save(data).then(function (response) {
                this.success()
                this.limpiaForm()
                this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
                swal({ title: "Tu codigo de tarea es:",   text: this.add.codigo,   type: "warning",   showCancelButton: false, confirmButtonColor: "#00c0ef", confirmButtonText: "Aceptar", closeOnConfirm: true })
            }, function (response) {
                this.cleanInputErrors()
                this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
                this.inputErrors(response.data)
            })
        },

        update: function (data, route, msgSuccess) {
            var resource = this.$resource(route)
            resource.save(data).then(function (response) {
                this.success()
                this.limpiaForm()
                this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
            }, function (response) {
                this.cleanInputErrors()
                this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
                this.inputErrors(response.data)
            })
        },

        delete: function (data, route, msgSuccess) {
            var resource = this.$resource(route)
            resource.delete(data).then(function (response) {
                this.success()
                this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
            }, function (response) {
                // this.errors()
                this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
            })
        },

        inputErrors: function (campos) {
                this.bolsaErrores = campos
                for (error in this.errores) {
                    if(campos.hasOwnProperty(error)) {
                        this.errores[error] = true
                    }
                }
            },

        modalContenido: function(idRecord){
            var id = this.findIndexByKeyValue(this.originalJson,'id', idRecord);
            this.showModal = true;
            this.ver.contenido=this.originalJson[id]['contenido'];
        },

        findIndexByKeyValue: function (arraytosearch, key, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i][key] == valuetosearch) {
                        return i;
                    }
                }
                return null;
        },

        limpiarData: function (){
            this.add.tipo = '';
            this.add.fechaAlta = '';
            this.add.fechaEntrega = '',
            this.add.materia = '';
            this.add.profesor = '';
            this.add.contenido = '',
            this.add.recomendaciones = '';


        },
        cleanInputErrors: function () {
                this.bolsaErrores = null
                for (error in this.errores) {
                    this.errores[error] = false
                }
            },

           success: function (response) {
                this.loadingModal = false
                this.showError = false
                this.showModal = false
                this.showModalMessage = false
                this.showTareasAll()
                this.regresaultimoid()
            },

    },
         findIndexByKeyValue: function (arraytosearch, key, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i][key] == valuetosearch) {
                        return i;
                    }
                }
                return null;
            },

    filters: {

        // codigotarea: function(string){
        //     return string.substring(0,3) + ;
        // }
        //Json Pagination
        // paginate: function(list) {
        //     this.pag.resultCount = list.length
        //     if (this.pag.currentPage > this.totalPages) {
        //         this.pag.currentPage = this.totalPages - 1
        //     }
        //     var index = this.pag.currentPage * this.pag.itemsPerPage
        //     return list.slice(index, index + this.pag.itemsPerPage)
   
        // }
    }     
});



</script>