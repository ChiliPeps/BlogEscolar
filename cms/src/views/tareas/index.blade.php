@extends('CMS::master')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-file-word-o"></i> @lang('CMS::tareas.tareas')
        </h1>
    </section>
    <section class="content" id="app" v-cloak>
       {{--  <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"> </h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('CMS::admin.tareas.create') }}" class="btn bg-navy"><i class="fa fa-plus-circle"></i> @lang('CMS::core.create_new')</a>
                </div>                
                
            </div> --}}
            <br>
        {{-- MENSAJES DE ERROR EN FORMS --}}
        {{--     <div class="alert alert-danger" v-if="bolsaErrores">
                <h4><i class="icon fa fa-ban"></i> @lang('CMS::core.errors_title')</h4>
                <ul><li v-for="msg in bolsaErrores">@{{ msg }}</li></ul>
            </div> --}}

            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Tarea</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                  <!-- /.box-tools -->
                </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
                 <div class = "col-md-12">
                    <div class ="col-md-2">
                        <select v-model = "add.tipo" class ="form-control" v-bind:style = "[errores.tipo ?  inputErrorStyle:'']">
                            <option value ="" disabled selected> Tipo tarea</option>
                            <option value = "Individual"> Individual </option>
                            <option value = "Equipo"> Equipo </option>
                        </select>
                    </div>

                    <div class ="col-md-2">
                        <input v-model="add.materia" type="text" class="form-control" placeholder="Materia" v-bind:style = "[errores.materia ?  inputErrorStyle:'']">
                    </div>

                    <div class ="col-md-2">
                        <input v-model="add.profesor" type="text" class="form-control" placeholder="Profesor" v-bind:style = "[errores.profesor ?  inputErrorStyle:'']">                        
                        {{-- {!! Field::text('', null, ['placeholder'=> 'Profesor', 'v-model' => 'add.profesor', 'v-bind:style' => '[errores.profesor ?  inputErrorStyle : ""]']) !!} --}}

                    </div>

                    <div class ="col-md-3">
                         @if(Auth::user()->type == "suadmin" || Auth::user()->type == "multinivel" || Auth::user()->type == "admin")                        
                            <select class ="form-control" v-model="add.nivel" id="tareaNivel" v-on:change = "regresaGyG()">
                            <option value="" disabled selected> Seleccione un Nivel </option>
                            <option  v-for="nivel in nivelJson">
                              @{{ nivel.nivel_educativo}} 
                            </option>
                            </select>
                        
                        @else
                            <select class ="form-control" v-model="add.nivel" id="tareaNivel" v-on:change = "regresaGyG()">
                            <option value="" disabled selected> Seleccione un Nivel </option>
                            <option>
                              {{ Auth::user()->type }} 
                            </option>
                            </select>
                        @endif
                        
                    </div>

                {{-- Tarea para 1 solo grupo --}}
                    <div class ="col-md-3" v-show = "uno">
                        <select v-model="add.idEscolaridad" class ="form-control"  >
                            <option value="" disabled >Seleccione un Grupo</option>
                            <option value = "@{{gg.id}}" v-for="gg in ggJson"> @{{gg.nivel_educativo}}-@{{gg.grado}}-@{{gg.grupo}}</option>
                          
                        </select>
                        <br>
                    </div> 
                {{-- Tarea para 1 solo grupo --}}

                    {{-- tarea para multigrupo --}}
                     <div class ="col-md-3" v-show="dos">
                        <select v-select="add.idmultipleEscolaridad" id="gg" class = "form-control" id="gpschk" data-placeholder="Seleccione el grupo" multiple>
                            
                            <option value = "@{{gg.id}}" v-for="gg in ggJson"> @{{gg.grado}}-@{{gg.grupo}}</option>
                          
                        </select>
                        <br> <br>
                    </div>
                    {{-- tarea para multigrupo --}}
                    
                    <div class="col-md-3">
                         <div class="input-group date">
                              <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                              </div>
                              <input v-model="add.fechaEntrega" type="text" id="datepicker" class="form-control pull-right"  placeholder="Fecha de entrega" v-bind:style = "[errores.fechaEntrega ?  inputErrorStyle:'']">

                        </div>
                    </div> 
                    <div class="col-md-9">
                         <input v-model="add.recomendaciones" type="text" class="form-control" placeholder="Recomendaciones">
                    </div> 
                 </div>  

                <div id="el"> <div  class="col-md-12">
                    <br>
                     <textarea name="txtTarea" id="txtContenido" rows="10" cols="150" v-editor="add.contenido" >
                              
                    </textarea>
                    <br>
                 </div> </div>

                 <div class = "col-md-12">
                    <button v-show = "btnadd" @click="agregarTarea" type="button" class="btn btn-info text-center"> Agregar </button>
                    <button v-show = "btnupdate" @click="actualizarTarea" type="button" class="btn btn-info text-center"> Actualizar </button>
                    <button v-show = "btncancel" @click="cancelar" type="button" class="btn btn-danger text-center"> Cancelar </button>
                 </div>
            </div>
            <!-- /.box-body -->
          </div>

         <div class="box box-primary">
              <div class="box-body table-responsive no-padding" style="display:block;">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr class="info">
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Tipo</th>
                                    <th>FechaAlta</th>
                                    <th>Fecha de entrega</th>
                                    <th>Materia</th>
                                    <th>Profesor</th>
                                    <th>Contenido</th>                                    
                                    <th>Escolaridad</th>
                                    <th>Recomendaciones</th>
                                    
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="t in tareasJson | paginate">

                                    <td>@{{t.id}}</td>
                                    <td>@{{t.name}}</td>
                                    <td>@{{t.tipo}}</td>
                                    <td>@{{t.fechaAlta}}</td>
                                    <td>@{{t.fechaEntrega}}</td>
                                    <td>@{{t.materia}}</td>
                                    <td>@{{t.profesor}}</td>
                                    <td> <button @click="modalContenido(t.id)" type="button" class="btn btn-info text-center" > Ver </button> </td>                                  
                                    <td>@{{t.nivel_educativo}}
                                        @{{t.grado}}
                                        @{{t.grupo}}
                                    </td>
                                    <td>@{{t.recomendaciones}}</td>
                                   
                         
                                    <td>
                                        <button type="button" class="btn btn-danger" @click="eliminar(t.id)" >X</button> 
                                    
                                    <a href="#app"><button type="button"  class="btn btn-warning" @click="Editar(t.id)" > <i class="fa fa-repeat"></i> </button></a> 
                                    </td>
                                </tr>

                                    </td>
                                                                
                                </tr>
                       
                                
                            </tbody>
                         </table> 

                         <!-- Vue Pagination -->
                         {{-- <pre> @{{valorJson | json}} </pre> --}}
                         {{-- <pre> @{{tareasJson | json}} </pre> --}}
                        {{-- valor: @{{add.idEscolaridad}} --}}
                        <br>
                        {{-- valor multiple: @{{add.idmultipleEscolaridad}} --}}

            </div>    
             <pagination :data="pag" :total-pages="totalPages" :start-point="startPoint" :set-page="setPage"></pagination>        
        </div>

    <modal :show.sync="showModal">
        
        <div slot = "head"> Contenido de la tarea</div>
            <div slot="body">
              @{{{ver.contenido}}}
            </div>
        
    </modal>

    </section>

    @section('scripts')
        @include('CMS::tareas.partials.scripts')
    @stop
@endsection