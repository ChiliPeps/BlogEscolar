@extends('CMS::master')

@section('content')
 <style>
    img.img-rounded {
        width: 75px;
        height: 50px;
    }

    .flex-test{
        display: flex;
        justify-content: flex-start;
        align-items: center;
    }

    td{
        vertical-align: middle !important;
    }

    .firstCol{
        min-width: 180px;
    }

    .btn-categoria{
        display: flex;
        justify-content: center;
        align-content: center;
        margin-top: 25px;
    }

    .flexislots {
        display: flex;
        flex-wrap: wrap;
        align-items: flex-start;
    }

</style>
   
    <section class="content-header">
        <h1>
            <i class="fa fa-file"></i> Noticias
        </h1>
    </section>
    <section id="app" class="content" v-cloak>

        <div v-show= "panelIndex"class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <button @click="loadPanel" class="btn bg-navy"><i class="fa fa-plus-circle"></i> Crear Nueva Noticia</button>
                </div>
            </div>

            <div class="box-body">

                <div class="row">
                    <div class="col-md-5 col-md-offset-2">
                        {!! Field::text('busqueda', null, ['id' => 'busqueda', 'label' => 'Busqueda', 'v-model' => 'busqueda']) !!}
                    </div>
                    <div class="col-md-3">
                        {!! Field::select('tipo', $tipos, 'titulo', ['id' => 'tipo', 'label' => 'Por', 'v-model' => 'tipo']) !!}
                    </div>
                </div>

                <!-- VueLoading icon -->
                {{-- <div class="text-center"><i v-show="loading" class="fa fa-spinner fa-spin fa-5x"></i></div> --}}

                <!-- VueTable -->
                <table class="features-table table table-bordered table-hover " >
                    <thead>
                    <tr>
                        <th>Noticia</th>
                        <th></th>
                        <th>Categoria</th>
                        <th></th>
                        <th>creado</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr  v-for="s in showJson | filterBy busqueda in tipo | paginate">
                        <td class="firstCol">
                            <div class="flex-test">
                                <div style="" class="text-center">
                                    <div> <img :src="public_url+s.thumb" class="img-rounded"/></div>

                                </div>
                                <div style="padding-left: 15px; ">
                                    <div style="color: #3c8dbc; font-weight: bold;" >@{{ s.titulo }}</div>
                                    <div>@{{s.autor}}</div>
                                </div>
                            </div>

                        </td>
                        <td>
                            {{-- <button v-if="s.categoria.title =='investigaciones'|| s.categoria.title =='reportajes'" @click="openMFU(s)" class="btn btn-success btn-xs" style="width: 100px; margin-bottom: 10px;"> Galería <i class="fa fa-file-image-o"></i> </button> --}}
                        </td>
                        <td>@{{ s.categoria }}</td>
                        <td>@{{  }}</td>
                        <td>@{{ s.created_at }}</td>
                        <td>
                            <div>
                                <button @click="loadPanelUpdate(s.id)" class="btn btn-warning btn-xs" style="width: 100px; margin-bottom: 10px;"> Editar <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                            <button @click="borrar(s.id)" class="btn btn-danger btn-xs" style="width: 100px"> Eliminar <i class="fa fa-trash"></i> </button>
                        </td>
                    </tr>

                    </tbody>
                </table>

                <!-- Vue Pagination -->
                {{-- <pagination :data="pag" :total-pages="totalPages" :start-point="startPoint" :set-page="setPage"></pagination> --}}

            </div>


        </div>

        <div v-show="panelNuevo">
            @include('CMS::noticias.partials.inputs')
        </div>

        {{-- vue MODAL --}}
        @include('CMS::noticias.partials.modal')
  

    </section>


@endsection

@section('scripts')
    @include('CMS::noticias.partials.scripts')
@stop