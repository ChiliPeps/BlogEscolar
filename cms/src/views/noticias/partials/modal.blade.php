@include('CMS::partials.vueModal')

<!-- Tag Name -->
<modal :show.sync="showModal">
    <div slot="head">Agregar Nuevo Tag</div>
    <div slot="body">
        {!! Field::text('nombre', null, ['v-model' => 'dataModal.nombre', 'id' => 'PN']) !!}

        <!-- Save Button -->
        <button @click="saveModal" class="btn bg-navy"><i class="fa fa-floppy-o"></i> Guardar
        <i v-show="loadingModal" class="fa fa-spinner fa-spin"></i></button>
    </div>
</modal>