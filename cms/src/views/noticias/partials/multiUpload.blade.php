<script>

multiUpload = {

    data: {
        panelMFU: false,
        noticiaMFU: { id:'', titulo: '' },
        loadingMFU: false,
        loadingUploadMFU: false,
        galeriaJson: [],

        //Image Render Data Array
        renderImages: [],
        imageData: [],
        fileData: [],

    },

    methods: {
        openMFU: function(noticia) {
            this.panelIndex = false
            this.panelMFU = true
            this.noticiaMFU.id     = noticia.id
            this.noticiaMFU.titulo = noticia.titulo
            this.loadingMFU = true

            var resource = this.$resource("{{route('CMS::admin.noticias.galeria.get')}}");
            resource.get({id_noticia: noticia.id}).then(function (response) {
                this.$set('galeriaJson', response.data);

                //Callbacks
                for (var i = 0; i < this.galeriaJson.length; i++) {
                    //Fetch & Blob
                    this.ajaxFetch(this.public_url+this.galeriaJson[i].imagen, i)
                }

                this.loadingMFU = false
            });
        },

        closeMFU: function() {
            this.galeriaJson = []
            this.renderImages = []
            this.imageData = []
            this.fileData = []
            this.noticiaMFU = { id:'', titulo: '' }
            this.panelMFU = false
            this.panelIndex = true
        },

        updateMFU: function() {
            this.loadingUploadMFU = true
            var form = new FormData();
            if(this.fileData.length > 0) {
                for (var i = 0; i < this.fileData.length; i++) {
                    form.append('imagenesMFU[]', this.fileData[i], this.imageData[i].name);
                }
            }
            form.append('id_noticia', this.noticiaMFU.id);
            var resource = this.$resource("{{route('CMS::admin.noticias.galeria.set')}}")
            resource.save(form).then(function (response) {
                this.notificationFx("Galería Actualizada!", "success", "fa fa-check-circle-o fa-3x")
                this.loadingUploadMFU = false
            }, function (response) {
                this.notificationFx("Se ha producido un error.", "error", "fa fa-exclamation-triangle fa-3x")
                this.loadingUploadMFU = false
            });
        },

        //Image Load & Render
        onFileChangeMFU: function(e) {                 
            var files = e.target.files || e.dataTransfer.files

            if (!files.length)
                return;

            if(!this.isValidImageMFU(files)) {
                this.removeImageMFU()
                return;
            }

            this.createImageMFU(files)
        },
        createImageMFU: function(files) {

            for (var i = 0; i < files.length && i < 8; i++) {
                var image = new Image()
                var reader = new FileReader()
                reader.onload = (e) => { 
                    this.renderImages.push(e.target.result); 
                }
                reader.readAsDataURL(files[i])

                //File Normal Data
                this.imageData.push({ name: files[i].name, size: files[i].size+' Bytes' })

                //File Important Data
                this.fileData.push(files[i])
            }
        },
        isValidImageMFU: function (files) {
            var _validFileExtensions = ["jpg", "jpeg", "png"];
            for (var i = 0; i < files.length; i++) {

                //Size
                if (files[i].size > 2097152) { return false } //2 MB

                //Type
                type = files[i].name.split('.')[files[i].name.split('.').length - 1].toLowerCase()
                if (_validFileExtensions.indexOf(type) == -1) { return false }

            } return true
        },
        removeImageMFU: function (index) {
            this.renderImages.splice(index, 1)
            this.imageData.splice(index, 1)
            this.fileData.splice(index, 1)
        },

        ajaxFetch: function(url, order) {
            fetch(url).then((res) => { //Check for Safari fetch()
                return res.blob();
            }).then((blob) => {

                //Add blob
                this.fileData.push(blob);

                 //File name
                var f = getFilename(url);
                var name = f.filename+'.'+f.ext;

                //File Normal Data
                this.imageData.push({ name: name, size: 'En el servidor' })

                readBlobAsDataUrl(blob, (dataUrl) => {
                    //Add Rendered Image
                    this.renderImages.push(dataUrl)
                    
                })
            })
		}
    }
}

var readBlobAsDataUrl = function(blob, cb) {
	var a = new FileReader();
	a.onload = function(e) {
		cb(e.target.result);
	};
	a.readAsDataURL(blob);
}

function getFilename(url) {
  // returns an object with {filename, ext} from url (from: http://coursesweb.net/ )

  // get the part after last /, then replace any query and hash part
  url = url.split('/').pop().replace(/\#(.*?)$/, '').replace(/\?(.*?)$/, '');
  url = url.split('.');  // separates filename and extension
  return {filename: (url[0] || ''), ext: (url[1] || '')}
}

</script>