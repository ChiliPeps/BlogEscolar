<style>
    .imgUpload img {
        /*width: 30%;
        height: 50%;*/
        margin: auto;
        display: block;
        margin-bottom: 10px;
    }
    .fileUpload {
        position: relative;
        overflow: hidden;
        margin: 10px;
    }
    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
</style>

<script>

//Image Render Methods
var imagen = {

    data: {
        //Image Render Data
        renderImage: ''
    },

    methods: {
        onFileChange: function(e) {                 
            var files = e.target.files || e.dataTransfer.files

            if (!files.length)
                return;

            if(!this.isValidImage(files)) {
                this.removeImage()
                return;
            }

            this.noticia.imagen = files
            this.createImage(files[0])
        },
        createImage: function(file) {
            var image = new Image()
            var reader = new FileReader()
            var vm = this
            reader.onload = (e) => {
                vm.renderImage = e.target.result
            }
            reader.readAsDataURL(file)
        },
        isValidImage: function (files) {

            var _validFileExtensions = ["jpg", "jpeg", "png"];
            for (var i = 0; i < files.length; i++) {

                //Size
                if (files[i].size > 2097152) { return false } //2 MB

                //Type
                type = files[i].name.split('.')[files[i].name.split('.').length - 1].toLowerCase()
                if (_validFileExtensions.indexOf(type) == -1) { return false }

            } return true
        },
        removeImage: function (e) {
            document.getElementById("file").value = "";
            this.renderImage = ''
            this.noticia.imagen = ''
        }
    }
}

</script>