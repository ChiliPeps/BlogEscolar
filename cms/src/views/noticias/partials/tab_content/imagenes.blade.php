<div class="row imgUpload">
    <div class="col-md-12">

        <div class="box box-primary box-solid">
            <div class="box-header"><h2 class="box-title">Imagen de Noticia (1000 x 670)</h2></div>
            <div class="box-body">
                
              {{--   {!! Field::text('autor_foto', null, ['label' => 'Autor de la Foto', 'v-model' => 'noticia.autor_foto', 'v-bind:style' => '[errores.autor_foto ?  inputErrorStyle : ""]']) !!} --}}

                <!-- Image Upload -->
                <div class="text-center" v-show="!renderImage">
                    {!! Html::image("images/noticias/01.png", "", ['class' => 'img-responsive']) !!}
                    <div class="fileUpload btn btn-primary btn-lg">
                        <span>Subir imagen</span>
                        <input id="file" name="file" type="file" class="upload" @change="onFileChange">
                    </div>
                </div>

                <div class="text-center" v-show="renderImage">
                    <img :src="renderImage" class="img-responsive"/>
                    <button class="btn btn-primary btn-lg" @click="removeImage">Remover imagen</button>
                </div>
                <!-- Image Upload -->

            </div>
        </div>
    
    </div>

</div>