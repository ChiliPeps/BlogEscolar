<div class="row">
    <div class="col-md-12">

        <div class="box box-primary">
            <div class="box-body">
                {!! Field::textarea('Descripcion', null, ['label' => 'Descripcion', 'class' => 'editor2', 'id' => 'editor2', 'rows' => '4', 'v-model' => 'noticia.descripcion', 'v-bind:style' => '[errores.descripcion ?  inputErrorStyle : ""]']) !!}
            </div>
        </div>
        
    </div>
    <div class="col-md-12">

        <div class="box box-danger">
            <div class="box-body">
                {!! Field::textarea('Contenido', null, ['label' => 'Contenido', 'class' => 'editor1', 'id' => 'txtContenido', 'rows' => '30', 'cols' => '60', 'v-editor' => 'noticia.contenido']) !!}
            </div>
        </div>

    </div>
</div>