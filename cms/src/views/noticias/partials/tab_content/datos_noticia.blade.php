<div class="row">

    <div class="col-md-6">

        <div class="box box-primary">
            <div class="box-body">
              
                {!! Field::text('titulo', null, ['v-model' => 'noticia.titulo', 'v-bind:style' => '[errores.titulo ?  inputErrorStyle : ""]']) !!}

                {{-- Categoria --}}
                <div class="form-group">
                    <label>Categoria</label>
                    <select  class="form-control"  v-model="noticia.categoria">
                        {{-- <option v-for="s in categoriasJson" value="@{{s.id}}">@{{s.title}}</option> --}}
                        <option>Kinder</option>
                        <option> Primaria </option>
                        <option> Secundaria </option>
                        <option>Bachillerato</option>
                        <option>Deportes</option>
                    </select>
                </div>

            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="box box-danger">
            <div class="box-body">

                {!! Field::text('slugurl', null, ['v-model' => 'slugurl', 'readonly']) !!}

                {{-- Tags --}}
                {{-- Select2 Input --}}
                <div class="form-group">
                    <label for="tags">Tags</label> <button @click="openModal" class="btn btn-xs bg-green"><i class="fa fa-plus-circle"></i> Agregar</button>
                    <i v-show="loadingSelect2" class="fa fa-spinner fa-spin"></i>
                    <select id="tags" style="width: 100%;" v-select2="noticia.tags" multiple>
                        <option v-for="t in tagsJson" value="@{{t.id}}">@{{t.nombre}}</option>
                    </select>
                </div>                
                {{-- Select2 Input --}}

            </div>
        </div>

    </div>

</div>