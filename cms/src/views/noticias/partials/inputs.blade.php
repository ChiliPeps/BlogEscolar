<div class="container-fluid">

    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title"></h3>
            <div class="box-tools pull-right">

                <!-- Close Button -->
                <button @click="closePanel" class="btn"><i class="fa fa-chevron-circle-left"></i> @lang('Regresar')</button>
                <!-- Save Button -->
                <button v-if="saveButton" @click="salvar" class="btn bg-navy"><i class="fa fa-floppy-o"></i> Guardar
                <i v-show="loadingNoticia" class="fa fa-spinner fa-spin"></i></button>
                <!-- Update Button -->
                <button v-if="updateButton" @click="actualizar" class="btn bg-navy"><i class="fa fa-floppy-o"></i> Actualizar
                <i v-show="loadingNoticia" class="fa fa-spinner fa-spin"></i></button>
                

            </div>
        </div> <br>
    </div>

    <div class="alert alert-danger" v-if="bolsaErrores">
        <h4><i class="icon fa fa-ban"></i> @lang('CMS::core.errors_title')</h4>
        <ul><li v-for="msg in bolsaErrores">@{{ msg }}</li></ul>
    </div>


    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true" id="tab1">Datos de la Noticia</a></li>
            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Contenido</a></li>
            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Imagenes</a></li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">

            <div class="tab-pane active" id="tab_1">
                @include('CMS::noticias.partials.tab_content.datos_noticia')
            </div>

            <div class="tab-pane" id="tab_2">
                @include('CMS::noticias.partials.tab_content.contenido')
            </div>

            <div class="tab-pane" id="tab_3">
                <h1>Tamaño Maximo del Archivo: 2 Mb</h1>
                @include('CMS::noticias.partials.tab_content.imagenes')
            </div>

        </div>

    </div>


</div>

