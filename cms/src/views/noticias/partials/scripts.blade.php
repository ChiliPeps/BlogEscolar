{{-- @include('CMS::partials.vueModal') --}}
@include('CMS::partials.vuePagination')
@include('CMS::partials.vueSelect2_multi_tags')
@include('CMS::noticias.partials.imagen')
@include('CMS::partials.vueHelperFunctions')
@include('CMS::noticias.partials.modalScript')
@include('CMS::noticias.partials.multiUpload')

<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

     Vue.component('modal', {
        template: '#modal-template',
        props: {
            show: {
                type: Boolean,
                required: true,
                twoWay: true
            }
        }
    }),
     
     //nueva directiva editor (v-editor) para mandar ckeditor a aviso.contenido
    Vue.directive('editor', {
        twoWay: true,

    bind: function () {
        this.vm.$nextTick(this.setupEditor.bind(this));
    },

    setupEditor: function () {
       
        CKEDITOR.env.isCompatible = true;
        var self = this;
        var finder = CKEDITOR.replace(this.el.id);
        CKFinder.setupCKEditor(finder, '../ckfinder/');
        CKEDITOR.instances[this.el.id].on('change', function () {
        self.set(CKEDITOR.instances[self.el.id].getData());

        });
    },

    update: function (value) {
        // value = value || ' ';
        if (!CKEDITOR.instances[this.el.id]) {
            return this.vm.$nextTick(this.update.bind(this, value));
        }
        CKEDITOR.instances[this.el.id].setData('',value,value);
    },

    unbind: function () {
        CKEDITOR.instances[this.el.id].destroy();
    }
  
  }); // fin directiva editor

new Vue({

    ready: function () { this.getAllData(), this.getTags() },


    el: '#app',

    data: {
        panelIndex:true,
        panelNuevo:false,
        saveButton:false,
        updateButton:false,
        loadingSelect2:false,
        tipo: '',
        public_url:"{{ URL::to('/') }}/",

        //Json Data
            originalJson: null, //Json Original
            showJson: [],     //Json que se muestra
            categoriasJson: [],
            tagsJson: [],
            tags_backup: [],

        noticia: {
                id: '',                 titulo: '',            
                descripcion: '',        contenido: '',          
                categoria: '',       
                tags: [],               imagen: ''
            },
        //Formulario errores
        errores: {
                id: false,              titulo: false,          descripcion: false,
                contenido: false,       imagen: false,          categoria: false
            },

            bolsaErrores: null,

            inputErrorStyle: {
                outline: 'none',
                'border-color':       'red',
                '-webkit-box-shadow': '0px 0px 17px 0px rgba(255, 0, 0, 1)',
                '-moz-box-shadow':    '0px 0px 17px 0px rgba(255, 0, 0, 1)',
                'box-shadow':         '0px 0px 17px 0px rgba(255, 0, 0, 1)'
            }
},
        //Mixins
        mixins: [pagination, helperFunctions, select2_multi_tags, imagen, modalScript, multiUpload],

computed: {
        //Json Pagination
        // totalPages: function() {
        //     return Math.ceil(this.pag.resultCount / this.pag.itemsPerPage)
        // },
         slugurl: function() {
                return this.slugify(this.noticia.titulo);
            }
    },

    methods: {
            
            getAllData: function () {
                this.loading = true;
                var resource = this.$resource("{{route('CMS::admin.noticias.data')}}");
                resource.get({}).then(function (response) {
                    this.$set('originalJson', response.data);
                    this.showJson = this.originalJson;
                    this.loading = false;
                });
            },

           loadPanel: function () {
                this.panelIndex = false;
                this.panelNuevo = true;
                this.saveButton = true;
            },

            closePanel:function(){
                this.panelIndex=true;
                this.panelNuevo=false;
                this.saveButton=false;
                this.cleanInputErrors()
                $('#tab1').tab('show')
            },

            loadPanelUpdate: function(idRecord) {
                //Load data to panel
                var index = this.findIndexByKeyValue(this.originalJson, 'id', idRecord)
                this.noticia.id           = this.originalJson[index]['id']
                this.noticia.titulo       = this.originalJson[index]['titulo']
                // this.noticia.fecha        = this.originalJson[index]['fecha']
                this.noticia.descripcion  = this.originalJson[index]['descripcion']
                this.noticia.contenido    = this.originalJson[index]['contenido']
                // this.noticia.autor        = this.originalJson[index]['autor']
                this.noticia.categoria    = this.originalJson[index]['categoria']
                this.noticia.prioridad    = this.originalJson[index]['prioridad']
                this.noticia.autor_foto   = this.originalJson[index]['autor_foto']

                //Tags
                this.noticia.tags = []
                if(this.originalJson[index]['tags'] != null) {
                    for(var i = 0; i < this.originalJson[index]['tags'].length; i++) {
                        this.noticia.tags.push(this.originalJson[index].tags[i].tag.id)
                    }
                }

                //Imagenes
                this.noticia.imagen  = this.originalJson[index].imagen
                this.renderImage     = this.public_url+this.originalJson[index].imagen +"?"+Date.now(); //Update cache

                this.panelIndex = false
                this.panelNuevo = true
                this.updateButton = true
            },     

            salvar: function ()  {
                if(this.saveInAction == true) { return; }
                this.loadingNoticia = true
                this.saveInAction = true

                this.save(this.generateFormData("save"), "{{route('CMS::admin.noticias.save')}}", "Noticia agregada correctamente.")
            },

            actualizar:function(){
                if(this.saveInAction == true) { return; }
                this.loadingNoticia = true
                this.saveInAction = true

               this.save(this.generateFormData("update"), "{{route('CMS::admin.noticias.update')}}", "Noticia actualizada correctamente.")
            },

            borrar: function (id) {
                swal({ title: "¿Estas seguro?",   text: "Esta a punto de borrar la noticia!",   type: "warning",   showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Si, borralo!", closeOnConfirm: true }, function() {
                    this.delete({ id: id }, "{{route('CMS::admin.noticias.delete')}}", "Noticia borrada correctamente.")
                }.bind(this))
            },

            getTags: function() {
                this.loadingSelect2 = true;
                var resource = this.$resource("{{route('CMS::admin.noticias.tags')}}");
                resource.get({}).then(function (response) {
                    this.$set('tagsJson', response.data);
                    this.loadingSelect2 = false;

                    //Tag backup Fix when data reloaded
                    if(this.tags_backup.length > 0) {
                        this.noticia.tags = this.tags_backup

                        //Tags
                        this.noticia.tags = []
                        for(var i = 0; i < this.tags_backup.length; i++) {
                            this.noticia.tags.push(this.tags_backup[i])
                        }

                    }
                });
            },

            generateFormData: function (action) {
                var form = new FormData();

                if(this.noticia.imagen[0] != null) {
                    form.append('imagen', this.noticia.imagen[0]);
                }

                if(action == "update") {
                    form.append('id', this.noticia.id);
                }
            
                form.append('titulo', this.noticia.titulo);
                form.append('slugurl', this.slugurl);
                form.append('fecha', this.noticia.fecha);
                form.append('descripcion', this.noticia.descripcion);
                form.append('contenido', this.noticia.contenido);
                // form.append('autor', this.noticia.autor);
                form.append('categoria', this.noticia.categoria);
                // form.append('prioridad', this.noticia.prioridad);
                // form.append('autor_foto', this.noticia.autor_foto);
                
                if(this.noticia.tags != null) {
                    for(var i = 0; i < this.noticia.tags.length; i++) {
                        form.append('tags[]', this.noticia.tags[i]);
                    }
                }

                return form;
            },
            //Vue Http Resources
            save: function (data, route, msgSuccess) {
                var resource = this.$resource(route)
                resource.save(data).then(function (response) {
                    this.cleanAll()
                    this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
                    this.closePanel()
                    this.getAllData()
                }, function (response) {
                    this.cleanAll()
                    this.notificationFx("Se ha producido un error.", "error", "fa fa-exclamation-triangle fa-3x")
                    this.inputErrors(response.data)
                })
            },
            delete: function (data, route, msgSuccess) {
                var resource = this.$resource(route)
                resource.delete(data).then(function (response) {
                    this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
                    this.getAllData()
                }, function (response) {
                    this.notificationFx("Se ha producido un error.", "error", "fa fa-exclamation-triangle fa-3x")
                })
            },

            cleanAll: function () {
                this.loadingNoticia = false
                this.saveInAction = false
            },

            inputErrors: function (campos) {
                this.bolsaErrores = campos
                for (error in this.errores) {
                    if(campos.hasOwnProperty(error)) {
                        this.errores[error] = true
                    }
                }
            },
            cleanInputErrors: function () {
                this.bolsaErrores = null
                for (error in this.errores) {
                    this.errores[error] = false
                }

                this.noticia.id = ''; this.noticia.titulo = ''; this.noticia.fecha = ''; 
                this.noticia.descripcion = ''; this.noticia.contenido = '';
                this.noticia.autor = ''; this.noticia.tags = [];
                this.noticia.imagen = ''; this.noticia.categoria='';

                this.renderImage = '';
                this.tags_backup = []

                this.saveButton = false
                this.updateButton = false
            },

             //Slug Url
            slugify: function(value) {
                value = value.replace(/^\s+|\s+$/g, ''); // trim
                value = value.toLowerCase();

                // remove accents, swap ñ for n, etc
                var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
                var to   = "aaaaaeeeeeiiiiooooouuuunc------";
                for (var i=0, l=from.length ; i<l ; i++) {
                    value = value.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
                }

                value = value.replace(/[^a-z0-9 -]/g, '').replace(/\s+/g, '-').replace(/-+/g, '-'); // collapse dashes

                return value;
            }

    }
});

</script>