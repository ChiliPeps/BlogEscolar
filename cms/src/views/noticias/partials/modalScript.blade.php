<script>
var modalScript = {
    data: {
        loadingModal: false,
        showModal: false,
        dataModal: { nombre: '' },
    },
    methods: {
        
        //Modal Functions
        openModal: function () {
            this.showModal = true
            // window.setTimeout(function () {
            //     document.getElementById("PN").focus();
            // }, 0);
        },
        saveModal: function () {
            if(this.saveInAction == true) { return; }
            this.loadingModal= true
            this.saveInAction = true

            var form = new FormData();
            form.append('nombre', this.dataModal.nombre);

            var resource = this.$resource("{{route('CMS::admin.noticias.save_tag')}}")
            resource.save(form).then(function (response) {
                this.notificationFx("Nombre agregado correctamente.", "success", "fa fa-check-circle-o fa-3x")
                this.showModal = false
                this.loadingModal = false

                //Fix para Tags al añadir
                this.tags_backup = this.noticia.tags
                if(this.tags_backup == null) { this.tags_backup = [] }
                this.tags_backup.push(String(response.data.id))

                //Reload Tags
                this.getTags()

                this.dataModal.nombre = ''
                this.saveInAction = false
            }, function (response) {
                this.notificationFx("Se ha producido un error. "+" "+response.data.nombre, "error", "fa fa-exclamation-triangle fa-3x")
                this.showModal = false
                this.loadingModal = false
                this.dataModal.nombre = ''
                this.saveInAction = false
            })
        }
    }
}
</script>
