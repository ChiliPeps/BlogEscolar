@extends('CMS::master')

@section('content')
    <section class="content-header">
        <h1>
            <i class="fa fa-file"></i> @lang('CMS::escolaridades.escolaridades')
        </h1>
    </section>
    
    <section id="app" class="content" >
        <div class="box-body">
            

            <div class="col-md-12">
                    <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Crear Escolaridades</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                    </div>
                <!-- /.box-header -->
                <div class="box-body">
                  
                    <div>

                        <div class="content">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <label>Nivel Educativo: </label>
                                </div>
                                <div class="col-md-4 text-center">
                                    <label>Grado: </label>
                                </div>
                                <div class="col-md-4 text-center">
                                    <label>Grupo: </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <input v-model="guardar.nivel_educativo | onlyTextDisplay | capitalize" class="form-control" id="nivel">
                                </div>
                                <div class="col-md-4 text-center">
                                    <input v-model="guardar.grado  | onlyNumberDisplay" class="form-control">
                                </div>
                                <div class="col-md-4 text-center">
                                    <input v-model="guardar.grupo | onlyTextDisplay | uppercase" class="form-control">
                                </div>
                            </div>
                            <br>
                            {{-- <div class="text-center"><i v-show="loadingModal" class="fa fa-spinner fa-spin fa-5x"></i></div> --}}
                            {{-- <div class="text-center"><i style="color: red;" v-show="showError" class="fa fa-times-circle fa-5x"></i></div> --}}

                            <div class="row">
                                <div class="col-md-12" style="margin-left: auto; margin-right: auto;">
                                    <div class="box-tools text-center">
                                        <button @click="guardarEsc" class="btn btn-success btn-xs"> Crear <i class="fa fa-arrow-circle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- /.box-body -->
                </div>
              <!-- /.box -->
            </div>



       {{--      <modal :show.sync="showModal">
                <div slot="head">Editar Escolaridad</div>
                <div slot="body"> hola </div>
            </modal>

            <button  @click= "cargar()" class="btn bg-orange btn-xs"> Editar <i class="fa fa-arrow-circle-right"></i></button> --}}

        </div>

       <div class="box box-primary">
             <div class="box-body table-responsive no-padding" style="display:block;">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Nivel Educativo</th>
                        <th>Grado</th>
                        <th>Grupo</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="s in showJson | paginate">
                        <td>@{{ s.nivel_educativo }}</td>
                        <td>@{{ s.grado }}</td>
                        <td>@{{ s.grupo }}</td>
                        <td>
                            <button  @click= "loadModal(s.id)" class="btn bg-orange btn-xs"> Editar <i class="fa fa-arrow-circle-right"></i></button>
                            <button @click="borrar(s.id)" class="btn btn-danger btn-xs"> Eliminar <i class=""></i></button>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <!-- Vue Pagination -->
            <pagination :data="pag" :total-pages="totalPages" :start-point="startPoint" :set-page="setPage"></pagination>
        </div>
        
        {{-- <pre> @{{ showJson | json }} </pre> --}}
         
 <!-- Vue Modals -->
        @include('CMS::escolaridades.partials.modals')
    </section>


@section('scripts')
    @include('CMS::escolaridades.partials.scripts')
@stop
@endsection