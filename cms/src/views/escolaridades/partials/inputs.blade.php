@include('CMS::partials._errors')
{!! Alert::render() !!}
<div class="box box-solid box-primary">
    <div class="box-header"><h2 class="box-title">Datos de Escolaridad</h2></div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                {!! Field::text('nivel_educativo', null, ['label' => 'Nivel Educativo', 'placeholder' => 'Primaria']) !!}
                @if(isset($escolaridad))
                    <div class="form-group">
                        {!! Field::makeDeleteButton($escolaridad) !!}
                    </div>
                @endif
            </div>
            <div class="col-md-4">
                {!! Field::select('grado', array_combine(range(1, 99), range(1, 99)), null, ['label' => 'Grado']) !!}
            </div>
            <div class="col-md-4">
                {!! Field::select('grupo', array_combine(range('A', 'Z'), range('A', 'Z')), null, ['label' => 'Grupo']) !!}
            </div>
        </div>
    </div>
</div>