<!-- Vue Components & Mixins -->
@include('CMS::partials.vueModal')
@include('CMS::partials.vuePagination')

<script>
    //Laravel's Token
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

    Vue.component('modal', {
        template: '#modal-template',
        props: {
            show: {
                type: Boolean,
                required: true,
                twoWay: true
            }
        }
    })

    new Vue({

        ready: function () { this.getAllData() },

        el: '#app',

        //Mixins
        mixins: [pagination],

        data: {

            //Json Data
            originalJson: null, //Json Original
            showJson: [],     //Json que se muestra

            panelEscolaridad: true,
            panelNuevaEscolaridad: true,

            //Error/Success/Loading Elements
            loading: false,
            loadingModal: false,
            showError: false,
            // actionOn: false, //??

            //Modal Vars
            showModal: false,

            editar: {
                nivelEducativo: '',
                grado: '',
                grupo: ''
            },

            guardar: {
                nivel_educativo: '',
                grado: '',
                grupo: ''
            },

        },

        methods: {

            //Get All Data
            getAllData: function () {
                this.loading = true
                var resource = this.$resource("{{route('CMS::admin.escolaridades.datos')}}")
                resource.get({}).then(function (response) {
                    //alert(JSON.stringify(response.data))
                    this.$set('originalJson', response.data)
                    this.showJson = this.originalJson
                    this.loading = false
                });
            },

            loadModal: function(idRecord){
                var id = this.findIndexByKeyValue(this.originalJson, 'id', idRecord);
                this.editar.id = this.originalJson[id]['id'];
                this.editar.nivelEducativo = this.originalJson[id]['nivel_educativo'];
                this.editar.grado = this.originalJson[id]['grado'];
                this.editar.grupo = this.originalJson[id]['grupo'];
                this.showError = false;
                this.showModal = true;
            },
            cargar: function(){
                this.showModal = true;
            },


            findIndexByKeyValue: function (arraytosearch, key, valuetosearch) {
                for (var i = 0; i < arraytosearch.length; i++) {
                    if (arraytosearch[i][key] == valuetosearch) {
                        return i;
                    }
                }
                return null;
            },

            borrar:function(id){

                var ConfirmBox = confirm("Deseas Eliminar la escolaridad con id: "+ id +" ?" )
                if(ConfirmBox) var resource = this.$resource("{{route('CMS::admin.escolaridades.borrar')}}")
                resource.delete({id:id}).then(function (response){
                this.notificationFx('Se ha borrado la escolaridad',"success","fa fa-exclamation-triangle fa-3x")
                this.getAllData();
                })

            },

            notificationFx: function (message, tipo, icon) {
                var notification = new NotificationFx({
                    message : '<span><i class="'+icon+'"></i></span><p>'+message+'</p>',
                    layout : 'bar',
                    effect : 'slidetop',
                    type : tipo // notice, warning, error or success
                })
                notification.show()
            },

     
            guardarEsc: function () {
                this.loadingModal = true
                //data = {id: this.guardar.id, nivelEducativo: this.guardar.nivelEducativo, grado: this.guardar.grado, grupo: this.guardar.grupo}

                var form = new FormData();
                form.append('nivel_educativo', this.guardar.nivel_educativo);
                form.append('grado', this.guardar.grado);
                form.append('grupo', this.guardar.grupo);

                this.save(form, "{{route('CMS::admin.escolaridades.save')}}", "Escolaridad creada correctamente.")

            },

            //Ajax Functions
            actualizar: function () {
                this.loadingModal = true
                data = {id: this.editar.id, nivelEducativo: this.editar.nivelEducativo, grado: this.editar.grado, grupo: this.editar.grupo}
                this.save(data, "{{route('CMS::admin.escolaridades.actualizar')}}", "Actualizado correctamente.")
            },

            //Vue Http Resources
            save: function (data, route, msgSuccess) {
                var resource = this.$resource(route)
                resource.save(data).then(function (response) {
                    this.success()
                    this.notificationFx(msgSuccess, "success", "fa fa-check-circle-o fa-3x")
                
                    this.guardar.nivel_educativo = '';
                    this.guardar.grado = '';
                    this.guardar.grupo = '';
                    nivel.focus();
                }, function (response) {
                    this.fail()
                    this.notificationFx("Hubo un problema en el proceso.", "error", "fa fa-exclamation-triangle fa-3x")
                })
            },
            //Vue Http Response Handler
            success: function (response) {
                this.loadingModal = false
                this.showError = false
                this.showModal = false
                this.showModalMessage = false
                this.getAllData()
            },
            fail: function (response) {
                this.loadingModal = false
                this.showError = true
            },
        },

        filters: {
            onlyNumberDisplay: {
                read: function(val) {

                    return isNaN(val) ? '' : val
                },
                write: function(val, oldVal) {
                    var number = +val.replace(/[^\d]/g, '')
                    console.log(number);
                    return (number == 0) ? '' : number
                }
            },
            onlyTextDisplay: {
                read: function(val) {
                    return val
                },
                write: function(val, oldVal) {
                    var text = '';
                    text = text + val.replace(/[\d]/g, '')
                    console.log(text)
                    return text
                }
            }
        }

    });

</script>