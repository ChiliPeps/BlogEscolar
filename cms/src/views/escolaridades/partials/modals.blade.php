<modal :show.sync="showModal">

  
    <div slot="head">Editar Escolaridad</div>
    <div slot="body">  
  
        <div class="row">
            <div class="col-md-4 text-center">
                <label>Nivel Educativo: </label>
            </div>
            <div class="col-md-4 text-center">
                <label>Grado: </label>
            </div>
            <div class="col-md-4 text-center">
                <label>Grupo: </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center">
                <input  v-model="editar.nivelEducativo" class="form-control">
            </div>
            <div class="col-md-4 text-center">
                <input v-model="editar.grado" class="form-control">
            </div>
            <div class="col-md-4 text-center">
                <input v-model="editar.grupo" class="form-control">
            </div>
        </div>
        <br>
        
        <div class="text-center"><i v-show="loadingModal" class="fa fa-spinner fa-spin fa-5x"></i></div>
        <div class="text-center"><i style="color: red;" v-show="showError" class="fa fa-times-circle fa-5x"></i></div>

        <div class="row">
            <div class="col-md-12" style="margin-left: auto; margin-right: auto;">
                <div class="box-tools text-center">
                    <button @click="actualizar" class="btn btn-success btn-xs"> Actualizar <i class="fa fa-arrow-circle-right"></i></button>
                </div>
            </div>
        </div>

    </div>

</modal>