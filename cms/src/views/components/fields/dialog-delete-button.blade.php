<!-- Modal -->
<div class="modal modal-danger fade" id="{{ $id }}" tabindex="-1" role="dialog" aria-labelledby="{{ $id }}Label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="{{ $id }}Label"><i class="fa fa-trash-o"></i> Borrar</h4>
            </div>
            {!! Form::open(['route' => [$route, $entity->id], 'method' => 'DELETE']) !!}
                <div class="modal-body">
                    ¿ Realmente deseas borrarlo?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">No, Cancelar</button>
                    <button type="submit" class="btn btn-outline ">Si</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>