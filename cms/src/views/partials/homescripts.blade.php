<!-- ChartJs -->
{!! Html::script('js/chartJs/Chart.min.js') !!}

<script>
var ctx = document.getElementById("myChart");
var data = {
    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    datasets: [
        {
            label: "Tareas",
            backgroundColor: "rgba(99, 255, 172,0.5)",
            borderColor: "rgba(90,99,132,1)",
            borderWidth: 1,
            hoverBackgroundColor: "rgba(99, 255, 172,0.8)",
            hoverBorderColor: "rgba(90,99,132,1)",
            data: [],
        },
        {
            label: "Avisos",
            backgroundColor: "rgba(255,99,132,0.5)",
            borderColor: "rgba(255,65,132,1)",
            borderWidth: 1,
            hoverBackgroundColor: "rgba(255,99,132,0.8)",
            hoverBorderColor: "rgba(255,99,132,1)",
            data: [],
        }
    ]
};

var options = {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero:true
            }
        }]
    }
};

var config = { type: 'bar', data: data, options: options };
var myBarChart = new Chart(ctx, config);
</script>

<script>
//Laravel's Token
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

new Vue({

	ready:function(){this.getGrafica()},

	el:'#app',

	mixins:[],

	data:{

		year: '',
	},

	methods:{
		getGrafica: function(){
			var resource = this.$resource("{{route('CMS::admin.tareas.graficas')}}")
            resource.get({ year: this.year }).then(function (response) {
                //alert(JSON.stringify(response.data))
                // this.loading = false

                myBarChart.data.datasets[0].data = response.data['tareas'];
                myBarChart.data.datasets[1].data = response.data['avisos'];
                myBarChart.update();
                myBarChart.resize();
            })
		}

	}
});



</script>