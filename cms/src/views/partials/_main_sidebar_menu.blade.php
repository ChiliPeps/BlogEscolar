<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header text-uppercase">@lang('CMS::core.primary_menu_title')</li>
           
            {{-- {!! CMS::makeLinkForSidebarMenu('CMS::admin.categories.index', trans('CMS::categories.categories'), 'fa fa-folder-open') !!} --}}
            {{-- {!! CMS::makeLinkForSidebarMenu('CMS::admin.articles.index', trans('CMS::articles.articles'), 'fa fa-file') !!} --}}
    

             @if (Auth::user()->type == "suadmin") {
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.users.index', trans('CMS::users.users'), 'fa fa-users') !!}    
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.tareas.index', trans('CMS::tareas.tareas'), 'fa fa-file-word-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.avisos.index', trans('CMS::avisos.avisos'), 'fa fa-comments-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.escolaridades.index', trans('CMS::escolaridades.escolaridades'), 'fa fa-file') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.sociales.index', trans('CMS::sociales.sociales'), 'fa fa-file-image-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.deportes.index', trans('CMS::deportes.deportes'), 'fa fa-futbol-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.mantenimiento.index', trans('Mantenimiento'), 'fa fa-eye') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.noticias.index', trans('CMS::noticias.noticias'), 'fa fa-file') !!}
                          
             }

            @elseif(Auth::user()->type == "admin")
            {
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.users.index', trans('CMS::users.users'), 'fa fa-users') !!}    
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.tareas.index', trans('CMS::tareas.tareas'), 'fa fa-file-word-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.avisos.index', trans('CMS::avisos.avisos'), 'fa fa-comments-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.escolaridades.index', trans('CMS::escolaridades.escolaridades'), 'fa fa-file') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.sociales.index', trans('CMS::sociales.sociales'), 'fa fa-file-image-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.deportes.index', trans('CMS::deportes.deportes'), 'fa fa-futbol-o') !!}
           
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.noticias.index', trans('CMS::noticias.noticias'), 'fa fa-file') !!}

            }

            @elseif(Auth::user()->name == "Padres") 
            {
                   {{-- {!! CMS::makeLinkForSidebarMenu('CMS::admin.users.index', trans('CMS::users.users'), 'fa fa-users') !!}  --}}

            }
            @else{

                {!! CMS::makeLinkForSidebarMenu('CMS::admin.tareas.index', trans('CMS::tareas.tareas'), 'fa fa-file-word-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.avisos.index', trans('CMS::avisos.avisos'), 'fa fa-comments-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.escolaridades.index', trans('CMS::escolaridades.escolaridades'), 'fa fa-file') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.sociales.index', trans('CMS::sociales.sociales'), 'fa fa-file-image-o') !!}
                {!! CMS::makeLinkForSidebarMenu('CMS::admin.deportes.index', trans('CMS::deportes.deportes'), 'fa fa-futbol-o') !!}            
            }
            @endif  
           
            
             {{--{!! CMS::makeLinkForSidebarMenu('CMS::admin.mantenimiento.index', trans('CMS::deportes.deportes'), 'fa fa-futbol-o') !!} --}}
            

        </ul>
    </section>
</aside>