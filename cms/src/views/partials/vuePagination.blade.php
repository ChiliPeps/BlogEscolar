<style> .pagination li a { cursor: pointer; } </style>

<template id="pagination-template" :value="data" :value="totalPages" :value="startPoint" :value="setPage">
	<ul class="pagination">
	    <li><a @click="setPage(data.currentPage - 1)" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>

	    <li class="disabled"><a v-if="startPoint != 0">...</a></li>

	    <li v-for="pageNumber in totalPages" v-bind:class="{'active': data.currentPage == pageNumber }">
	        <a v-if="$index >= startPoint && $index <= startPoint + data.pagesToShow" @click="setPage(pageNumber)">@{{ pageNumber + 1 }}</a>
	    </li>

	    <li class="disabled"><a v-if="startPoint < totalPages - data.pagesToShow - 1">...</a></li>

	    <li><a @click="setPage(data.currentPage + 1)" aria-label="Siguiente"><span aria-hidden="true">&raquo;</span></a></li>
	</ul>
</template>

<script>
var pagination = {

	components: {
		'pagination': {
			template: '#pagination-template',
			props: ['data', 'totalPages', 'startPoint', 'setPage']
		}
	},

    data: {
		pag: {
            currentPage: 0,
            itemsPerPage: 15,
            resultCount: 0,
            pagesToShow: 5,
            range: 3
        }
	},
    
	computed: {
        totalPages: function() {
          return Math.ceil(this.pag.resultCount / this.pag.itemsPerPage)
        },
        startPoint: function() {
            var point = this.totalPages - this.pag.currentPage
            if(point <= this.pag.pagesToShow) {
                point = this.totalPages - this.pag.pagesToShow
            } else { point = this.pag.currentPage - this.pag.range }
            if(point < 0) { point = 0 }
            return point
        }
    },
    methods: {
        setPage: function(pageNumber) {
            if(pageNumber >= 0) { 
                if(pageNumber < this.totalPages) { this.pag.currentPage = pageNumber }
                else { this.pag.currentPage = this.totalPages - 1 }
            } else { this.pag.currentPage = 0 }
        }
    },
    filters: {
        paginate: function(list) {
            this.pag.resultCount = list.length
            if (this.pag.currentPage > this.totalPages) {
              this.pag.currentPage = this.totalPages - 1
            }
            var index = this.pag.currentPage * this.pag.itemsPerPage
            return list.slice(index, index + this.pag.itemsPerPage)
        }
    }
}
</script>