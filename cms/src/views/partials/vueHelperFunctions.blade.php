<script>
var helperFunctions = {
    methods: {
        //Helper Functions
        findIndexByKeyValue: function (arraytosearch, key, valuetosearch) {
            for (var i = 0; i < arraytosearch.length; i++) {
                if (arraytosearch[i][key] == valuetosearch) { return i }
            } return null
        },

        notificationFx: function (message, tipo, icon) {
            var notification = new NotificationFx({
                message : '<span><i class="'+icon+'"></i></span><p>'+message+'</p>',
                layout : 'bar',
                effect : 'slidetop',
                type : tipo // notice, warning, error or success
            })
            notification.show()
        }
    }
}
</script>   