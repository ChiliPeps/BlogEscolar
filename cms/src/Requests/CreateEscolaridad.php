<?php

namespace Gmlo\CMS\Requests;

use App\Http\Requests\Request;

class CreateEscolaridad extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grado'           => 'required',
            'grupo'           => 'required',
            'nivel_educativo' => 'required'
        ];
    }
}
