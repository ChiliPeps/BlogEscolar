<?php

namespace Gmlo\CMS\Requests;

use App\Http\Requests\Request;

class CreateSocial extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contenido'     => 'required',
            // 'fechaAlta'  => 'required',
            'fechaEntrega'    => 'required',
            'idEscolaridad' => 'required',
            'idUsuario' => 'required',
            'materia' => 'required',
            'profesor'=>'required',
            'recomendaciones' => 'required',
            'tipo'=> 'required',
        ];
    }
}
