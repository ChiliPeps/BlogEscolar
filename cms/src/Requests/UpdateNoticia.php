<?php

namespace Gmlo\CMS\Requests;

use App\Http\Requests\Request;

class UpdateNoticia extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo'          =>      'required',
            // 'fecha'           =>      'required',
            // 'id_categoria'    =>      'required',
            'descripcion'     =>      'required',
            'contenido'       =>      'required',
            // 'autor'           =>      'required',
            'imagen'          =>      'required'
            // 'autor_foto'      =>      'required'
        ];
    }
}
