<?php

namespace Gmlo\CMS\Modules\Noticias;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Noticia extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    //Table
    protected $table = 'cms_noticias';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['titulo',  /*'id_categoria',*/ 'descripcion', 'contenido', 'imagen', 'thumb',  'hits', 'created_at'];

    // public function categoria()
    // {
    //     return $this->belongsTo('Nhitrort90\CMS\Modules\Categorias\Categoria', 'id_categoria');
    // }

    //Inverse Relation
    public function tags()
    {
        return $this->hasMany('Gmlo\CMS\Modules\Tags\Tag_Noticia', 'id_noticia');
    }

    //Inverse Relation
    public function galerias()
    {
        return $this->hasMany('Gmlo\CMS\Modules\Investimges\Investimg', 'id_noticia');
    }
}
