<?php

namespace Gmlo\CMS\Modules\Carpetas;

use Gmlo\CMS\Modules\Lib\BaseRepo;


class CarpetasRepo extends BaseRepo
{

    public function getModel()
    {
        return new Carpeta;
    }

    public function prepareData($data = [])
    {
        $data['created_by'] = \Auth::user()->id;
        return $data;
    }

}