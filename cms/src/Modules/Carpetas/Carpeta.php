<?php

namespace Gmlo\CMS\Modules\Carpetas;

use Illuminate\Database\Eloquent\Model;
use Gmlo\CMS\Modules\Lib\PresentableTrait;
use Gmlo\CMS\Modules\Carpetas\CarpetaPresenter;

class Carpeta extends Model
{
    use PresentableTrait;

    protected $presenter = CarpetaPresenter::class;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_carpetas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre', 'contenido', 'created_at', 'updated_at'];

   
}
