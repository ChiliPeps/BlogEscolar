<?php

namespace Gmlo\CMS\Modules\Carpetas;

use Gmlo\CMS\Modules\Lib\Presenter;

class CarpetaPresenter extends Presenter
{
    public function isPublish()
    {
        return $this->published_at != null;
    }

}