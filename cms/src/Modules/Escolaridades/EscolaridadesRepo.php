<?php

namespace Gmlo\CMS\Modules\Escolaridades;

use Gmlo\CMS\Modules\Lib\BaseRepo;


class EscolaridadesRepo extends BaseRepo
{

    public function getModel()
    {
        return new Escolaridad;
    }

    public function prepareData($data = [])
    {
        $data['created_by'] = \Auth::user()->id;
        return $data;
    }

}