<?php

namespace Gmlo\CMS\Modules\Escolaridads;

use Gmlo\CMS\Modules\Lib\Presenter;

class EscolaridadPresenter extends Presenter
{
    public function isPublish()
    {
        return $this->published_at != null;
    }

}