<?php

namespace Gmlo\CMS\Modules\Escolaridades;

use Illuminate\Database\Eloquent\Model;
use Gmlo\CMS\Modules\Lib\PresentableTrait;
use Gmlo\CMS\Modules\Escolaridades\EscolaridadPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;


class Escolaridad extends Model
{
    use PresentableTrait;

    protected $presenter = EscolaridadPresenter::class;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_escolaridad';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nivel_educativo', 'grado', 'grupo', 'created_at'];

    public function alumnos()
    {
        return $this->hasMany('Gmlo\CMS\Modules\Alumnos\Alumno', 'idescolaridad');
    }

    // softsDeletes
    use SoftDeletes;

    protected $dates = ['deleted_at'];
}
