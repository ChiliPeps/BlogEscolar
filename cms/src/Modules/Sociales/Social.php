<?php

namespace Gmlo\CMS\Modules\Sociales;

use Illuminate\Database\Eloquent\Model;
use Gmlo\CMS\Modules\Lib\PresentableTrait;
use Gmlo\CMS\Modules\Sociales\SocialPresenter;

class Social extends Model
{
    use PresentableTrait;

    protected $presenter = SocialPresenter::class;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_social';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['contenido', 'fechaAlta', 'fechaEntrega', 'idEscolaridad', 'idUsuario', 'materia', 'profesor', 'recomendaciones', 'tipo'];

   
}
