<?php

namespace Gmlo\CMS\Modules\Sociales;

use Gmlo\CMS\Modules\Lib\Presenter;

class SocialPresenter extends Presenter
{
    public function isPublish()
    {
        return $this->published_at != null;
    }

}