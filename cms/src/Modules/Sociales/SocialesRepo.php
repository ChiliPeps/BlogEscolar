<?php

namespace Gmlo\CMS\Modules\Sociales;

use Gmlo\CMS\Modules\Lib\BaseRepo;


class SocialesRepo extends BaseRepo
{

    public function getModel()
    {
        return new Social;
    }

    public function prepareData($data = [])
    {
        $data['created_by'] = \Auth::user()->id;
        return $data;
    }

}