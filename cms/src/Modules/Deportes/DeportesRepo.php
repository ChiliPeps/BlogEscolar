<?php

namespace Gmlo\CMS\Modules\Deportes;

use Gmlo\CMS\Modules\Lib\BaseRepo;


class DeportesRepo extends BaseRepo
{

    public function getModel()
    {
        return new Deporte;
    }

    public function prepareData($data = [])
    {
        $data['created_by'] = \Auth::user()->id;
        return $data;
    }

}