<?php

namespace Gmlo\CMS\Modules\Deportes;

use Gmlo\CMS\Modules\Lib\Presenter;

class DeportePresenter extends Presenter
{
    public function isPublish()
    {
        return $this->published_at != null;
    }

}