<?php

namespace Gmlo\CMS\Modules\Deportes;

use Illuminate\Database\Eloquent\Model;
use Gmlo\CMS\Modules\Lib\PresentableTrait;
use Gmlo\CMS\Modules\Deportees\DeportePresenter;

class Deporte extends Model
{
    use PresentableTrait;

    protected $presenter = DeportePresenter::class;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_deportes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['contenido', 'fechaAlta', 'idUsuario', 'tipoContenido', 'tipoDeporte', 'created_at', 'updated_at'];

   
}
