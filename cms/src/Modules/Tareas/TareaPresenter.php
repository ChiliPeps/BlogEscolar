<?php

namespace Gmlo\CMS\Modules\Tareas;

use Gmlo\CMS\Modules\Lib\Presenter;

class TareaPresenter extends Presenter
{
    public function isPublish()
    {
        return $this->published_at != null;
    }

}