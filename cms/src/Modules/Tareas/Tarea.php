<?php

namespace Gmlo\CMS\Modules\Tareas;

use Illuminate\Database\Eloquent\Model;
use Gmlo\CMS\Modules\Lib\PresentableTrait;
use Gmlo\CMS\Modules\Tareas\TareaPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tarea extends Model
{
    use PresentableTrait;

    protected $presenter = TareaPresenter::class;


    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_tareas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['contenido', 'fechaAlta', 'fechaEntrega', 'idEscolaridad', 'idUsuario', 'materia', 'profesor', 'recomendaciones', 'tipo'];

    public function user()
    {
        return $this->belongsTo('Gmlo\CMS\Modules\Users\User','idUsuario');
    }

    public function escolaridad()
    {
        return $this->belongsTo('Gmlo\CMS\Modules\Escolaridades\Escolaridad', 'idEscolaridad');
    }
}
