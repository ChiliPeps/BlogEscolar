<?php

namespace Gmlo\CMS\Modules\Tareas;

use Gmlo\CMS\Modules\Lib\BaseRepo;


class TareasRepo extends BaseRepo
{

    public function getModel()
    {
        return new Tarea;
    }

    public function prepareData($data = [])
    {
        $data['created_by'] = \Auth::user()->id;
        return $data;
    }

}