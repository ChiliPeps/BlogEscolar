<?php

namespace Gmlo\CMS\Modules\Tags;

use Illuminate\Database\Eloquent\Model;

class Tag_Noticia extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_tag_noticia';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_tag', 'id_noticia'];

    public function noticia()
    {
        return $this->belongsTo('Gmlo\CMS\Modules\Noticias\Noticia', 'id_noticia');
    }

    public function tag()
    {
        return $this->belongsTo('Gmlo\CMS\Modules\Tags\Tag', 'id_tag');
    }
}
