<?php

namespace Gmlo\CMS\Modules\Avisos;

use Gmlo\CMS\Modules\Lib\BaseRepo;


class AvisosRepo extends BaseRepo
{

    public function getModel()
    {
        return new Aviso;
    }

    public function prepareData($data = [])
    {
        $data['created_by'] = \Auth::user()->id;
        return $data;
    }

}