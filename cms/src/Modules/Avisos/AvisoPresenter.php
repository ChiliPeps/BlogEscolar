<?php

namespace Gmlo\CMS\Modules\Avisos;

use Gmlo\CMS\Modules\Lib\Presenter;

class AvisoPresenter extends Presenter
{
    public function isPublish()
    {
        return $this->published_at != null;
    }

}