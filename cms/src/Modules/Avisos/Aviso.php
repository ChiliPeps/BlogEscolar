<?php

namespace Gmlo\CMS\Modules\Avisos;

use Illuminate\Database\Eloquent\Model;
use Gmlo\CMS\Modules\Lib\PresentableTrait;
use Gmlo\CMS\Modules\Avisos\AvisoPresenter;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aviso extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    use PresentableTrait;

    protected $presenter = AvisoPresenter::class;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cms_avisos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['contenido', 'fechaAlta', 'fechaEntrega', 'idEscolaridad', 'idUsuario', 'materia', 'profesor', 'recomendaciones', 'tipo'];

    public function category()
    {
        return $this->belongsTo('Gmlo\CMS\Modules\Categories\Category');
    }

    public function image()
    {
        return $this->belongsTo('Gmlo\CMS\Modules\Assets\Asset', 'primary_img');
    }

}
