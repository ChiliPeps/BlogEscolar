<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CmsCoreTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('avatar')->nullable();
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->enum('type', ['suadmin', 'admin', 'multinivel','primaria','secundaria','kinder','bachillerato']);
            $table->timestamp('blocked_at')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });

        //tabla escolaridad
         Schema::create('cms_escolaridad', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grado');
            $table->string('grupo');
            $table->string('nivel_educativo');
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla tareas
         Schema::create('cms_tareas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('idEscolaridad')->unsigned()->nullable();
            $table->foreign('idEscolaridad')->references('id')->on('cms_escolaridad');

            $table->integer('idUser')->unsigned()->nullable();
            $table->foreign('idUser')->references('id')->on('cms_users');

            $table->string('tipo');
            $table->date('fechaAlta');
            $table->date('fechaEntrega');
            $table->string('materia');
            $table->string('profesor');
            $table->text('contenido');
            $table->string('recomendaciones');
            $table->string('codigo');

            $table->timestamps();
            $table->softDeletes();
        });
        
        // tabla Carpetas

          Schema::create('cms_carpetas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('receptor');
            $table->timestamps();
        });

          //tabla avisos
         Schema::create('cms_avisos', function (Blueprint $table) {
            $table->increments('id');
            $table->text('contenido');
            $table->date('fechaAlta');

            $table->integer('idUser')->unsigned()->nullable();
            $table->foreign('idUser')->references('id')->on('cms_users');

            $table->string('nombreMes');
            $table->string('receptor');
            $table->string('tipo');
            $table->string('titulo');
            $table->char('para', 15);
            $table->char('nivel', 15);

            $table->timestamps();
            $table->softDeletes();
        });

         //tabla Social
         Schema::create('cms_social', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenido');
            $table->date('fechaAlta');

            $table->integer('idCarpeta')->unsigned()->nullable();
            $table->foreign('idCarpeta')->references('id')->on('cms_carpetas');

            $table->integer('idUser')->unsigned()->nullable();
            $table->foreign('idUser')->references('id')->on('cms_users');

            $table->string('receptor');
           
            $table->timestamps();
        });

         //tabla Reflexiones
         Schema::create('cms_reflexiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenido');
            $table->date('fechaAlta');

            $table->integer('idUser')->unsigned()->nullable();
            $table->foreign('idUser')->references('id')->on('cms_users');

            $table->string('mes');
           
            $table->timestamps();
        });

         //tabla Deportes
         Schema::create('cms_deportes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenido');
            $table->date('fechaAlta');

            $table->integer('idUser')->unsigned()->nullable();
            $table->foreign('idUser')->references('id')->on('cms_users');

            $table->string('tipoContenido');
            $table->string('tipoDeporte');
           
            $table->timestamps();
        });

         //tabla tipo aviso
         Schema::create('cms_tipoaviso', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenido');
            $table->date('fechaAlta');

            $table->integer('idEscolaridad')->unsigned()->nullable();
            $table->foreign('idEscolaridad')->references('id')->on('cms_escolaridad');

            $table->string('nivelEscolaridad');
            $table->string('tipo');
           
            $table->timestamps();
        });
        //default tables
        Schema::create('cms_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });

        Schema::create('cms_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('path');
            $table->boolean('is_image');
            $table->text('tags')->nullable();
            $table->string('extension');
            $table->timestamps();
        });


        Schema::create('cms_articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug_url');
            $table->integer('primary_img')->nullable();
            $table->text('sumary');
            $table->text('body');
            $table->string('title_seo');
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();

            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('cms_categories');

            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('id')->on('cms_users');

            $table->integer('section_id')->nullable();
            $table->integer('views')->default(0);
            $table->date('published_at')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('cms_noticias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->date('fecha');
            $table->text('descripcion');
            $table->text('contenido');
            $table->string('categoria');
            $table->string('autor');

            $table->string('slugurl');
            $table->string('imagen');
            $table->string('thumb');

            $table->integer('hits')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('cms_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('cms_tag_noticia', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('id_tag')->unsigned()->nullable();
            $table->foreign('id_tag')->references('id')->on('cms_tags');

            $table->integer('id_noticia')->unsigned()->nullable();
            $table->foreign('id_noticia')->references('id')->on('cms_noticias');
        });

        Schema::create('deletes', function (Blueprint $table) {
            // $table->increments('id');
            $table->string('idUserElimino');
            $table->string('nombreElimino');
            $table->timestamp('fechaBorrado');
            $table->string('tabla');
            $table->string('idBorrado');
        });

        Schema::create('vaciado', function (Blueprint $table) {
            // $table->increments('id');
            $table->string('tablaVaciado');
            $table->string('idUserUltimoVaciado');
            $table->string('nombreUltimoVaciado');
            $table->integer('Cantidad');
            $table->timestamp('fechaVaciado');
           
        });

        // DB::unprepared('
        //      CREATE TRIGGER `avisobaja` BEFORE DELETE ON `cms_avisos`
        //     FOR EACH ROW INSERT INTO deleteavisos set iduser = OLD.iduser, fecha = NOW(), contenido = OLD.contenido, receptor = OLD.receptor
        //     ');
       
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cms_articles');
        Schema::drop('cms_categories');
        Schema::drop('cms_users');
        Schema::drop('cms_escolaridad');
        Schema::drop('cms_tareas');
        Schema::drop('cms_carpetas');
        Schema::drop('cms_social');
        Schema::drop('cms_avisos');
        Schema::drop('cms_tipoaviso');
        Schema::drop('cms_reflexiones');
        Schema::drop('cms_deporte');
        Schema::drop('deletes');
        Schema::drop('vaciado');
        // DB::unprepared('DROP TRIGGER''avisobaja');


    }
}
